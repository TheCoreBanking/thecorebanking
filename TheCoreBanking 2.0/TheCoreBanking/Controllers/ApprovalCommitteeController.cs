﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using TheCoreBanking.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheCoreBanking.Data.Helpers;


namespace TheCoreBanking.Controllers
{
    public class ApprovalCommitteeController : Controller
    {
        private readonly TheCoreBankingContext _context;
        public ApprovalCommitteeController(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }



        public IActionResult Index()
        {
            return View();
        }




        //LIST COMMITTEE

        public JsonResult listcommittee()
        {

            /*var results = setupUnitOfWork.Committee.GetAll();*/
            var results = setupUnitOfWork.Committee.GetAll().GroupBy(b => b.OperationId).Select(b => b.FirstOrDefault());

            //var results = setupUnitOfWork.Committee.GetAll();

            List<CommitteeApprovalVM> ComAppVM = new List<CommitteeApprovalVM>();

            foreach (var item in results)
            {

                CommitteeApprovalVM ComViewModel = new CommitteeApprovalVM();
                ComViewModel.MgtNo = item.MgtNo;
                ComViewModel.MgtNoApproval = item.MgtNoApproval;
                ComViewModel.BoardNo = item.BoardNo;
                ComViewModel.BoardNoApproval = item.BoardNoApproval;
                ComViewModel.CreditMinMgt = item.CreditMinMgt;
                ComViewModel.CreditMaxComteeAmt = item.CreditMaxComteeAmt;
                ComViewModel.CreditMinBoard = item.CreditMinBoard;
                ComViewModel.CreditMaxBoardAmt = item.CreditMaxBoardAmt;
                ComViewModel.Committee = item.Committee;
                ComViewModel.Createdby = item.Createdby;
                ComAppVM.Add(ComViewModel);

            }


            return Json(results);


        }


        [HttpPost]
        public JsonResult PostCommittee(TblBankingCommittee commit)
        {

            TblBankingCommittee addcommit = new TblBankingCommittee();

            var loguser = User.Identity.Name;
            if (loguser == null)
            {
                loguser = "tayo.olawumi";
            }
            else
            {
                loguser = User.Identity.Name;
            }

            var branch = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().BranchId;

            var company = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().CompanyId;

            //if (committee.MgtNo.ToString() == "" || committee.MgtNo.ToString() == "0" || committee.MgtNo.ToString() == null)
            if (commit.BoardNo.ToString() == "" || commit.BoardNo.ToString() == "0" || commit.BoardNo.ToString() == null)
            {
                addcommit.CreditMaxComteeAmt = commit.CreditMaxComteeAmt;
                addcommit.CreditMinMgt = commit.CreditMinMgt;
                addcommit.Committee = true;
                addcommit.CommitteeType = "M";
                addcommit.CoyCode = company;
                addcommit.CreateDate = DateTime.Now;
                addcommit.Createdby = loguser;
                addcommit.MgtNo = commit.MgtNo;
                addcommit.MgtNoApproval = commit.MgtNoApproval;
                addcommit.OperationId = commit.OperationId;
                addcommit.BrCode = branch;
                setupUnitOfWork.Committee.Add(addcommit);
                setupUnitOfWork.Commit();
                return Json(commit);

            }
            else
            {
                addcommit.CreditMaxComteeAmt = commit.CreditMaxComteeAmt;
                addcommit.CreditMinMgt = commit.CreditMinMgt;
                addcommit.Committee = true;
                addcommit.CommitteeType = "M";
                addcommit.CoyCode = company;
                addcommit.CreateDate = DateTime.Now;
                addcommit.Createdby = loguser;
                addcommit.MgtNo = commit.MgtNo;
                addcommit.MgtNoApproval = commit.MgtNoApproval;
                addcommit.OperationId = commit.OperationId;
                addcommit.BrCode = branch;

                addcommit.BoardNo = commit.BoardNo;
                addcommit.CreditMaxBoardAmt = commit.CreditMaxBoardAmt;
                addcommit.BoardNoApproval = commit.BoardNoApproval;
                addcommit.CreditMinBoard = commit.CreditMinBoard;
                setupUnitOfWork.Committee.Add(addcommit);
                //setupUnitOfWork.Commit();
                //return Json(committee);


                TblBankingCommittee addbodcommittee = new TblBankingCommittee();

                addbodcommittee.BoardNo = commit.BoardNo;
                addbodcommittee.CreditMaxBoardAmt = commit.CreditMaxBoardAmt;
                addbodcommittee.BoardNoApproval = commit.BoardNoApproval;
                addbodcommittee.BrCode = branch;
                addbodcommittee.CreditMinBoard = commit.CreditMinBoard;
                addbodcommittee.Committee = true;
                addbodcommittee.CommitteeType = "B";
                addbodcommittee.CoyCode = company;
                addbodcommittee.CreateDate = DateTime.Now;
                addbodcommittee.Createdby = loguser;
                addbodcommittee.OperationId = commit.OperationId;

                addbodcommittee.CreditMaxComteeAmt = commit.CreditMaxComteeAmt;
                addbodcommittee.CreditMinMgt = commit.CreditMinMgt;
                addbodcommittee.MgtNo = commit.MgtNo;
                addbodcommittee.MgtNoApproval = commit.MgtNoApproval;
                setupUnitOfWork.Committee.Add(addbodcommittee);
                setupUnitOfWork.Commit();

                return Json(true);
            }
        }



        [HttpGet]
        public JsonResult GetOperation()
        {

            var list = new List<SelectTwoContentM>();

            var result = setupUnitOfWork.OperationLevel.GetAll();
            foreach (var item in result)
            {
                list.Add(new SelectTwoContentM
                {
                    id = Convert.ToString(item.OperationId),
                    text = item.Operations,
                });
            }
            return Json(list);
        }






        public class SelectTwoContentM
        {
            public string id { get; set; }
            public string text { get; set; }
        }


    }
}
