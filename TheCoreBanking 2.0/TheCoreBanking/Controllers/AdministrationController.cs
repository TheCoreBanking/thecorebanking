﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using TheCoreBanking.Models;
using IdentityModel;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
//using TheCoreBanking.Data.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TheCoreBanking.Controllers
{
    public class AdministrationController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public AdministrationController(
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          RoleManager<IdentityRole> roleManager)
        {
            // if the TestUserStore is not in DI, then we'll just use the global users collection
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ManageRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ManageRole(RoleViewModel model)
        {
            bool isRoleExist = await _roleManager.RoleExistsAsync(model.RoleName);
            if (!isRoleExist)
            {  
                var role = new IdentityRole(model.RoleName);
                role.Name = model.RoleName;
                var result = await _roleManager.CreateAsync(role);
                if (result.Succeeded)
                {
                   await _roleManager.AddClaimAsync(role, 
                            new Claim("User","Register")
                        );
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddUserRole(UserRoleViewModel model)
        {
            bool isRoleExist = await _roleManager.RoleExistsAsync(model.RoleName);
            ApplicationUser isUser = await _userManager.FindByNameAsync(model.Username);
            if (isRoleExist && isUser != null)
            {
                
                var result = await _userManager.AddToRoleAsync(isUser, model.RoleName);
                if (result.Succeeded)
                {
                   
                }
            }
            return View();
        }


        public JsonResult ListRole()
        {
            var allRoles =  _roleManager.Roles;
            return Json(allRoles.Select(o => new { o.Id, o.Name}));
        }

        public JsonResult ListUser()
        {
            var allUsers = _userManager.Users;
            return Json(allUsers.Select(o => new { o.Id, o.UserName }));
        }

        public JsonResult ListUsersRoles()
        {
            List<UserRoles> userRole = new List<UserRoles>();
            var allUsers = _userManager.Users;
            foreach(var user in allUsers)
            {
               var userRoles = _userManager.GetRolesAsync(user).Result;
                 foreach(var role in userRoles)
                 {
                    userRole.Add(new UserRoles { role = role, Username = user.UserName });
                 }
            }
            return Json(userRole);
        }

        public class UserRoles
        {
            public string Username { get; set; }
            public string role { get; set; }
        }


        [Authorize("UserRegistration")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel model)
        {

            var newUser = _userManager.FindByNameAsync(model.Username).Result;

          
            if (newUser == null)
            {
                newUser = new ApplicationUser
                {
                    UserName = model.Username,
                    tblStaffInformation = model.staffInformation

            };
                var result = _userManager.CreateAsync(newUser, model.Password).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = _userManager.AddClaimsAsync(newUser, new Claim[]{
                            new Claim(JwtClaimTypes.Name, model.staffInformation.StaffName),
                            new Claim(JwtClaimTypes.EmailVerified, "false", ClaimValueTypes.Boolean),
                            new Claim(JwtClaimTypes.Role, "Staff"),
                            new Claim(JwtClaimTypes.Address, @"{ 'street_address': 'One Jesus Way', 'locality': 'Alberta', 'postal_code': 70392, 'country': 'Germany' }", IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                        }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
            }
            return View();
        }

        #region Select2 Helper

        public class Select2Format
        {
            public List<SelectContent> results { get; set; }
        }
        public class SelectContent
        {
            public string id { get; set; }
            public string text { get; set; }
        }
        #endregion
    }
}
