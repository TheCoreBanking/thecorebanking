﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheCoreBanking.Data.Contracts;
using System;
using TheCoreBanking.Data.Helpers;
using TheCoreBanking.Data.Models;
using TheCoreBanking.Products.Helpers;
using TheCoreBanking.ViewModel;

namespace TheCoreBanking.Controllers
{
    
    public class CompanySetupController : Controller
    {
        //string[] companyclass = { "Current/Savings Receiving", "Non Current/Saving Receiving" };
        //string[] managementtype = {"Absolute Line Mgt","Dynamic Line Mgt" };
        //string[] companytype = { "Asset Management", "Commercial Bank","Microfinance","Finance Company" };
        string[] accountingstand = { "GAAP", "IFRS" };
       

        private readonly TheCoreBankingContext _context;
        public CompanySetupController(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }


        // GET: /<controller>/
        [Authorize()]
        [AllowAnonymous]
        public IActionResult Index()
        {
            ViewBag.states = EnumToList.ListStates();
            //ViewBag.companyclass = companyclass;          
            //ViewBag.managementtype = managementtype;
            //ViewBag.companytype = companytype;
            ViewBag.accountingstand = accountingstand;
          
            return View();
        }

        #region Company
        public JsonResult AddCompany(TblCompanyInformation companyInformation)
        {
            companyInformation.Deleted = false;
            companyInformation.Approved = false;
            companyInformation.Disapproved = false;
            setupUnitOfWork.Company.Add(companyInformation);
            setupUnitOfWork.Commit();
            return Json(companyInformation.Id);
        }
        public JsonResult UpdateCompany(int id, TblCompanyInformation companyInformation)
        {
            companyInformation.Deleted = false;
            setupUnitOfWork.Company.Update(companyInformation);
            setupUnitOfWork.Commit();
            return Json(companyInformation.Id);
        }
        public IActionResult RemoveCompany(TblCompanyInformation companyInformation)
        {
            
            companyInformation.Deleted = true;

            setupUnitOfWork.Company.Update(companyInformation);
            setupUnitOfWork.Commit();
            return Json(companyInformation.Id);
        }
        public JsonResult CompanyCode(string CoyCode)
        {
            var checkCompany = setupUnitOfWork.Company.GetAll().Where(o => o.CoyCode == CoyCode).Count();
            if (string.IsNullOrEmpty(CoyCode))
                return Json("The Company Code Cannot be Null");
            if (checkCompany > 0)

                return Json("The Company Code Already Exist");
            else
                return Json("");
        }
        #endregion
        #region Branch
        public JsonResult BranchCode(string BrId)
        {
            var checkCompany = setupUnitOfWork.Branch.GetAll().Where(o => o.BrId == BrId).Count();
            if (string.IsNullOrEmpty(BrId))
                return Json("The branch code cannot be null");
            if (checkCompany > 0)

                return Json("The branch code already exist");
            else
                return Json("");
        }
        public JsonResult BranchName(string BrName)
        {
            var checkCompany = setupUnitOfWork.Branch.GetAll().Where(o => o.BrName == BrName).Count();
            if (string.IsNullOrEmpty(BrName))
                return Json("The branch name cannot be null");
            if (checkCompany > 0)

                return Json("The branch name already exist");
            else
                return Json("");
        }
        public JsonResult AddBranch(TblBranchInformation branchInformation)
        {
            ViewBag.states = EnumToList.ListStates();
            branchInformation.Deleted = false;
            setupUnitOfWork.Branch.Add(branchInformation);
            setupUnitOfWork.Commit();
            return Json(branchInformation.Id);
        }
        public JsonResult UpdateBranch(int Id, TblBranchInformation branchInformation)
        {
            ViewBag.states = EnumToList.ListStates();
            setupUnitOfWork.Branch.Update(branchInformation);
            setupUnitOfWork.Commit();
            return Json(branchInformation.Id);
        }

        public IActionResult RemoveBranch()
        {
            ViewBag.states = EnumToList.ListStates(); ;
            return View();
        }
        [HttpPost]
        public IActionResult RemoveBranch(TblBranchInformation branchInformation)
        {
            //var getId = setupUnitOfWork.Branch.GetAll().Where(o => o.Id == branchInformation.Id).FirstOrDefault(); ;
            //branchInformation.CoyId = getId.CoyId;
            //branchInformation.BrState = getId.BrState;
            //branchInformation.BrName = getId.BrName;
            //branchInformation.BrManager = getId.BrManager;
            //branchInformation.BrLocation = getId.BrLocation;
            //branchInformation.BrId = getId.BrId;
            //branchInformation.BrAddress = getId.BrAddress;
            branchInformation.Deleted = true;

            ViewBag.states = EnumToList.ListStates();
            setupUnitOfWork.Branch.Update(branchInformation);
            setupUnitOfWork.Commit();
            return Json(branchInformation.Id);
        }
        #endregion
        #region Currency
        public JsonResult CurrencyCode(string CurrCode)
        {
            var checkCompany = setupUnitOfWork.Currency.GetAll().Where(o => o.CurrCode.ToString() == CurrCode).Count();
            if (string.IsNullOrEmpty(CurrCode))
                return Json("Currency code cannot be null");
            if (checkCompany > 0)

                return Json("Currency code already exist");
            else
                return Json("");
        }
        public JsonResult CurrencyName(string CurrName)
        {
            var checkCompany = setupUnitOfWork.Currency.GetAll().Where(o => o.CurrName == CurrName).Count();
            if (string.IsNullOrEmpty(CurrName))
                return Json("The currency name cannot be null");
            if (checkCompany > 0)

                return Json("The currency name already exist");
            else
                return Json("");
        }
        public JsonResult AddCurrency(TblCurrency currency)
        {
            setupUnitOfWork.Currency.Add(currency);
            setupUnitOfWork.Commit();
            return Json(currency.Id);
        }

        //public JsonResult EditCurrency(int CurrCode, TblCurrency currency)
        public JsonResult EditCurrency(CurrencyViewModel currency)
        {
            var currentCurrencyDetail = setupUnitOfWork.Currency.GetByIdLong(currency.Id).FirstOrDefault();
            TblCurrencyHistory tblCurrencyHistory = new TblCurrencyHistory();    //setupUnitOfWork.currencyHist.Add(currency);
            tblCurrencyHistory.CurrName = currentCurrencyDetail.CurrName;
            tblCurrencyHistory.ExchangeRate = currentCurrencyDetail.ExchangeRate;
            tblCurrencyHistory.CurrCode = currentCurrencyDetail.CurrCode;
            tblCurrencyHistory.AverageRate = currentCurrencyDetail.AverageRate;
            tblCurrencyHistory.CountryCode = currentCurrencyDetail.CountryCode;
            tblCurrencyHistory.CurrSymbol = currentCurrencyDetail.CurrSymbol;

            DateTime time = DateTime.Now;
            tblCurrencyHistory.UpdatedDate = time;
            setupUnitOfWork.currencyHist.Add(tblCurrencyHistory);
            setupUnitOfWork.Commit();
            /*TblCurrency tblCurrency = new TblCurrency();
            tblCurrency.Id = currentCurrencyDetail.Id;
             tblCurrencyHistory.CurrCode = currency.CurrCode;
            tblCurrency.CurrName = currency.CurrName;
            tblCurrency.CountryCode = currency.CountryCode;
            tblCurrency.CurrSymbol = currency.CurrSymbol;
            tblCurrency.AverageRate = currency.AverageRate;
            tblCurrency.ExchangeRate = currency.ExchangeRate;*/

            currentCurrencyDetail.ExchangeRate = currency.ExchangeRate;
            setupUnitOfWork.Currency.Update(currentCurrencyDetail);
            setupUnitOfWork.Commit();
            return Json(currentCurrencyDetail.Id);
        }

        public IActionResult RemoveCurrency(TblCurrency currency)
        {
            setupUnitOfWork.Currency.Delete(currency);
            setupUnitOfWork.Commit();
            return Json(currency.Id);
        }
        #endregion

        #region Fetch Company Data

        public JsonResult listcompany()
        {
            var result = setupUnitOfWork.Company.GetAll().Where(o => o.Deleted == false).OrderByDescending(o => o.Id).ToList();
            return Json(result);
        }
        public JsonResult listbranch()
        {
            var result = setupUnitOfWork.Branch.GetAll().Where(o=>o.Deleted==false).OrderByDescending(o => o.Id); 
            return Json(result);
        }
 
        public JsonResult listcurrency()
        {
            var result = setupUnitOfWork.Currency.GetAll().OrderByDescending(o => o.Id);
            return Json(result);
        }

        #endregion

        #region Director
        public JsonResult DirectorBVN(string Bvn)
        {
            var checkBVN = setupUnitOfWork.Director.GetAll().Where(o => o.Bvn == Bvn).Count();
            if (string.IsNullOrEmpty(Bvn))
                return Json("BVN cannot be null");
            if (Bvn.Length != 10)
                return Json("BVN number cannot exceed 10 characters");
            if (checkBVN > 0)

                return Json("BVN already exist");
            else
                return Json("");
        }
        public JsonResult AddDirector(TblDirectorInformation branchInformation)
        {
            
            ViewBag.states = EnumToList.ListStates();

            setupUnitOfWork.Director.Add(branchInformation);
            setupUnitOfWork.Commit();
            return Json(branchInformation.Id);
        }
        public JsonResult UpdateDirector(int Id, TblDirectorInformation branchInformation)
        {
            ViewBag.states = EnumToList.ListStates();
            setupUnitOfWork.Director.Update(branchInformation);
            setupUnitOfWork.Commit();
            return Json(branchInformation.Id);
        }
        public JsonResult listDirector()
        {
            
           var result= setupUnitOfWork.Director.GetAll().OrderByDescending(o => o.Id);

            return Json(result);
        }
        public IActionResult RemoveDirector()
        {
            ViewBag.states = EnumToList.ListStates(); ;
            return View();
        }
        [HttpPost]
        public IActionResult RemoveDirector(TblDirectorInformation branchInformation)
        {
            ViewBag.states = EnumToList.ListStates();
            setupUnitOfWork.Director.Delete(branchInformation);
            setupUnitOfWork.Commit();
            return Json(branchInformation.Id);
        }
        #endregion

        //For Dropdown list for company() box Select2 starts here
        public JsonResult loadCompany()
        {
            Select2Format loadFormat = new Select2Format();
            var result = setupUnitOfWork.Company.GetAll().Where(o => o.Deleted == false ).ToList();
            List<SelectContent> list = new List<SelectContent>();

            foreach (var item in result)
            {
                SelectContent load = new SelectContent()
                {
                    id = item.Id.ToString(),
                    text = item.CoyName
                };

               list.Add(load);
            }
            loadFormat.results = list;
            return Json(loadFormat);

        }

        #region Select2 Helper

        public class Select2Format
        {
            public List<SelectContent> results { get; set; }
        }
        public class SelectContent
        {
            public string id { get; set; }
            public string text { get; set; }
        }
        #endregion
    }
}
