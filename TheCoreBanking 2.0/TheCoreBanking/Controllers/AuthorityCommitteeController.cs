﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using TheCoreBanking.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheCoreBanking.Data.Helpers;

namespace TheCoreBanking.Controllers
{
    public class AuthorityCommitteeController : Controller
    {

        private readonly TheCoreBankingContext _context;
        public AuthorityCommitteeController(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }



        public IActionResult Index()
        {
            return View();
        }



        public JsonResult listCommitMgt()
        {

            var result = setupUnitOfWork.ApprovalCommittee.GetAll().Where(o => o.CommitteeType == "M");

            List<CommitteeApprovalVM> ManagementAppVM = new List<CommitteeApprovalVM>();

            foreach (var item in result)
            {
                CommitteeApprovalVM manViewModel = new CommitteeApprovalVM();
                var operations = setupUnitOfWork.OperationLevel.GetAll().Where(i => i.OperationId == item.OperationId).Select(m => m.Operations).FirstOrDefault();

                manViewModel.Id = item.Id;
                manViewModel.CreditMaxComteeAmt = item.CreditMaxComteeAmt;
                manViewModel.CreditMinMgt = item.CreditMinMgt;
                manViewModel.Department = item.Department;
                manViewModel.CummulativeAmt = item.CummulativeAmt;
                manViewModel.ApprovingAuthority = item.ApprovingAuthority;
                manViewModel.ApprovingLevel = item.ApprovingLevel;
                manViewModel.StaffId = item.StaffId;
                manViewModel.BranchCode = item.BranchCode;
                manViewModel.CompanyCode = item.CompanyCode;
                manViewModel.BoardNo = item.BoardNo;
                //manViewModel.CreatedBy = item.CreatedBy;

                manViewModel.Operations = operations;

                ManagementAppVM.Add(manViewModel);

            }

            return Json(ManagementAppVM);

        }




        [HttpPost]
        public JsonResult AddComMgt(CommitteeApprovalVM man)
        {

            Data.Models.TblOperationApprovalCommittee addman = new Data.Models.TblOperationApprovalCommittee();
            var comm = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId);
            if (comm.Count() > 0)
            {
                var count = (setupUnitOfWork.ApprovalCommittee.GetAll().Where(o => o.ApprovingAuthority == man.StaffName && o.OperationId == man.OperationId && o.CommitteeType.Contains("M")).Select(o => o.CommitteeType).Count());
                //var checkLevel = (setupUnitOfWork.OpeApprove.GetAll().Where(o => o.OperationId == opeothers.OperationId && o.CommitteeType == "O" && o.ApprovingLevel == opeothers.ApprovingLevel).Count());

                if (count > 0)
                {
                    return Json(false);
                }

                else
                {
                    var loguser = User.Identity.Name;
                    if (loguser == null)
                    {
                        loguser = "tayo.olawumi";
                    }
                    else
                    {
                        loguser = User.Identity.Name;
                    }

                    var branch = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().BranchId;

                    var company = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().CompanyId;

                    var staffno = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == man.StaffName).FirstOrDefault().StaffNo;

                    var mannum = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().MgtNo;

                    var credmgtminamt = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().CreditMinMgt;

                    var credmgtmax = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().CreditMaxComteeAmt;

                    var credminbod = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().CreditMinBoard;

                    var bodno = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().BoardNo;

                    var credmaxbodamt = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().CreditMaxBoardAmt;

                    var mgtappno = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().MgtNoApproval;

                    var bodappno = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == man.OperationId).FirstOrDefault().BoardNoApproval;

                    addman.CreatedBy = loguser;
                    addman.ApprovingAuthority = man.StaffName;
                    addman.ApprovingLevel = man.ApprovingLevel;
                    addman.BranchCode = branch;
                    //addgroup.CommitteeType = opeothers.CommitteeType;
                    addman.CompanyCode = company;
                    addman.CreateDate = DateTime.Now;
                    addman.CummulativeAmt = man.CummulativeAmt;
                    addman.Department = man.Department;
                    addman.CreditMaxComteeAmt = credmgtmax;
                    addman.CreditMinMgt = credmgtminamt;
                    addman.OperationId = man.OperationId;
                    addman.MinAmt = man.MinAmt;
                    addman.MaxAmt = man.MaxAmt;
                    addman.StaffId = staffno;
                    addman.CommitteeType = "M";
                    addman.Approved = false;
                    addman.Disapproved = false;

                    addman.MgtNo = mannum;
                    addman.CreditMinBoard = credminbod;
                    addman.BoardNo = bodno;
                    addman.CreditMaxBoardAmt = credmaxbodamt;
                    addman.OperationId = man.OperationId;
                    addman.MgtNoApproval = mgtappno;
                    addman.BoardNoApproval = bodappno;

                    setupUnitOfWork.ApprovalCommittee.Add(addman);
                    setupUnitOfWork.Commit();

                    return Json(true);
                }
            }
            else
            {
                return Json(false);
            }
        }






        [HttpGet]
        public JsonResult GetOperation()
        {

            var list = new List<SelectTwoContentM>();

            var result = setupUnitOfWork.OperationLevel.GetAll();
            foreach (var item in result)
            {
                list.Add(new SelectTwoContentM
                {
                    id = Convert.ToString(item.OperationId),
                    text = item.Operations,
                });
            }
            return Json(list);
        }




        [HttpGet]
        public JsonResult AppLevel(int id)
        {
            List<ApplevelVM> applevelViewModel = new List<ApplevelVM>();
            var result = setupUnitOfWork.OperationLevel.GetAll().Where(m => m.OperationId == id);

            List<int> applevelForSelect2Input = new List<int>();

            var count = result.FirstOrDefault().ApprovalLevels;

            for (var i = 1; i <= count; i++)
            {

                var approvinglev = new ApplevelVM()
                {
                    id = i.ToString(),
                    text = Convert.ToString(i)
                };
                applevelViewModel.Add(approvinglev);
            }

            //foreach (var item in result)
            //{
            //    var approvinglev = new ApplevelVM()
            //    {
            //        id = item.Operations,
            //        text = Convert.ToString(item.ApprovalLevels)
            //    };
            //    applevelViewModel.Add(approvinglev);
            //}
            return Json(applevelViewModel);
        }



        [HttpGet]
        public JsonResult LoadDeptStaff(string id)
        {
            List<CommitteeDepartmentVM> deptViewModel = new List<CommitteeDepartmentVM>();
            var result = setupUnitOfWork.Staff.GetAll().Where(m => m.DeptCode == id);



            foreach (var item in result)
            {
                var departmentalStaff = new CommitteeDepartmentVM()
                {
                    id = item.Department,
                    text = item.StaffName
                };
                deptViewModel.Add(departmentalStaff);
            }
            return Json(deptViewModel);
        }




        [HttpGet]
        public JsonResult GetDepartment()
        {

            var list = new List<SelectTwoContentM>();
            var result = setupUnitOfWork.Staff.GetAll().Select(o => new { DeptCode = o.DeptCode, Department = o.Department }).Distinct();

            foreach (var item in result)
            {

                list.Add(new SelectTwoContentM
                {
                    id = Convert.ToString(item.DeptCode),
                    text = item.Department,

                });

            }

            return Json(list);
        }






        public class SelectTwoContentM
        {
            public string id { get; set; }
            public string text { get; set; }
        }
    }
}

