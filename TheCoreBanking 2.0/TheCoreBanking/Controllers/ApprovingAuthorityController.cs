﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using TheCoreBanking.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheCoreBanking.Data.Helpers;

namespace TheCoreBanking.Controllers
{
    public class ApprovingAuthorityController : Controller
    {

        private readonly TheCoreBankingContext _context;
        public ApprovingAuthorityController(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }





        public IActionResult Index()
        {
            return View();
        }




        [HttpGet]
        public JsonResult LoadOppApproval()
        {
            var result = setupUnitOfWork.OpeApprove.GetAll();

            List<OperationApprovalVM> OperationAppVM = new List<OperationApprovalVM>();

            foreach (var item in result)
            {
                OperationApprovalVM opViewModel = new OperationApprovalVM();
                var operations = setupUnitOfWork.OperationLevel.GetAll().Where(i => i.OperationId == item.OperationId).Select(m => m.Operations).FirstOrDefault();
                var levels = setupUnitOfWork.OperationLevel.GetAll().Where(i => i.OperationId == item.OperationId).Select(m => m.ApprovalLevels).FirstOrDefault();
                opViewModel.Id = item.Id;
                opViewModel.MaxAmt = item.MaxAmt;
                opViewModel.MinAmt = item.MinAmt;
                opViewModel.Designation = item.Designation;
                opViewModel.CummulativeAmt = item.CummulativeAmt;
                opViewModel.ApprovingAuthority = item.ApprovingAuthority;
                opViewModel.ApprovingLevel = item.ApprovingLevel;
                opViewModel.StaffId = item.StaffId;
                opViewModel.BranchCode = item.BranchCode;
                opViewModel.CompanyCode = item.CompanyCode;
                opViewModel.CreatedBy = item.CreatedBy;

                opViewModel.Operations = operations;
                ViewBag.Message = levels;


                OperationAppVM.Add(opViewModel);

            }
            return Json(OperationAppVM);


        }



        [HttpPost]
        public JsonResult AddOpeOthers(OperationApprovalVM opeothers)
        {

            Data.Models.TblOperationApproval addOP = new Data.Models.TblOperationApproval();

            var count = (setupUnitOfWork.OpeApprove.GetAll().Where(o => o.ApprovingAuthority == opeothers.StaffName && o.OperationId == opeothers.OperationId).Select(o => o.CommitteeType).Count());


            if (count > 0)
            {

                return Json(false);
            }

            else
            {
                var loguser = User.Identity.Name;
                if (loguser == null)
                {
                    loguser = "tayo.olawumi";
                }
                else
                {
                    loguser = User.Identity.Name;
                }

                var branch = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().BranchId;

                var company = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().CompanyId;

                var staffno = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == opeothers.StaffName).FirstOrDefault().StaffNo;

                addOP.CreatedBy = loguser;
                addOP.ApprovingAuthority = opeothers.StaffName;
                addOP.ApprovingLevel = opeothers.ApprovingLevel;
                addOP.BranchCode = branch;
                addOP.CommitteeType = "O";
                addOP.CompanyCode = company;
                addOP.CreateDate = DateTime.Now;
                addOP.CummulativeAmt = opeothers.CummulativeAmt;
                addOP.Designation = opeothers.Designation;
                addOP.MaxAmt = opeothers.MaxAmt;
                addOP.MinAmt = opeothers.MinAmt;
                addOP.OperationId = opeothers.OperationId;
                addOP.StaffId = staffno;
                addOP.Approved = false;
                addOP.Disapproved = false;

                setupUnitOfWork.OpeApprove.Add(addOP);
                setupUnitOfWork.Commit();

                //return Json(opeothers);
                return Json(true);
            }
            // return Json(new { message = ViewBag.Reply });
        }        






        [HttpGet]
        public JsonResult LoadOperate()
        {

            var list = new List<SelectTwoContentM>();
            var result = setupUnitOfWork.OperationLevel.GetAll();


            foreach (var item in result)
            {

                list.Add(new SelectTwoContentM
                {
                    id = Convert.ToString(item.OperationId),
                    text = item.Operations,
                });

            }

            return Json(list);
        }



        [HttpGet]
        public JsonResult AppLevel(int id)
        {
            List<ApplevelVM> applevelViewModel = new List<ApplevelVM>();
            var result = setupUnitOfWork.OperationLevel.GetAll().Where(m => m.OperationId == id);

            List<int> applevelForSelect2Input = new List<int>();

            var count = result.FirstOrDefault().ApprovalLevels;

            for (var i = 1; i <= count; i++)
            {

                var approvinglev = new ApplevelVM()
                {
                    id = i.ToString(),
                    text = Convert.ToString(i)
                };
                applevelViewModel.Add(approvinglev);
            }

            //foreach (var item in result)
            //{
            //    var approvinglev = new ApplevelVM()
            //    {
            //        id = item.Operations,
            //        text = Convert.ToString(item.ApprovalLevels)
            //    };
            //    applevelViewModel.Add(approvinglev);
            //}
            return Json(applevelViewModel);
        }



        [HttpGet]
        public JsonResult GetDesignation()
        {

            var list = new List<SelectTwoContentM>();
            var result = setupUnitOfWork.Designation.GetAll();


            foreach (var item in result)
            {

                list.Add(new SelectTwoContentM
                {
                    id = item.Id.ToString(),
                    text = item.Designation

                });

            }

            return Json(list);
        }



        [HttpGet]
        public JsonResult LoadStaff(int id)
        {
            List<OperateAppSetupVM> OpeViewModel = new List<OperateAppSetupVM>();
            var result = setupUnitOfWork.Staff.GetAll().Where(m => m.DesignationCode == id);
            //var results = setupUnitOfWork.Designation.GetAll().Where(m => m.Id == result.DesignationCode);



            foreach (var item in result)
            {
                var departmentalStaff = new OperateAppSetupVM()
                {
                    id = setupUnitOfWork.Designation.GetAll().Where(m => m.Id == item.DesignationCode).Select(m => m.Designation).FirstOrDefault(),
                    text = item.StaffName,
                };
                OpeViewModel.Add(departmentalStaff);
            }
            return Json(OpeViewModel);
        }





        public class SelectTwoContentM
        {
            public string id { get; set; }
            public string text { get; set; }
        }
    }
}
