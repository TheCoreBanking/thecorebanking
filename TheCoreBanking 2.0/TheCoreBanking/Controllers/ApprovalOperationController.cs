﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using TheCoreBanking.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheCoreBanking.Data.Helpers;

namespace TheCoreBanking.Controllers
{
    public class ApprovalOperationController : Controller
    {

        private readonly TheCoreBankingContext _context;
        public ApprovalOperationController(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }



        public IActionResult Index()
        {
            return View();
        }


        public JsonResult listOperatelevel()
        {

            var results = setupUnitOfWork.OperationLevel.GetAll();

            return Json(results);


        }




        [HttpPost]
        public JsonResult AddOperatelevel(Data.Models.TblOperationsLevel opelevel)
        {
            setupUnitOfWork.OperationLevel.Add(opelevel);
            setupUnitOfWork.Commit();

            return Json(true);

        }








    }
}
