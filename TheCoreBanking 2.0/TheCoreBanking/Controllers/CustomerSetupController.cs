﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;


namespace TheCoreBanking.Controllers
{
    public class CustomerSetupController : Controller
    {
        string[] misType = { "Account Officer", "Team", "Group", "Branch", "Head office", "Segment" };
        TheCoreBankingContext _context=new TheCoreBankingContext();
        public CustomerSetupController(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }


        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.misType = misType;
            return View();
        }
 



        public JsonResult AddMIS(TblMisinformation misinformation)
        {
            misinformation.DateCreated = DateTime.Now;
            setupUnitOfWork.MIS.Add(misinformation);
            setupUnitOfWork.Commit();
            return Json(misinformation.Id);
        }
        public JsonResult UpdateMIS(int Id, TblMisinformation misinformation)
        {
            setupUnitOfWork.MIS.Update(misinformation);
            setupUnitOfWork.Commit();
            return Json(misinformation.Id);
        }

        public IActionResult RemoveMIS(TblMisinformation misinformation)
        {
            setupUnitOfWork.MIS.Delete(misinformation);
            setupUnitOfWork.Commit();
            return Json(misinformation.Id);
        }
        [HttpPost]
        public JsonResult AddDepartment(long id, [Bind("Id,CoyId,Department,Remark,DeptCode")] TblDepartment tblDepartment)
        {

            //TheCoreBankingContext context = new TheCoreBankingContext();
            var checkDeptCode = setupUnitOfWork.Department.GetAll().Where(o => o.DeptCode == tblDepartment.DeptCode).Count();
            if(checkDeptCode > 0)
            {
                return Json("");
            }
            else
            {
                setupUnitOfWork.Department.Add(tblDepartment);
                setupUnitOfWork.Commit();
                return Json(tblDepartment.Id);
            }
            
        }
        public JsonResult AddDepartmentBranch(int Department, int BranchId)
        {
            var logInUser = User.Identity.Name;
            if (logInUser == null)
            {
                logInUser = "tayo.olawumi";
            }
            else
            {
                logInUser = User.Identity.Name;
            }
            var staff = _context.TblStaffInformation.Where(o => o.StaffName == logInUser).FirstOrDefault();
            var getDepartment = setupUnitOfWork.Department.GetAll().Where(o=>o.Id == Department).FirstOrDefault();
            var getBranch = setupUnitOfWork.Branch.GetAll().Where(o => o.Id== BranchId).FirstOrDefault();
            TblBranchDepartmentUnit Dept = new TblBranchDepartmentUnit
            {
                 BranchId=getBranch.Id.ToString(),
                 CoyId=staff.CompanyId,
                 DeptCode=getDepartment.DeptCode,
                 Department=getDepartment.Department
                    
            };
            _context.TblBranchDepartmentUnit.Add(Dept);
            _context.SaveChanges();
            return Json("");
        }

        public JsonResult AddUnitDepartment(int Department, int UnitId)
        {
            var logInUser = User.Identity.Name;
            if (logInUser == null)
            {
                logInUser = "tayo.olawumi";
            }
            else
            {
                logInUser = User.Identity.Name;
            }
            var staff = _context.TblStaffInformation.Where(o => o.StaffName == logInUser).FirstOrDefault();
            var getDepartment = setupUnitOfWork.Department.GetAll().Where(o => o.Id == Department).FirstOrDefault();           
            var getUnit= setupUnitOfWork.Unit.GetAll().Where(o => o.Id == UnitId).FirstOrDefault();
            var mapped = _context.TblBranchDepartmentUnit.Where(o => o.Department == getDepartment.Department).FirstOrDefault();
            var mappeds = _context.TblBranchDepartmentUnit.Where(o => o.Department == getDepartment.Department).Count();
            if (mappeds > 0)
            {
                TblBranchDepartmentUnit Dept = new TblBranchDepartmentUnit
                {
                    BranchId = mapped.BranchId,
                    CoyId = mapped.CoyId,
                    DeptCode = mapped.DeptCode,
                    Department = mapped.Department,
                    UnitCode = getUnit.UnitCode,
                    UnitName = getUnit.UnitName,
                 

                };
                _context.TblBranchDepartmentUnit.Add(Dept);
                _context.SaveChanges();
                _context.TblBranchDepartmentUnit.Remove(mapped);
                _context.SaveChanges();
                return Json(new {message= "Successful" });
            }
            else
            {
                return Json("Department should be map to branch first");
            }
       
        }
        public JsonResult listbranch()
        {
            var result = setupUnitOfWork.Branch.GetAll().Where(o => o.Deleted == false).OrderByDescending(o => o.Id);
            return Json(result);
        }

        public JsonResult UpdateDepartment(long id, [Bind("Id,CoyId,Department,Remark,DeptCode")] TblDepartment tblDepartment)
        {
            setupUnitOfWork.Department.Update(tblDepartment);
            setupUnitOfWork.Commit();
            return Json(tblDepartment.Id);
        }

        public IActionResult RemoveDepartment(TblDepartment department)
        {
            setupUnitOfWork.Department.Delete(department);
            setupUnitOfWork.Commit();
            return Json(department.Id);
        }

        public JsonResult AddDesignation(long id, [Bind("Id,Designation,DesignationCode")] TblDesignation tblDesignation)
        {
            var checkDesCode = setupUnitOfWork.Designation.GetAll().Where(o => o.DesignationCode == tblDesignation.DesignationCode).Count();
            if (checkDesCode > 0)
            {
                return Json("");
            }
            else
            {
                setupUnitOfWork.Designation.Add(tblDesignation);
                setupUnitOfWork.Commit();
                return Json(tblDesignation.Id);
            }
  
        }
        public JsonResult AddUnit(long id, [Bind("Id,UnitName,UnitCode,Remark")] TblUnit tblUnit)
        {
            setupUnitOfWork.Unit.Add(tblUnit);
            setupUnitOfWork.Commit();
            return Json(tblUnit.Id);
        }
        public JsonResult UpdateUnit(int id, [Bind("Id,UnitName,UnitCode,Remark")] TblUnit tblUnit)
        {
            setupUnitOfWork.Unit.Update(tblUnit);
            setupUnitOfWork.Commit();
            return Json(tblUnit.Id);
        }
        public IActionResult RemoveUnit(TblUnit unit)
        {
            setupUnitOfWork.Unit.Delete(unit);
            setupUnitOfWork.Commit();
            return Json(unit.Id);
        }
        public JsonResult UpdateDesignation(long id, [Bind("Id,Designation")] TblDesignation tblDesignation)
        {
            setupUnitOfWork.Designation.Update(tblDesignation);
            setupUnitOfWork.Commit();
            return Json(tblDesignation.Id);
        }

        public IActionResult RemoveDesignation(TblDesignation designation)
        {
            setupUnitOfWork.Designation.Delete(designation);
            setupUnitOfWork.Commit();
            return Json(designation.Id);
        }

        public JsonResult RankCode(string Rank)
        {
            var checkCompany = setupUnitOfWork.Rank.GetAll().Where(o => o.Rank.ToString() == Rank).Count();
            if (string.IsNullOrEmpty(Rank))
                return Json("Rank cannot be null");
            if (checkCompany > 0)

                return Json("Rank already exist");
            else
                return Json("");
        }
        public JsonResult UnitCode(string UnitCode)
        {
            var checkCompany = setupUnitOfWork.Unit.GetAll().Where(o => o.UnitCode.ToString() == UnitCode).Count();
            if (string.IsNullOrEmpty(UnitCode))
                return Json("Unit code cannot be null");
            if (checkCompany > 0)

                return Json("Unit code already exist");
            else
                return Json("");
        }
        public JsonResult UnitName(string UnitName)
        {
            var checkCompany = setupUnitOfWork.Unit.GetAll().Where(o => o.UnitName == UnitName).Count();
            if (string.IsNullOrEmpty(UnitName))
                return Json("The unit name cannot be null");
            if (checkCompany > 0)

                return Json("The unit name already exist");
            else
                return Json("");
        }
        public JsonResult DeptCode(string DeptCode)
        {
            var checkCompany = setupUnitOfWork.Department.GetAll().Where(o => o.DeptCode.ToString() == DeptCode).Count();
            if (string.IsNullOrEmpty(DeptCode))
                return Json("Dept code cannot be null");
            if (checkCompany > 0)

                return Json("Dept code already exist");
            else
                return Json("");
        }
        public JsonResult DeptName(string Department)
        {
            var checkCompany = setupUnitOfWork.Department.GetAll().Where(o => o.Department == Department).Count();
            if (string.IsNullOrEmpty(Department))
                return Json("Department name cannot be null");
            if (checkCompany > 0)

                return Json("Department name already exist");
            else
                return Json("");
        }
        public JsonResult AddRank(string Rank)
        {
            TblRank rank = new TblRank();
            rank.Rank = Rank;
            setupUnitOfWork.Rank.Add(rank);
            setupUnitOfWork.Commit();
            return Json(rank.Id);
        }
        public JsonResult UpdateRank(int Id, string Rank)
        {
            TblRank rank = new TblRank();
            rank.Rank = Rank;
            rank.Id = Id;
            setupUnitOfWork.Rank.Update(rank);
            setupUnitOfWork.Commit();
            return Json(rank.Id);
        }

        public IActionResult RemoveRank(TblRank rank)
        {
            setupUnitOfWork.Rank.Delete(rank);
            setupUnitOfWork.Commit();
            return Json(rank.Id);
        }
        #region





        #endregion
        #region Fetch Company Data
        //For Dropdown list for company() box Select2 starts here
        public JsonResult loadCompany()
        {
            Select2Format loadFormat = new Select2Format();
            var result = setupUnitOfWork.Company.GetAll();
            List<SelectContent> list = new List<SelectContent>();

            foreach (var item in result)
            {
                SelectContent load = new SelectContent()
                {
                    id = item.Id.ToString(),
                    text = item.CoyName
                };

                list.Add(load);
            }
            loadFormat.results = list;
            return Json(loadFormat);

        }

        #region Select2 Helper

        public class Select2Format
        {
            public List<SelectContent> results { get; set; }
        }
        public class SelectContent
        {
            public string id { get; set; }
            public string text { get; set; }
        }
        #endregion

        public JsonResult listmis()
        {
            var result = setupUnitOfWork.MIS.GetAll().OrderByDescending(o => o.Id);
            return Json(result);
        }
    
        public JsonResult listdepartment()
        {
            var result = setupUnitOfWork.Department.GetAll().OrderByDescending(o=>o.Id);
            return Json(result);
        }
        public JsonResult listdesignation()
        {
            var result = setupUnitOfWork.Designation.GetAll().OrderByDescending(o => o.Id);
            return Json(result);
        }
        public JsonResult listrank()
        {
            var result = setupUnitOfWork.Rank.GetAll().OrderByDescending(o => o.Id); 
            return Json(result);
        }
        public JsonResult listunit()
        {
            var result = setupUnitOfWork.Unit.GetAll().OrderByDescending(o => o.Id);
            return Json(result);
        }
        #endregion
    }
}
