﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Controllers
{
    public class NarrationSetup : Controller
    {
       
        public NarrationSetup(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }

        public IActionResult Index()
        {
            return View();
        }


        public JsonResult ListNarration()
        {
            var result = setupUnitOfWork.NarrateComment.GetAll();

            return Json(result);
        }


        [HttpPost]
        public JsonResult Create(GeneralSetupTblOperationComment ope)
        {


            //callermemo.CallMemoDate = DateTime.Now.ToString("yyyy-MM-dd");

            //callermemo.CallMemoDate = formatDate;



            var loguser = User.Identity.Name;
            if (loguser == null)
            {
                loguser = "tayo.olawumi";
            }
            else
            {
                loguser = User.Identity.Name;
            }

            var branch = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().BranchId;

            var company = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().CompanyId;

            ope.BrCode = branch;
            ope.CoyCode = company;
            ope.CreatedBy = loguser;
            setupUnitOfWork.NarrateComment.Add(ope);
            setupUnitOfWork.Commit();


            return Json(ope);

        }




        //public JsonResult Edit(int id)
        //{
        //    var result = setupUnitOfWork.OpeComment.GetById(id);
        //    return Json(result);
        //}




        // POST: CallMemoSetUp/Edit/5
        [HttpPost]
        public JsonResult Edit(GeneralSetupTblOperationComment opec)
        {


            var loguser = User.Identity.Name;
            if (loguser == null)
            {
                loguser = "tayo.olawumi";
            }
            else
            {
                loguser = User.Identity.Name;
            }

            var branch = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().BranchId;

            var company = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().CompanyId;

            opec.CreatedBy = loguser;
            opec.BrCode = branch;
            opec.CoyCode = company;
            setupUnitOfWork.NarrateComment.Update(opec);
            setupUnitOfWork.Commit();
            return Json(opec.Id);
        }



        [HttpPost]
        public JsonResult Delete(GeneralSetupTblOperationComment opeco)
        {
            setupUnitOfWork.NarrateComment.Delete(opeco);
            setupUnitOfWork.Commit();
            return Json(opeco.Id);
        }




        public class SelectTwoContentM
        {
            public string id { get; set; }
            public string text { get; set; }
        }



    }
}
