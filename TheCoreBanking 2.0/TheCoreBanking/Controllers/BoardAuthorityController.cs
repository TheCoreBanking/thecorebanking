﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using TheCoreBanking.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheCoreBanking.Data.Helpers;


namespace TheCoreBanking.Controllers
{
    public class BoardAuthorityController : Controller
    {
        private readonly TheCoreBankingContext _context;
        public BoardAuthorityController(ISetupUnitOfWork uowSetup)
        {
            setupUnitOfWork = uowSetup;
        }

        public ISetupUnitOfWork setupUnitOfWork { get; set; }




        public IActionResult Index()
        {
            return View();
        }




        [HttpGet]
        public JsonResult listCommitBoard()
        {
            var result = setupUnitOfWork.ApprovalCommittee.GetAll().Where(o => o.CommitteeType == "B");

            List<CommitteeApprovalVM> BoardAppVM = new List<CommitteeApprovalVM>();

            foreach (var item in result)
            {
                CommitteeApprovalVM brdViewModel = new CommitteeApprovalVM();
                var operations = setupUnitOfWork.OperationLevel.GetAll().Where(i => i.OperationId == item.OperationId).Select(m => m.Operations).FirstOrDefault();
                brdViewModel.Id = item.Id;
                brdViewModel.CreditMaxBoardAmt = item.CreditMaxBoardAmt;
                brdViewModel.CreditMinBoard = item.CreditMinBoard;
                brdViewModel.Department = item.Department;
                brdViewModel.CummulativeAmt = item.CummulativeAmt;
                brdViewModel.ApprovingAuthority = item.ApprovingAuthority;
                brdViewModel.ApprovingLevel = item.ApprovingLevel;
                brdViewModel.StaffId = item.StaffId;
                brdViewModel.BranchCode = item.BranchCode;
                brdViewModel.CompanyCode = item.CompanyCode;
                //brdViewModel.CreatedBy = item.CreatedBy;
                brdViewModel.BoardNo = item.BoardNo;


                brdViewModel.Operations = operations;

                BoardAppVM.Add(brdViewModel);

            }

            return Json(BoardAppVM);

        }





        [HttpPost]
        public JsonResult AddComBoard(CommitteeApprovalVM bod)
        {

            Data.Models.TblOperationApprovalCommittee addbod = new Data.Models.TblOperationApprovalCommittee();
            var bodcomm = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId);
            if (bodcomm.Count() > 0)
            {
                var count = (setupUnitOfWork.ApprovalCommittee.GetAll().Where(o => o.ApprovingAuthority == bod.StaffName && o.OperationId == bod.OperationId && o.CommitteeType.Contains("B")).Select(o => o.CommitteeType).Count());
                //var checkLevel = (setupUnitOfWork.OpeApprove.GetAll().Where(o => o.OperationId == opeothers.OperationId && o.CommitteeType == "O" && o.ApprovingLevel == opeothers.ApprovingLevel).Count());
                // var levels = setupUnitOfWork.OperationLevel.GetAll().Where(i => i.OperationId == opeothers.OperationId).Select(m => m.ApprovalLevels).FirstOrDefault();
                if (count > 0)
                {
                    return Json(false);
                }


                else
                {
                    var loguser = User.Identity.Name;
                    if (loguser == null)
                    {
                        loguser = "tayo.olawumi";
                    }
                    else
                    {
                        loguser = User.Identity.Name;
                    }

                    var branch = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().BranchId;

                    var company = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == loguser).FirstOrDefault().CompanyId;

                    var staffno = setupUnitOfWork.Staff.GetAll().Where(i => i.StaffName == bod.StaffName).FirstOrDefault().StaffNo;

                    var mannum = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().MgtNo;

                    var mgtmin = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().CreditMinMgt;

                    var mgtmax = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().CreditMaxComteeAmt;

                    var bodno = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().BoardNo;

                    var bodappnum = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().BoardNoApproval;

                    var mgtappno = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().MgtNoApproval;

                    var credbodminamt = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().CreditMinBoard;

                    var credbodmax = setupUnitOfWork.Committee.GetAll().Where(i => i.OperationId == bod.OperationId).FirstOrDefault().CreditMaxBoardAmt;

                    addbod.CreatedBy = loguser;
                    addbod.ApprovingAuthority = bod.StaffName;
                    addbod.ApprovingLevel = bod.ApprovingLevel;
                    addbod.BranchCode = branch;
                    addbod.CommitteeType = "B";
                    addbod.CompanyCode = company;
                    addbod.CreateDate = DateTime.Now;
                    addbod.CummulativeAmt = bod.CummulativeAmt;
                    addbod.Department = bod.Department;
                    addbod.MaxAmt = bod.MaxAmt;
                    addbod.MinAmt = bod.MinAmt;
                    addbod.BoardNo = bodno;
                    addbod.CreditMinBoard = credbodminamt;
                    addbod.CreditMaxBoardAmt = credbodmax;
                    addbod.BoardNoApproval = bodappnum;
                    addbod.MgtNoApproval = mgtappno;
                    addbod.MgtNo = mannum;
                    addbod.CreditMinMgt = mgtmin;
                    addbod.CreditMaxComteeAmt = mgtmax;
                    addbod.OperationId = bod.OperationId;
                    addbod.StaffId = staffno;
                    addbod.Approved = false;
                    addbod.Disapproved = false;


                    setupUnitOfWork.ApprovalCommittee.Add(addbod);
                    setupUnitOfWork.Commit();

                    return Json(true);
                }
            }
            else
            {
                return Json(false);
            }
        }



        [HttpGet]
        public JsonResult GetOperation()
        {

            var list = new List<SelectTwoContentM>();

            var result = setupUnitOfWork.OperationLevel.GetAll();
            foreach (var item in result)
            {
                list.Add(new SelectTwoContentM
                {
                    id = Convert.ToString(item.OperationId),
                    text = item.Operations,
                });
            }
            return Json(list);
        }




        [HttpGet]
        public JsonResult AppLevel(int id)
        {
            List<ApplevelVM> applevelViewModel = new List<ApplevelVM>();
            var result = setupUnitOfWork.OperationLevel.GetAll().Where(m => m.OperationId == id);

            List<int> applevelForSelect2Input = new List<int>();

            var count = result.FirstOrDefault().ApprovalLevels;

            for (var i = 1; i <= count; i++)
            {

                var approvinglev = new ApplevelVM()
                {
                    id = i.ToString(),
                    text = Convert.ToString(i)
                };
                applevelViewModel.Add(approvinglev);
            }

            //foreach (var item in result)
            //{
            //    var approvinglev = new ApplevelVM()
            //    {
            //        id = item.Operations,
            //        text = Convert.ToString(item.ApprovalLevels)
            //    };
            //    applevelViewModel.Add(approvinglev);
            //}
            return Json(applevelViewModel);
        }



        [HttpGet]
        public JsonResult LoadDeptStaff(string id)
        {
            List<CommitteeDepartmentVM> deptViewModel = new List<CommitteeDepartmentVM>();
            var result = setupUnitOfWork.Staff.GetAll().Where(m => m.DeptCode == id);



            foreach (var item in result)
            {
                var departmentalStaff = new CommitteeDepartmentVM()
                {
                    id = item.Department,
                    text = item.StaffName
                };
                deptViewModel.Add(departmentalStaff);
            }
            return Json(deptViewModel);
        }




        [HttpGet]
        public JsonResult GetDepartment()
        {

            var list = new List<SelectTwoContentM>();
            var result = setupUnitOfWork.Staff.GetAll().Select(o => new { DeptCode = o.DeptCode, Department = o.Department }).Distinct();

            foreach (var item in result)
            {

                list.Add(new SelectTwoContentM
                {
                    id = Convert.ToString(item.DeptCode),
                    text = item.Department,

                });

            }

            return Json(list);
        }









        public class SelectTwoContentM
        {
            public string id { get; set; }
            public string text { get; set; }
        }


    }
}
