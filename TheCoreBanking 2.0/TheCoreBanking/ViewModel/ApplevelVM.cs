﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCoreBanking.ViewModel
{
    public class ApplevelVM
    {
        public string id { get; set; }
        public string text { get; set; }
        public int? ApprovalLevels { get; set; }
    }
}
