﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCoreBanking.ViewModel
{
    public class OperateAppSetupVM
    {
        public string id { get; set; }
        public string text { get; set; }
        public string StaffName { get; set; }
    }
}
