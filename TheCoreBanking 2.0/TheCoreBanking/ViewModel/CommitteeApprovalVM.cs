﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCoreBanking.ViewModel
{
    public class CommitteeApprovalVM
    {
        public int Id { get; set; }
        public bool? Committee { get; set; }
        public int? BoardNo { get; set; }
        public int? MgtNo { get; set; }
        public decimal? CreditMinMgt { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string Createdby { get; set; }
        public DateTime? CreateDate { get; set; }
        public decimal? CreditMinBoard { get; set; }
        public int? MgtNoApproval { get; set; }
        public int? BoardNoApproval { get; set; }
        public int OperationId { get; set; }
        public string CommitteeType { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string ApprovedBy { get; set; }
        public string Comment { get; set; }
        public string ApprovingAuthority { get; set; }
        public string StaffId { get; set; }
        public int Department { get; set; }
        public int? ApprovingLevel { get; set; }
        public string CompanyCode { get; set; }
        public string BranchCode { get; set; }
        public string Miscode { get; set; }
        public decimal? CummulativeAmt { get; set; }
      
        public decimal? CreditMaxComteeAmt { get; set; }
        public decimal? CreditMaxBoardAmt { get; set; }
        public string Operations { get; set; }
        public string StaffName { get; set; }
        public string StaffNo { get; set; }
        public decimal? MinAmt { get; set; }
        public decimal? MaxAmt { get; set; }
    }
}
