﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCoreBanking.ViewModel
{
    public class CurrencyViewModel
    {
        public long Id { get; set; }
        public int? CurrCode { get; set; }
        public string CurrName { get; set; }
        public string CurrSymbol { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string CountryCode { get; set; }
        public decimal? AverageRate { get; set; }
        public DateTime? DateUpdated { get; set; }

        //public long Id { get; set; }
        //public int? CurrCode { get; set; }
        //public string CurrName { get; set; }
        //public string CurrSymbol { get; set; }
        //public decimal?ExchangeRate { get; set; }
        //public string CountryCode { get; set; }
        //public decimal? AverageRate { get; set; }
        //public DateTime? DateUpdated { get; set; }
    }


}
