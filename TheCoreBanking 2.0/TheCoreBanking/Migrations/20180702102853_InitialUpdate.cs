﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TheCoreBanking.Migrations
{
    public partial class InitialUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_TblStaffInformation_tblStaffInformationId",
                table: "AspNetUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TblStaffInformation",
                table: "TblStaffInformation");

            migrationBuilder.EnsureSchema(
                name: "GeneralSetup");

            migrationBuilder.RenameTable(
                name: "TblStaffInformation",
                newName: "tbl_StaffInformation",
                newSchema: "GeneralSetup");

            migrationBuilder.RenameColumn(
                name: "PcCode",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                newName: "pcCode");

            migrationBuilder.RenameColumn(
                name: "Miscode",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                newName: "MISCode");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                newName: "CompanyID");

            migrationBuilder.RenameColumn(
                name: "BranchId",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                newName: "BranchID");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                newName: "ID");

            migrationBuilder.AlterColumn<string>(
                name: "UnitCode",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Unit",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "State",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "Staffsignature",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                type: "image",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StaffNo",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StaffName",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RelationShip",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Rank",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Phone",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "pcCode",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinPhone",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinName",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinGender",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinEmail",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinAddress",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nationality",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MISCode",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "JobTitle",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DeptCode",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Department",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyID",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Comment",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BranchID",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Age",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                type: "datetime",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                unicode: false,
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_tbl_StaffInformation",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_tbl_StaffInformation_tblStaffInformationId",
                table: "AspNetUsers",
                column: "tblStaffInformationId",
                principalSchema: "GeneralSetup",
                principalTable: "tbl_StaffInformation",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_tbl_StaffInformation_tblStaffInformationId",
                table: "AspNetUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_tbl_StaffInformation",
                schema: "GeneralSetup",
                table: "tbl_StaffInformation");

            migrationBuilder.RenameTable(
                name: "tbl_StaffInformation",
                schema: "GeneralSetup",
                newName: "TblStaffInformation");

            migrationBuilder.RenameColumn(
                name: "pcCode",
                table: "TblStaffInformation",
                newName: "PcCode");

            migrationBuilder.RenameColumn(
                name: "MISCode",
                table: "TblStaffInformation",
                newName: "Miscode");

            migrationBuilder.RenameColumn(
                name: "CompanyID",
                table: "TblStaffInformation",
                newName: "CompanyId");

            migrationBuilder.RenameColumn(
                name: "BranchID",
                table: "TblStaffInformation",
                newName: "BranchId");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "TblStaffInformation",
                newName: "Id");

            migrationBuilder.AlterColumn<string>(
                name: "UnitCode",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Unit",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "State",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "Staffsignature",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldType: "image",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StaffNo",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StaffName",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RelationShip",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Rank",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Phone",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PcCode",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinPhone",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinName",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinGender",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinEmail",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NextOfKinAddress",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nationality",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Miscode",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "JobTitle",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DeptCode",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Department",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyId",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Comment",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BranchId",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Age",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "TblStaffInformation",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TblStaffInformation",
                table: "TblStaffInformation",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_TblStaffInformation_tblStaffInformationId",
                table: "AspNetUsers",
                column: "tblStaffInformationId",
                principalTable: "TblStaffInformation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
