﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}

var $Departmenttable = $('#departmentAssignTable'), $Branchtable = $('#BranchAssignTable'), $button = $('#btnMapDepartment')

function departmentFormatter(value, row, index) {
    return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edit btn btn-sm btn-info"  title="Edit Department">'
        + '<i class="fas fa-edit"></i>' +
        '<a style="color:white"  title="Remove Department" class="remove btn btn-sm btn-danger">'
        +'<i class="fas fa-trash"></i></a>'+
        '</a> '
    ].join('');
}

window.departmentEvents = {
    'click .edit': function (e, value, row, index) {
       
        if (row.state = true) {
          
            var data = JSON.stringify(row);

            $('#Id').val(row.id);        
            $('#DeptCode').val(row.deptCode).attr("disabled", "disabled"); 
            $('#CoyId').val(row.coyId);
            $('#Remarks').val(row.remark);
            $('#Department').val(row.department);
            $('#AddNewDepartment').modal('show'); 
            $('#btnDepartmentUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            $('#btnDepartmentUpdate').show();
            $('#btnDepartment').hide();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);
        var reg = {
            Id: $('#Id').val(),
            CoyId: $('#CoyId').val(),          
            Department:$('#Department').val(),
            DeptCode: $('#DeptCode').val(),
            Remark: $('#Remark').val()
           
        }
        debugger
        $('#ID').val(row.id);
        $.ajax({
            url: url_path + '/RemoveDepartment',
            type: 'POST',
            data: { ID: row.id},
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,           
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {


                      
                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#departmentTable').
                            bootstrapTable(
                                'refresh', { url: url_path +'/listdepartment' });

                        //return false;
                    }
                    else {
                        swal("Department", "You cancelled add department.", "error");
                    }
                    $('#departmentTable').
                            bootstrapTable(
                                'refresh', { url: url_path +'/listdepartment' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};
$(document).ready(function ($) {
    $('#btnDepartmentUpdate').hide();
    $('#btnDepartment').show();
    $('#btnDepartmentUpdate').on('click', function () {
        debugger
        updateDepartment();
    });

});
function updateDepartment() {
    debugger

    var json_data = {};
    json_data.Id = $('#Id').val();
    json_data.Department = $('#Department').val();
    json_data.DeptCode = $('#DeptCode').val();
    json_data.Remark = $('#Remarks').val();

    $("input[type=submit]").attr("disabled", "disabled");  

    $('#frmdepartment').validate({

        messages: {
            
            Department: { required: "Department Name is required" },
            DeptCode: { required: "Department Code is required" }
           
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Department will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnDepartmentUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: url_path +'/UpdateDepartment',
                        type: 'POST',
                        data: json_data,
                        dataType: 'json',
                        cache: false,
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                            
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Department', text: 'Department updated successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewDepartment').modal('show'); 
                                $('#departmentTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path +'/listdepartment' });

                                $("#btnDepartmentUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Department', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnDepartmentUpdate").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Department', text: 'Department update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnDepartmentUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Department', 'You cancelled department update.', 'error');
            $("#btnDepartmentUpdate").removeAttr("disabled");
        });

}

$(document).ready(function ($) {

    $('#btnDepartment').on('click', function () {
        debugger      
       
            addDepartment();   
 
    });

    $('#btnMapDepartments').on('click', function () {
        debugger

        MapDepartment();

    });
  
});
function addDepartment() {
    debugger
    $('#btnDepartmentUpdate').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmdepartment').validate({
        messages: {

            Department: { required: "Department Name is required" },
            DeptCode: { required: "Department Code is required" },
            Remark: {required:"Remark is required"}
       
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Department will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnDepartment").attr("disabled", "disabled");

                    debugger
                    var department_data = {
                        id: $('#Id').val(),
                        department: $('#Department').val(),
                        deptCode: $('#DeptCode').val(),
                        remark: $('#Remarks').val()

                    }
                           

                    $.ajax({
                        url: url_path + '/AddDepartment',
                        type: 'POST',
                        data: department_data,
                        dataType: "json",                      
                       
                        success: function (result) {
                            debugger
                            if (result.toString() != '') {
                                swal({ title: 'Add Department', text: 'Department added successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewDepartment').modal('show'); 
                                $('#departmentTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path +'/listdepartment' });

                                $("#btnDepartment").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Department', text: 'Something went wrong:' + "The Department Code already exist", type: 'error' }).then(function () { clear(); });
                                $("#btnDepartment").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Department', text: 'Adding department encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnDepartment").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Department', 'You cancelled add department.', 'error');
            $("#btnDepartment").removeAttr("disabled");
        });

}


function reloadpage() {
    location.reload();
}

function clear() {
    $('#Id').val('');
    $('#CoyId').val('');
 
    $('#Department').val('');
    $('#DeptCode').val('');
    $('#Remark').val('');

}

$('#departmentTable').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<h8>' +
        '<p style="text-align:left">' +
        '<strong>Company Id:</strong>&nbsp' + row.coyId + '' + '<p>' +
        ' <strong>Department: </strong>&nbsp' + row.department + '' + '<p>' +
        ' <strong>Remark: </strong>&nbsp' + row.remark + '' + '<p>' +
        ' <strong>Dept Code: </strong>&nbsp' + row.deptCode + '</div>';
       

    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});



function MapDepartment() {
    debugger
    var DepartmentData = $Departmenttable.bootstrapTable('getAllSelections');
    var branchData = $Branchtable.bootstrapTable('getAllSelections');
    var result = $('#txtComment');
    $("input[type=submit]").attr("disabled", "disabled");
    if (DepartmentData) {
        swal({
            title: "Are you sure?",
            text: "You are about to map the department to branch!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ff9800",
            confirmButtonText: "Yes, continue",
            cancelButtonText: "No, stop!",
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 4000);
                });
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                $("#btnApprovalCommittee").attr("disabled", "disabled");
                debugger
                $.each(DepartmentData, function (index, DepartmentItemData) {
                    debugger
                    $.each(branchData, function (index, branchItemData) {
                        $.ajax({                           
                            url: url_path + '/AddDepartmentBranch',
                            type: 'POST',
                            data: { BranchId: branchItemData.id, Department: DepartmentItemData.id },
                            dataType: "json",
                            success: function (result) {
                                debugger
                                if (result.toString != '' && result != null) {
                                    swal({ title: 'Map Dept to Branch', text: 'Map Dept to Branch applied successfully!', type: 'success' }).then(function () { clear(); });

                                    $('#departmentAssignTable').
                                        bootstrapTable(
                                            'refresh', { url: url_path + '/listdepartment' });
                                    $('#BranchAssignTable').
                                        bootstrapTable(
                                            'refresh', { url: url_path + '/listbranch' });


                                }
                                else {
                                    //alert(result.message);
                                    swal({ title: 'Map Dept to Branch', text: 'Something went wrong: ' + result.message, type: 'error' }).then(function () { clear(); });

                                }
                            },
                            error: function (e) {
                                swal({ title: 'Map Dept to Branch', text: 'Map Dept to Branch encountered an error', type: 'error' }).then(function () { clear(); });

                            }
                        });

                    })
                })
             }

        }),

            function (dismiss) {
                swal('Add Approving Interest Suspension ', 'You cancelled add interest suspension approval.', 'error');

            }
    }
}

$(document).ready(function ($) {
    debugger

    $('#DeptCode').mouseleave(function (event) {
        $.ajax({
            url: url_path + '/DeptCode',
            data: { DeptCode: $('#DeptCode').val() }
        }).then(function (result) {
            debugger
            if (result != "" && result != null) {
                $.notify({ icon: "add_alert", message: result.toString() }, { type: 'danger', timer: 1000 });
                $(this).css('border-color', 'red');
                $('#DeptCode').focus();
                return false;

            }
        })
    });

    $('#Department').mouseleave(function (event) {
        $.ajax({
            url: url_path + '/DeptName',
            data: { Department: $('#Department').val() }
        }).then(function (result) {
            debugger
            if (result != "" && result != null) {
                $.notify({ icon: "add_alert", message: result.toString() }, { type: 'danger', timer: 1000 });
                $(this).css('border-color', 'red');
                $('#Department').focus();
                return false;

            }
        })
    });
});
