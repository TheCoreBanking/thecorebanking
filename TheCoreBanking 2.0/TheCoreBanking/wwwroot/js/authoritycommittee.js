﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}



$(document).ready(function () {

    initDataTable();

    $(".modal").perfectScrollbar();
});


function reloadpage() {
    location.reload();
}




$(document).ready(function ($) {

    $("#btnclosemanage").on('click', function () {
        clearManagementContent();
    });

    $("#btnaddman").on('click', function () {
        clearManagementContent();
    });

});



function clearManagementContent() {

    $("#ManOperation").val('').trigger("change");

    $("#Mandepartment").val('').trigger("change");

    $("#Manauthority").val('').trigger("change");

    $("#ManculLimit").val('');

    $("#Manminamount").val('');

    $("#Manmaxamount").val('');

    $("#Manapplevel").val('');
}





$("#btnclearmanage").on("click", function () {
    clearM();
});




function clearM() {

    $("#ManOperation").val('').trigger("change");

    $("#Mandepartment").val('').trigger("change");

    $("#Manauthority").val('').trigger("change");

    $("#ManculLimit").val('');

    $("#Manminamount").val('');

    $("#Manmaxamount").val('');

    $("#Manapplevel").val('');
}




//MANAGEMENT COMMITTEE





function initDataTable() {
    debugger
    $("#data-table").bootstrapTable('showLoading');


    $("#data-table").bootstrapTable({
        search: true,
        searchAlign: "right",
        url: url_path + "/listCommitMgt",
        showPaginationSwitch: true,
        pagination: true,
        mobileResponsive: true,
        checkOnInit: true,
        minWidth: 626,
        toolbar: "#custom-toolbar",
        showRefresh: true,
        showToggle: true,
        buttonsClass: "danger",
        columns: [
            {
                field: "operations",
                title: "Operations",
                sortable: true
            },
            {

                field: "approvingAuthority",
                title: "Officer",
                sortable: true
            },

            {
                field: "staffId",
                title: "Staff No",
                sortable: true
            },
            {

                field: "department",
                title: "Department",
                sortable: true
            },


            {
                field: "creditMinMgt",
                title: "Min Mgt.Amount",
                sortable: true
            },
            {

                field: "creditMaxComteeAmt",
                title: "Max Mgt.Amount",
                sortable: true
            },

            {
                field: "cummulativeAmt",
                title: "Cul Amount",
                sortable: true
            },
            {

                field: "approvingLevel",
                title: "Approving Level",
                sortable: true
            },
        ],
    });



};




$(document).ready(function () {
    debugger
    $("#ManOperation").select2({
        theme: "bootstrap4",
        placeholder: "Loading..."

    });
    $.ajax({
        url: "../AuthorityCommittee/GetOperation",
    }).then(function (response) {
        debugger
        console.log(response);
        $("#ManOperation").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation",
            width: '100%',
            data: response,
            dropdownParent: $("#CreateManagement.modal"),
        });
    });
})





$("#ManOperation").on('select2:select', function (e) {
    debugger
    var OPId = e.params.data.id;
    $('#manapplevel').html('<option></option>')

    $.ajax({
        url: "../AuthorityCommittee/AppLevel",
        data: { id: OPId },

    }).then(function (response) {
        debugger
        console.log(response);
        $("#manapplevel").select2({
            theme: "bootstrap4",
            placeholder: "Select Approving level",
            width: '100%',
            data: response,
            dropdownParent: $("#CreateManagement"),
        });
    });

    console.log(data);
});







$(document).ready(function () {
    debugger
    $("#Mandepartment").select2({
        theme: "bootstrap4",
        placeholder: "Loading..."

    });
    $.ajax({
        url: "../AuthorityCommittee/GetDepartment",
    }).then(function (response) {
        debugger
        console.log(response);
        $("#Mandepartment").select2({
            theme: "bootstrap4",
            placeholder: "Select Department",
            width: '100%',
            data: response,
            dropdownParent: $("#CreateManagement.modal"),
        });
    });
})




$("#Mandepartment").on('select2:select', function (e) {
    debugger
    var deptId = e.params.data.id;
    $('#Manauthority').html('<option></option>')

    $.ajax({
        url: "../AuthorityCommittee/LoadDeptStaff",
        data: { id: deptId },

    }).then(function (response) {
        debugger
        console.log(response);
        $("#Manauthority").select2({
            theme: "bootstrap4",
            placeholder: "Select Staff",
            width: '100%',
            data: response,
            dropdownParent: $("#CreateManagement"),
        });
    });

    console.log(data);
});




$(document).ready(function () {
    $("#btnmanapp").click(function (e) {
        debugger
        e.preventDefault();
        var mgt = {};

        var data = $("#Manauthority").select2("data")

        mgt.StaffName = data[0].text;
        mgt.text = data[0].text;
        mgt.id = data[0].id;

        mgt.OperationId = $("#ManOperation").val();
        mgt.MaxAmt = $("#Manmaxamount").val();
        mgt.MinAmt = $("#Manminamount").val();
        mgt.Department = $("#Mandepartment").val();
        mgt.ApprovingLevel = $("#manapplevel").val();
        mgt.CummulativeAmt = $("#ManculLimit").val();

        $.ajax({
            url: "../AuthorityCommittee/AddComMgt",
            method: 'POST',
            dataType: "json",
            data: mgt,
            success: function (response) {
                if (response == true) {
                    swal({ title: 'Management Officer', text: 'Your Management Approving Officer has been Created Successfully!', type: 'success' }).then(function () { clear(); });

                    $("#data-table")
                        .bootstrapTable("refresh", {
                            url: url_path + "/AuthorityCommittee/listCommitMgt"
                        });
                    $("#btnmanapp").removeAttr("disabled");
                }
                else if (response == false) {
                    swal({ title: 'Management Officer', text: 'Management Approving Officer Has Already been profiled! or Management Committee does not exist!', type: 'success' }).then(function () { clear(); });
                }
                else {
                    swal({ title: 'Management Officer', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                    $("#btnmanapp").removeAttr("disabled");
                }
                $("#CreateManagement").modal("hide");
            },
            error: function (err) {
                alert(err);
            }
        })

        $("#CreateManagement").modal("hide");

    });
});




$("#data-table").on("expand-row.bs.table", function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<form id="Create-Group">' +
        '<div class="card"><div class="card-header">' +
        '<div class="content"><div class="card">' +
        '<div class="row"><div class="col-md-6">' +
        //'<strong style="color:#007E33"> Branch Code:</strong>  &nbsp' + row.BranchCode + '' +
        '<strong style="color:#007E33"> Branch Code:</strong> &nbsp' + row.branchCode + '</div>' +
        '<strong style="color:#007E33"> Company Code:</strong> &nbsp' + row.companyCode + '</div></div>' +
        '<div class="row"><div class="col-md-6">' +
        '<strong style="color:#007E33"> Created By:</strong> &nbsp' + row.createdBy + '</div></div>' +
        '<strong style="color:#007E33"> Board No:</strong> &nbsp' + row.boardNo + '</div></div>' +
        '<br/>' + '</form>'


    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});



