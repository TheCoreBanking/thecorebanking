﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}



$(document).ready(function () {

    //initDataTable();
    //initFormValidations();


    $(".modal").perfectScrollbar();

});





$(document).ready(function ($) {
    $("#comp").on('click', function () {
        clearModalcontent();
    });

    $("#btnAddCompanys").on('click', function () {
        clearModalcontent();
    });

});




function clearModalcontent() {
    $("#CoyName").val('');
    $("#CoyCode").val('');
    $("#Telephone").val('');
    $("#Fax").val('');
    $("#Email").val('');
    $("#DateOfIncorporation").val('');
    $("#Webbsite").val('');
    $("#Address").val('');
    $("#InvestmentObjective").val('');
    $("#ddlCompanyClass").val('').trigger("change");
    $("#ddlCompanyType").val('').trigger("change");
    $("#ddlmgtType").val('').trigger("change");
    $("#Manager").val('');
    $("#NatureOfBusiness").val('');
    $("#NameOfScheme").val('');
    $("#FunctionsRegistered").val('');
    $("#AuthorisedShareCapital").val('');
    $("#NameOfRegistrar").val('');
    $("#NameOfTrustees").val('');
    $("#FormerManagersTrustees").val('');
    $("#DateOfRenewalOfRegistration").val('');
    $("#DateOfCommencement").val('');
    $("#InitialFloatation").val('');
    $("#InitialSubscription").val('');
    $("#ddlacctTStand").val('').trigger("change");
    $("#EoyprofitAndLossGl").val('').trigger("change");
}




$("#btnclear").on("click", function () {
    clear();
});



function clear() {
    $("#CoyName").val('');
    $("#CoyCode").val('');
    $("#Telephone").val('');
    $("#Fax").val('');
    $("#Email").val('');
    $("#DateOfIncorporation").val('');
    $("#Webbsite").val('');
    $("#Address").val('');
    $("#InvestmentObjective").val('');
    $("#ddlCompanyClass").val('').trigger("change");
    $("#ddlCompanyType").val('').trigger("change");
    $("#ddlmgtType").val('').trigger("change");
    $("#Manager").val('');
    $("#NatureOfBusiness").val('');
    $("#NameOfScheme").val('');
    $("#FunctionsRegistered").val('');
    $("#AuthorisedShareCapital").val('');
    $("#NameOfRegistrar").val('');
    $("#NameOfTrustees").val('');
    $("#FormerManagersTrustees").val('');
    $("#DateOfRenewalOfRegistration").val('');
    $("#DateOfCommencement").val('');
    $("#InitialFloatation").val('');
    $("#InitialSubscription").val('');
    $("#ddlacctTStand").val('').trigger("change");
    $("#EoyprofitAndLossGl").val('').trigger("change");
}




//function totalPriceFormatter(data) {
//    var total = 0;
//    $.each(data, function (i, row) {
//        total += +(row.price.substring(1));
//    });
//    return '$' + total;
//}
function companyFormatter(value, row, index) {
    return [
        '<div class="btn-group">'+
        '<a style="color:white" class="edit btn btn-sm  btn-info"  title="Edit Company">'
        + '<i class="fas fa-edit"></i>' +
        '<a style="color:white"  title="Remove company" class="remove btn btn-sm btn-danger">'
        + '<i class="fas fa-trash"></i></a>' +
        '</a> '
    ].join('');
}

$('.datetimepicker').datetimepicker({
    format: "DD MMMM, YYYY",
    icons: {
        time: "now-ui-icons tech_watch-time",
        date: "now-ui-icons ui-1_calendar-60",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'now-ui-icons arrows-1_minimal-left',
        next: 'now-ui-icons arrows-1_minimal-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    }
});

window.companyEvents = {
    'click .edit': function (e, value, row, index) {
        if (row.state = true) {
            var data = JSON.stringify(row);
            $('#Id').val(row.id);
            $('#CoyId').val(row.coyId);
            $('#CoyName').val(row.coyName);
            $('#Address').val(row.address);
            $('#Telephone').val(row.telephone);
            $('#CoyCode').val(row.coyCode);
            $('#Fax').val(row.fax);
            $('#Email').val(row.email);
            $('#DateOfIncorporation').val(row.dateOfIncorporation);
            $('#Manager').val(row.manager);
            $('#NatureOfBusiness').val(row.natureOfBusiness);
            $('#NameOfScheme').val(row.nameOfScheme);
            $('#FunctionsRegistered').val(row.functionsRegistered);
            $('#AuthorisedShareCapital').val(row.authorisedShareCapital);
            $('#NameOfRegistrar').val(row.nameOfRegistrar);       
            $('#NameOfTrustees').val(row.nameOfTrustees);
            $('#FormerManagersTrustees').val(row.formerManagersTrustees);
            $('#DateOfRenewalOfRegistration').val(row.dateOfRenewalOfRegistration);
            $('#DateOfCommencement').val(row.dateOfCommencement);
            $('#InitialFloatation').val(row.initialFloatation);
            $('#InitialSubscription').val(row.initialSubscription);
            $('#CoyRegisteredBy').val(row.coyRegisteredBy);
            $('#TrusteesAddress').val(row.trusteesAddress);
            $('#InvestmentObjective').val(row.investmentObjective);
            //$('#MgtType').val(row.mgtType);
            $('#Webbsite').val(row.webbsite);
            $('#ddlacctTStand').val(row.accountStand).trigger('change');
            $('#ddlCompanyType').val(row.companyType).trigger('change');
            $('#ddlCompanyClass').val(row.coyClass).trigger('change');
           // $('#AccountStand').val(row.accountStand);
            $('#ddlmgtType').val(row.managementType).trigger('change');
            $('#EoyprofitAndLossGl').val(row.eoyprofitAndLossGl);       
            $('#NameOfRegistrar').val(row.nameOfRegistrar);
            $('#AddNewCompany').modal('show');
            $('#btnCompanyUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            $('#btnCompanyUpdate').show();
            $('#btnCompany').hide();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);

        debugger
        var json_data = {};
      
       
            swal({
                title: "Are you sure?",
                text: "Company will be deleted!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    

                    debugger
                    var regs = {
                        Id: row.id,
                        CoyId: row.coyId,
                        CoyName: row.coyName,
                        Address: row.address,
                        Telephone: row.telephone,
                        Fax: row.fax,
                        Email: row.email,
                        DateOfIncorporation: row.dateOfIncorporation,
                        Manager: row.manager,
                        NatureOfBusiness: row.natureOfBusiness,
                        NameOfScheme: row.nameOfScheme,
                        FunctionsRegistered: row.functionsRegistered,
                        AuthorisedShareCapital: row.authorisedShareCapital,
                        NameOfRegistrar: row.nameOfRegistrar,
                        NameOfTrustees: row.nameOfTrustees,
                        FormerManagersTrustees: row.formerManagersTrustees,
                        DateOfRenewalOfRegistration: row.dateOfRenewalOfRegistration,
                        DateOfCommencement: row.dateOfCommencement,
                        InitialFloatation: row.initialFloatation,
                        InitialSubscription: row.initialSubscription,
                        CoyRegisteredBy: row.coyRegisteredBy,
                        TrusteesAddress: row.trusteesAddress,
                        InvestmentObjective: row.investmentObjective,
                        MgtType: row.mgtType,
                        Webbsite: row.webbsite,
                        CoyClass: row.coyClass,
                        CompanyType: row.companyType,
                        AccountStand: row.accountStand,
                        ManagementType: row.managementType,
                        EoyprofitAndLossGl: row.eoyprofitAndLossGl,
                        NameOfRegistrar: row.nameOfRegistrar,
                        Deleted: true,
                    }

                    $.ajax({
                        url: url_path + '/Companysetup/RemoveCompany',
                        type: 'POST',
                        data: regs,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Delete company', text: 'Company deleted successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                               // $('#AddNewCompany').modal('hide')
                                $('#companyTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path + '/Companysetup/listcompany' });

                                $("#btnCompany").removeAttr("disabled");

                            }
                            else {
                                swal({ title: 'Delete company', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnCompany").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Delete Company', text: 'deleting company encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnCompany").removeAttr("disabled");
                        }
                    });
                }
            });
    

    }

};
$(document).ready(function ($) {
 $(".modal").perfectScrollbar(); 

    $('#btnCompanyUpdate').on('click', function () {
        $('#btnCompanyUpdate').hide();
        $('#btnCompany').show();
        debugger
        updateCompany();
    });

});
function updateCompany() {
    debugger
    var json_data = {};
    json_data.Id=$('#Id').val();
    json_data.CoyId = $('#CoyId').val();
    json_data.CoyName = $('#CoyName').val();
    json_data.CoyCode = $('#CoyCode').val();
    json_data.Address = $('#Address').val();
    json_data.Telephone=$('#Telephone').val();
    json_data.Fax=$('#Fax').val();
    json_data.Email=$('#Email').val();
    json_data.DateOfIncorporation=$('#DateOfIncorporation').val();
    json_data.Manager=$('#Manager').val();
    json_data.NatureOfBusiness=$('#NatureOfBusiness').val();
    json_data.NameOfScheme=$('#NameOfScheme').val();
    json_data.FunctionsRegistered=$('#FunctionsRegistered').val();
    json_data.AuthorisedShareCapital=$('#AuthorisedShareCapital').val();
    json_data.NameOfRegistrar=$('#NameOfRegistrar').val();
    json_data.NameOfTrustees=$('#NameOfTrustees').val();
    json_data.FormerManagersTrustees=$('#FormerManagersTrustees').val();
    json_data.DateOfRenewalOfRegistration=$('#DateOfRenewalOfRegistration').val();
    json_data.DateOfCommencement=$('#DateOfCommencement').val();
    json_data.InitialFloatation=$('#InitialFloatation').val();
    json_data.InitialSubscription=$('#InitialSubscription').val();
    json_data.CoyRegisteredBy=$('#CoyRegisteredBy').val();
    json_data.TrusteesAddress=$('#TrusteesAddress').val();
    json_data.InvestmentObjective=$('#InvestmentObjective').val();
    json_data.MgtType=$('#MgtType').val();
    json_data.Webbsite=$('#Webbsite').val();
    json_data.CoyClass = $('#ddlCompanyClass').val();
    json_data.CompanyType = $('#ddlCompanyType').val();
    json_data.AccountStand = $('#ddlacctTStand').val();
    json_data.ManagementType = $('#ddlmgtType').val();
    json_data.EoyprofitAndLossGl=$('#EoyprofitAndLossGl').val();
    json_data.NameOfRegistrar=$('#NameOfRegistrar').val();
  

    $("input[type=submit]").attr("disabled", "disabled");

    $('#frmcompany').validate({

	rules: {
		Telephone: {
			digits: true,
			maxlength: 50
			}
		},


        messages: {
  		Telephone: {
                        required: "Mobile phone number is required",
                        digits: "Phone number(s) can only contain digits",
                        maxlength: jQuery.validator.format("Mobile phone number cannot exceed {0} characters")
                    },

            coyID: { required: "Company ID is required" },
            coyName: { required: "Company Name is required" },
            Address: { required: "Address is required" },           
            Email: { required: "Email is required" },
			ignore: false,
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Company will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnCompanyUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: url_path + '/Companysetup/UpdateCompany',
                        type: 'POST',
                        data: json_data,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Company', text: 'Company updated successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewCompany').modal('hide')
                                
			$('#companyTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path + '/Companysetup/listcompany' });

                                $("#btnCompanyUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Company', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnCompanyUpdate").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Company', text: 'Company update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnCompanyUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Company', 'You cancelled company update.', 'error');
            $("#btnCompanyUpdate").removeAttr("disabled");
        });

}





$(document).ready(function ($) {
    $('#btnCompanyUpdate').hide();
    $('#btnCompany').on('click', function () {
        debugger
        addCompany();
    });

});
function addCompany() {
    debugger
   
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmcompany').validate({       
        
	 rules: {
            Telephones: {
                digits: true,
                maxlength: 50
            }
        },

        messages: {
            Telephones: {
                required: "Mobile phone number is required",
                digits: "Phone number(s) can only contain digits",
                maxlength: jQuery.validator.format("Mobile phone number cannot exceed {0} characters")
            }

            //coyID: { required: "Company ID is required" },
            //coyName: { required: "Company Name is required" },
            //Address: { required: "Address is required" },
            //Email: { required: "Email is required" },
       
        },     
        
        //errorPlacement: function (error, element) {
        //    $.notify({
        //        icon: "now-ui-icons travel_info",
        //        message: error.text(),
        //    }, {
        //            type: 'danger',
        //            placement: {
        //                from: 'bottom',
        //                align: 'right'
        //            }
        //        });
        //},
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Company will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnCompany").attr("disabled", "disabled");

                    debugger      
                    //var regs = {
                    //    Id: $('#Id').val(),
                    //    CoyCode: $('#CoyCode').val(),
                    //    Coyname: $('#CoyName').val(),
                    //    Address: $('#Address').val(),
                    //    Telephone: $('#Telephone').val(),
                    //    Fax: $('#Fax').val(),
                    //    DateOfIncorporation: $("txtdate").val(),
                    //    Email:$('#Email').val(),
                    //    Webbsite: $('#Webbsite').val(),
                    //    InvestmentObjective: $('#InvestmentObjective').val(),
                    //    CoyClass: $('#ddlCompanyClass').val(),
                    //    CompanyType: $('#ddlCompanyType').val(),
                    //    ManagementType: $('#ddlmgtType').val(),
                    //    Manager: $('#Manager').val(),
                    //    NatureOfBusiness: $('#NatureOfBusiness').val(),
                    //    NameOfScheme: $('#NameOfScheme').val(),
                    //    FunctionsRegistered: $('#FunctionsRegistered').val(),
                    //    AuthorisedShareCapital: $('#AuthorisedShareCapital').val(),
                    //    NameOfRegistrar: $('#NameOfRegistrar').val(),
                    //    NameOfTrustees: $('#NameOfTrustees').val(),
                    //    FormerManagersTrustees: $('#FormerManagersTrustees').val(),
                    //    DateOfRenewalOfRegistration: $('#DateOfRenewalOfRegistration').val(),
                    //    DateOfCommencement: $('#DateOfCommencement').val(),
                    //    InitialFloatation: $('#InitialFloatation').val(),
                    //    InitialSubscription: $('#InitialSubscription').val(),
                    //    AccountStand: $('#ddlacctTStand').val(), 
                    //    EoyprofitAndLossGl: $('#EoyprofitAndLossGl').val()
                    //}


                    var regs = {};

                    regs.Id = $('#Id').val();
                    regs.CoyCode = $('#CoyCode').val();
                    regs.Coyname = $('#CoyName').val();
                    regs.Address = $('#Address').val();
                    regs.Telephone = $('#Telephone').val();
                    regs.Fax = $('#Fax').val();
                    regs.DateOfIncorporation = $("txtdate").val();
                    regs.Email = $('#Email').val();
                    regs.Webbsite = $('#Webbsite').val();
                    regs.InvestmentObjective = $('#InvestmentObjective').val();
                    regs.CoyClass = $('#ddlCompanyClass').val();
                    regs.CompanyType = $('#ddlCompanyType').val();
                    regs.ManagementType = $('#ddlmgtType').val();
                    regs.Manager = $('#Manager').val();
                    regs.NatureOfBusiness = $('#NatureOfBusiness').val();
                    regs.NameOfScheme = $('#NameOfScheme').val();
                    regs.FunctionsRegistered = $('#FunctionsRegistered').val(),
                    regs.AuthorisedShareCapital = $('#AuthorisedShareCapital').val();
                    regs.NameOfRegistrar = $('#NameOfRegistrar').val();
                    regs.NameOfTrustees = $('#NameOfTrustees').val();
                    regs.FormerManagersTrustees = $('#FormerManagersTrustees').val();
                    regs.DateOfRenewalOfRegistration = $('#DateOfRenewalOfRegistration').val();
                    regs.DateOfCommencement = $('#DateOfCommencement').val();
                    regs.InitialFloatation = $('#InitialFloatation').val();
                    regs.InitialSubscription = $('#InitialSubscription').val();
                    regs.AccountStand = $('#ddlacctTStand').val();
                    regs.EoyprofitAndLossGl = $('#EoyprofitAndLossGl').val();
                    


                    $.ajax({
                        url: url_path + '/Companysetup/AddCompany',
                        type: 'POST',
                        data: regs,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                           
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Add company', text: 'Company added successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewCompany').modal('hide')
                                $('#companyTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path + '/Companysetup/listcompany' });

                                $("#btnCompany").removeAttr("disabled");
                                
                            }
                            else {
                                swal({ title: 'Add company', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnCompany").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Company', text: 'Adding company encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnCompany").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Company', 'You cancelled add company.', 'error');
            $("#btnCompany").removeAttr("disabled");
        });

}


function reloadpage() {
    location.reload();
}

//function clear() {
//    $('#Id').val('');
//    $('#coyID').val('');
//    $('#coyName').val('');
//    $('#Address').val('');
//    $('#Telephone').val('');
//    $('#Fax').val('');
//    $('#Email').val('');
//    $('#DateOfIncorporation').val('');
//    $('#Manager').val('');
//    $('#NatureOfBusiness').val('');
//    $('#NameOfScheme').val('');
//    $('#FunctionsRegistered').val('');
//    $('#AuthorisedShareCapital').val('');
//    $('#NameOfRegistrar').val('');
//    $('#NameOfTrustees').val('');
//    $('#FormerManagers_Trustees').val('');
//    $('#DateOfRenewalOfRegistration').val('');
//    $('#DateOfCommencement').val('');
//    $('#InitialFloatation').val('');
//    $('#InitialSubscription').val('');
//    $('#CoyRegisteredBy').val('');
//    $('#TrusteesAddress').val('');
//    $('#InvestmentObjective').val('');
//    $('#CompanyClass').val('');
//    $('#CompanyType').val('');
//    $('#ddlacctTStand').val('');
//    $('#MgtType').val('');
//    $('#Webbsite').val('');
//    $('#ddlCompanyClass').val('');
//    $('#AccountStand').val('');
//    $('#ddlmgtType').val('');
//    $('#EOYProfitAndLossGL').val('');
  
//}

$('#companyTable').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<h8>' +
        '<div class="content"><div class="card"><div class="card-body">'+
        '<div class="row"><p style="color:red">' +
        '<div class="col-md-12 "><strong style="color:red"> Company Name:</strong> &nbsp' + row.coyName  + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Telephone:</strong>  &nbsp' + row.telephone + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Email:</strong>  &nbsp' + row.email + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Website:</strong> ' + row.webbsite + '</div>' +
        '<div class="col-sm-6"><strong style="color:red">Accounting Standard:</strong>  &nbsp' + row.accountingStandard + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Management Type:</strong>  &nbsp' + row.managementType + '</div>' +       
        '<div class="col-sm-6"><strong style="color:red"> Nature Of Business:</strong>  &nbsp' + row.natureOfBusiness + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Manager:</strong>  &nbsp' + row.manager + '</div>' +
        '<div class="col-sm-6"><strong style="color:red">  Functions Registered:</strong>  &nbsp' + row.functionsRegistered + '</div>' +
        '<div class="col-sm-12"><strong style="color:red">  Name Of Scheme :</strong>  &nbsp' + row.nameOfScheme + '</div>' +
        '<div class="col-sm-12"><strong style="color:red">Investment Objective:</strong> &nbsp' + row.investmentObjective + '</div>' + 

        '<div class="col-sm-12"><strong style="color:red"> Address:</strong>  &nbsp' + row.address + '</div>' +''       
       
       

      
        
        //'<strong> Nature Of Business:</strong>  &nbsp' + row.natureOfBusiness + ''+
       
        //'<strong> Functions Registered:</strong>  &nbsp' + row.functionsRegistered + '' +
        //'<strong> Authorised Share Capital:</strong>  &nbsp' + row.authorisedShareCapital + '' +
        //'<strong> Name Of Registrar:</strong>  &nbsp' + row.nameOfRegistrar + '' + '<p>' +

        //'<strong> Name Of Trustees:</strong>  &nbsp' + row.nameOfTrustees + '' +
        //'<strong> Former Managers Trustees:</strong>  &nbsp' + row.formerManagersTrustees + '' +
        //'<strong> Date Of Renewal Registration:</strong>  &nbsp' + row.dateOfRenewalOfRegistration + '' +
        //'<strong> Date Of Commencement:</strong>  &nbsp' + row.dateOfCommencement + '' +
        //'<strong> Initial Floatation:</strong>  &nbsp' + row.initialFloatation + '' +
        //'<strong> Initial Subscription:</strong>  &nbsp' + row.initialSubscription + '' +
        //'<strong> Accounting Standard:</strong>  &nbsp' + row.accountingStandard + '' +
        //'<strong> End of Year Profit or Loss Ledger:</strong>  &nbsp' + row.eoyprofitAndLossGl + ''
        ;

    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});

//.................Validation
//$(document).ready(function ($) {
//    $("#CoyName").mouseout(function (event) {
//        debugger
//        var coyname = $('#CoyName').val();
//        if (coyname == '') {
//            $.notify({ icon: "add_alert", message: 'Your company name is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#coyname').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//        }
//        if (/^[0-9]+$/.test($('#CoyName').val())) {
//            $.notify({ icon: "add_alert", message: 'Your company name contain numbers' }, { type: 'danger' });
//            $(this).css('border-color', 'red');
//            $('#CoyName').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#coyname').focus();
//        }
//        if (coyname.length <= 3) {
//            $.notify({ icon: "add_alert", message: 'Your company name is too short' }, { type: 'danger' });
//            $(this).css('border-color', 'red');
//            $('#CoyName').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#coyname').focus();

//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#Address").mouseout(function (event) {
//        debugger
//        var Address = $('#Address').val();
//        if (Address == '') {
//            $.notify({ icon: "add_alert", message: 'Your company address is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#Address').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Address').focus();
//        }

//        if (Address.length <= 4) {
//            $.notify({ icon: "add_alert", message: 'Your company address is too short' }, { type: 'danger' });
//            $(this).css('border-color', 'red');
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Address').focus();

//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#Telephone").mouseout(function (event) {
//        debugger
//        var Telephone = $('#Telephone').val();
//        if (Telephone == '') {
//            $.notify({ icon: "add_alert", message: 'Your phone number is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#Telephone').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Telephone').focus();
//        }

//        if (Telephone.length != 11) {
//            $.notify({ icon: "add_alert", message: 'Your phone number is not valid' }, { type: 'danger' });
//            $(this).css('border-color', 'red');
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Telephone').focus();

//        }

//    });
////});
//$(document).ready(function ($) {
//    $("#Fax").mouseout(function (event) {
//        debugger
//        var Fax = $('#Fax').val();
//        if (Fax == '') {
//            $.notify({ icon: "add_alert", message: 'Your fax number is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#Fax').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Fax').focus();
//        }

//        if (Fax.length != 11) {
//            $.notify({ icon: "add_alert", message: 'Your fax number is not valid' }, { type: 'danger' });
//            $(this).css('border-color', 'red');
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Fax').focus();

//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#Email").mouseout(function (event) {
//        debugger
//        var emails = $('#Email').val();
//        if (emails == '') {
//            $.notify({ icon: "add_alert", message: 'Your  email is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#Email').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Email').focus();
//        }
//        if (/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($('#Email').val())) {
//            //$.notify({ icon: "add_alert", message: 'Your email is valid' });
//            $(this).css('border-color', 'green');
//            $('#Email').focus();
//            return false;
//        } else {
//            $.notify({ icon: "add_alert", message: 'Your email is not valid' }, { type: 'danger' });
//            $(this).css('border-color', 'red');
//            $('#Email').focus();
//        }


//    });
//});
//$(document).ready(function ($) {
//    $("#DateOfIncorporation").mouseout(function (event) {
//        debugger
//        var DateOfIncorporation = $('#DateOfIncorporation').val();
//        if (DateOfIncorporation == '') {
//            $.notify({ icon: "add_alert", message: 'Your date of incorporation is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#DateOfIncorporation').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#DateOfIncorporation').focus();
//        }


//    });
//});
//$(document).ready(function ($) {
//    $("#Webbsite").mouseout(function (event) {
//        debugger
//        var Webbsite = $('#Webbsite').val();
//        if (Webbsite == '') {
//            $.notify({ icon: "add_alert", message: 'Your website is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#Webbsite').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Webbsite').focus();
//        }


//    });
//});
//$(document).ready(function ($) {
//    $("#InvestmentObjective").mouseout(function (event) {
//        debugger
//        var InvestmentObjective = $('#InvestmentObjective').val();
//        if (InvestmentObjective == '') {
//            $.notify({ icon: "add_alert", message: 'Your investment objective is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#InvestmentObjective').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#InvestmentObjective').focus();
//        }


//    });
//});
//$(document).ready(function ($) {
//    $("#ddlCompanyClass").mouseout(function (event) {
//        debugger
//        var ddlCompanyClass = $('#ddlCompanyClass').val();
//        if (ddlCompanyClass == '') {
//            $.notify({ icon: "add_alert", message: 'Your company class is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#ddlCompanyClass').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#ddlCompanyClass').focus();
//        }


//    });
//});
//$(document).ready(function ($) {
//    $("#ddlCompanyType").mouseout(function (event) {
//        debugger
//        var ddlCompanyType = $('#ddlCompanyType').val();
//        if (ddlCompanyType == '') {
//            $.notify({ icon: "add_alert", message: 'Your company type is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#ddlCompanyType').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#ddlCompanyType').focus();
//        }


//    });
//});
//$(document).ready(function ($) {
//    $("#ddlmgtType").mouseout(function (event) {
//        debugger
//        var ddlmgtType = $('#ddlmgtType').val();
//        if (ddlmgtType == '') {
//            $.notify({ icon: "add_alert", message: 'Your management type is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#ddlmgtType').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#ddlmgtType').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#Manager").mouseout(function (event) {
//        debugger
//        var Manager = $('#Manager').val();
//        if (Manager == '') {
//            $.notify({ icon: "add_alert", message: 'Your manager is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#Manager').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#Manager').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#NatureOfBusiness").mouseout(function (event) {
//        debugger
//        var NatureOfBusiness = $('#NatureOfBusiness').val();
//        if (NatureOfBusiness == '') {
//            $.notify({ icon: "add_alert", message: 'Your nature of business is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#NatureOfBusiness').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#NatureOfBusiness').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#NameOfScheme").mouseout(function (event) {
//        debugger
//        var NameOfScheme = $('#NameOfScheme').val();
//        if (NameOfScheme == '') {
//            $.notify({ icon: "add_alert", message: 'Your nature of scheme is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#NameOfScheme').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#NameOfScheme').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#FunctionsRegistered").mouseout(function (event) {
//        debugger
//        var FunctionsRegistered = $('#FunctionsRegistered').val();
//        if (FunctionsRegistered == '') {
//            $.notify({ icon: "add_alert", message: 'Your registered function is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#FunctionsRegistered').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#FunctionsRegistered').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#AuthorisedShareCapital").mouseout(function (event) {
//        debugger
//        var AuthorisedShareCapital = $('#AuthorisedShareCapital').val();
//        if (AuthorisedShareCapital == '') {
//            $.notify({ icon: "add_alert", message: 'Your authorised share capital is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#AuthorisedShareCapital').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#AuthorisedShareCapital').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#NameOfRegistrar").mouseout(function (event) {
//        debugger
//        var NameOfRegistrar = $('#NameOfRegistrar').val();
//        if (NameOfRegistrar == '') {
//            $.notify({ icon: "add_alert", message: 'Your name of registrar is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#NameOfRegistrar').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#NameOfRegistrar').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#NameOfTrustees").mouseout(function (event) {
//        debugger
//        var NameOfTrustees = $('#NameOfTrustees').val();
//        if (NameOfTrustees == '') {
//            $.notify({ icon: "add_alert", message: 'Your name of trustee is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#NameOfTrustees').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#NameOfTrustees').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#FormerManagersTrustees").mouseout(function (event) {
//        debugger
//        var FormerManagersTrustees = $('#FormerManagersTrustees').val();
//        if (FormerManagersTrustees == '') {
//            $.notify({ icon: "add_alert", message: 'Your former manager trustee is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#FormerManagersTrustees').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#FormerManagersTrustees').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#DateOfRenewalOfRegistration").mouseout(function (event) {
//        debugger
//        var DateOfRenewalOfRegistration = $('#DateOfRenewalOfRegistration').val();
//        if (DateOfRenewalOfRegistration == '') {
//            $.notify({ icon: "add_alert", message: 'Your date of renewal of registration is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#DateOfRenewalOfRegistration').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#DateOfRenewalOfRegistration').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#DateOfCommencement").mouseout(function (event) {
//        debugger
//        var DateOfCommencement = $('#DateOfCommencement').val();
//        if (DateOfCommencement == '') {
//            $.notify({ icon: "add_alert", message: 'Your date of commencement is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#DateOfCommencement').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#DateOfCommencement').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#InitialFloatation").mouseout(function (event) {
//        debugger
//        var InitialFloatation = $('#InitialFloatation').val();
//        if (InitialFloatation == '') {
//            $.notify({ icon: "add_alert", message: 'Your initial floatation is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#InitialFloatation').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#InitialFloatation').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#InitialSubscription").mouseout(function (event) {
//        debugger
//        var InitialSubscription = $('#InitialSubscription').val();
//        if (InitialSubscription == '') {
//            $.notify({ icon: "add_alert", message: 'Your initial subscription is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#InitialSubscription').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#InitialSubscription').focus();
//        }

//    });
//});
//$(document).ready(function ($) {
//    $("#ddlacctTStand").mouseout(function (event) {
//        debugger
//        var ddlacctTStand = $('#ddlacctTStand').val();
//        if (ddlacctTStand == '') {
//            $.notify({ icon: "add_alert", message: 'Your account standard is empty' }, { type: 'danger', timer: 1000 });
//            $(this).css('border-color', 'red');
//            $('#ddlacctTStand').focus();
//            return false;
//        } else {
//            $(this).css('border-color', 'green');
//            $('#ddlacctTStand').focus();
//        }

//    });
//});


$(document).ready(function ($) {
    debugger
    $('#DateOfIncorporation')
        .datetimepicker({
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#eventForm').formValidation('revalidateField', 'date');
        });

   
});
$(document).ready(function ($) {
    debugger

    $('#CoyCode').mouseleave(function (event) {
        $.ajax({
            url: url_path + '/Companysetup/CompanyCode',
            data: { CoyCode: $('#CoyCode').val()}
        }).then(function (result) {
            debugger
            if (result != "" && result != null) {
                $.notify({ icon: "add_alert", message: result.toString() }, { type: 'danger', timer: 1000 });
            $(this).css('border-color', 'red');
            $('#CoyCode').focus();
            return false;

            }
        })
    });
});

$(document).ready(function ($) {
    debugger
    $('#DateOfRenewalOfRegistration')
        .datetimepicker({
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#eventForm').formValidation('revalidateField', 'date');
        });
});

$(document).ready(function ($) {
    debugger
    $('#DateOfCommencement')
        .datetimepicker({
        })
        .on('changeDate', function (e) {
            // Revalidate the date field
            $('#eventForm').formValidation('revalidateField', 'date');
        });
});




$(document).ready(function ($) {
    debugger
    $("input[required]").parent("label").addClass("required");

});


    //-Email Validation
    var telReg = /^[+]* [(]{0,1}[0 - 9]{1,4}[)]{0,1}[-\s\./0-9]*$/g;
    function validateEmail(tel) {
        return telReg.test(tel);      
    }

