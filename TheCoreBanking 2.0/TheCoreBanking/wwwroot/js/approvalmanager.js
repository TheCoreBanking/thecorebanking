﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}

function reloadpage() {
    location.reload();
}




$(document).ready(function ($) {

    $("#btncloseother").on('click', function () {
        clearOtherContent();
    });

    $("#btncreateotheeop").on('click', function () {
        clearOtherContent();
    });
});







function clearOtherContent() {

    $("#operations").val('').trigger("change");

    $("#designation").val('').trigger("change");

    $("#authority").val('').trigger("change");

    $("#culLimit").val('');

    $("#minamount").val('');

    $("#maxamount").val('');

    $("#applevel").val('');
}






$("#btnclearother").on("click", function () {
    clearO();
});




function clearO() {
    $("#operations").val('').trigger("change");

    $("#designation").val('').trigger("change");

    $("#authority").val('').trigger("change");

    $("#culLimit").val('');

    $("#minamount").val('');

    $("#maxamount").val('');

    $("#applevel").val('');
}




$(document).ready(function () {
    debugger
    $("#operations").select2({
        theme: "bootstrap4",
        placeholder: "Loading..."

    });
    $.ajax({
        url: "../ApprovingAuthority/LoadOperate",
    }).then(function (response) {
        debugger

        console.log(response);
        $("#operations").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation",
            width: '100%',
            data: response,
            dropdownParent: $("#btnCreateother.modal"),
        });
    });
})




$("#operations").on('select2:select', function (e) {
    debugger
    var OPId = e.params.data.id;
    $('#applevel').html('<option></option>')

    $.ajax({
        url: "../ApprovingAuthority/AppLevel",
        data: { id: OPId },

    }).then(function (response) {
        debugger
        console.log(response);
        $("#applevel").select2({
            theme: "bootstrap4",
            placeholder: "Select Approving level",
            width: '100%',
            data: response,
            dropdownParent: $("#btnCreateother"),
        });
    });

    console.log(data);
});






$(document).ready(function () {
    debugger
    $("#designation").select2({
        theme: "bootstrap4",
        placeholder: "Loading..."
    });
    $.ajax({
        url: "../ApprovingAuthority/GetDesignation",
    }).then(function (response) {
        debugger
        console.log(response);
        $("#designation").select2({
            theme: "bootstrap4",
            placeholder: "Select Designation",
            width: '100%',
            data: response,
            dropdownParent: $("#btnCreateother.modal"),
        });
    });
})




$("#designation").on("select2:select", function (e) {
    debugger
    var deptId = e.params.data.id;
    $("#authority").html('<option></option>')

    $.ajax({
        url: "../ApprovingAuthority/LoadStaff",
        data: { id: deptId },

    }).then(function (response) {
        debugger
        console.log(response);
        $("#authority").select2({
            theme: "bootstrap4",
            placeholder: "Select Staff",
            width: "100%",
            data: response,
            dropdownParent: $("#btnCreateother"),
        });
    });
});





$(document).ready(function () {
    $("#btnothers").click(function (e) {
        debugger
        e.preventDefault();
        var ope = {};

        var data = $("#authority").select2("data")

        ope.StaffName = data[0].text;
        ope.text = data[0].text;
        ope.id = data[0].id;
        ope.OperationId = $("#operations").val();
        ope.MaxAmt = $("#maxamount").val();
        ope.MinAmt = $("#minamount").val();
        ope.Designation = $("#designation").val();
        ope.ApprovingLevel = $("#applevel").val();
        ope.CummulativeAmt = $("#culLimit").val();

        $.ajax({
            url: "../ApprovingAuthority/AddOpeOthers",
            method: "POST",
            //contentType: 'application/json;charset = utf-8',
            dataType: "json",
            data: ope,
            success: function (response) {
                if (response == true) {
                    swal({ title: 'Operation Officer', text: 'Your Operation Approval has been Created Successfully!', type: 'success' }).then(function () { clear(); });

                    $("#Other-Table")
                        .bootstrapTable("refresh", {
                            url: "../ApprovingAuthority/LoadOppApproval"
                        });
                    $("#btnothers").removeAttr("disabled");
                }
                else if (response == false) {
                    swal({ title: 'Operation Officer', text: 'Approving Officer Has Already been profiled!', type: 'success' }).then(function () { clear(); });
                }
                else {
                    swal({ title: 'Operation Officer', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                    $("#btnothers").removeAttr("disabled");
                }
                $("#btnCreateother").modal("hide");
            },
            error: function (err) {
                alert(err);
            }
        })

        $("#btnCreateother").modal("hide");

    });
});



$("#Other-Table").on("expand-row.bs.table", function (e, index, row, $detail) {
    $detail.html("Loading request...");

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<form id="form">' +
        '<div class="card"><div class="card-header">' +
        '<div class="content"><div class="card">' +
        '<div class="row"><div class="col-md-6">' +
        //'<strong style="color:#007E33"> Branch Code:</strong>  &nbsp' + row.BranchCode + '' +
        '<strong style="color:#007E33"> Branch Code:</strong>  &nbsp' + row.branchCode + '</div>' +
        '<strong style="color:#007E33"> Company Code:</strong> &nbsp' + row.companyCode + '</div></div>' +
        '<div class="row"><div class="col-md-6">' +
        '<strong style="color:#007E33">  Created By:</strong> &nbsp' + row.createdBy + '</div></div>' +
        //'<strong style="color:#007E33">  Details:</strong>  &nbsp' + row.Details + '</div></div>' +
        '<br/>' + '</form>'


    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});


