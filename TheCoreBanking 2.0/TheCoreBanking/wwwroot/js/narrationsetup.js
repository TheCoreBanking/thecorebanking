﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}

var groupId;

$(document).ready(function () {

    initDataTable();

    $(".modal").perfectScrollbar();
});



$(document).ready(function () {
    $('#txtsubmit').click(function () {
        debugger
        var ope = {};
        ope.Comment = $("#comment").val();

        $.ajax({
            url: "/NarrationSetup/Create",
            method: 'POST',
            dataType: "json",
            data: ope,
            success: function (response) {
                $("#data-table")
                    .bootstrapTable("refresh", {
                        url: "/NarrationSetup/ListNarration"
                    });

                swal({ title: 'Narration Comment', text: 'Your Comment has been Created Successfully!', type: 'success' });

            },
            error: function (err) {
                alert(err);
            }
        })

        initDataTable();
        $("#btnNarrationModal").modal("hide");

    });
});



function initDataTable() {
    debugger
    $("#data-table").bootstrapTable('showLoading');


    $("#data-table").bootstrapTable({
        search: true,
        searchAlign: "right",
        url: "/NarrationSetup/ListNarration",
        showPaginationSwitch: true,
        pagination: true,
        mobileResponsive: true,
        checkOnInit: true,
        minWidth: 626,
        toolbar: "#custom-toolbar",
        showRefresh: true,
        showToggle: true,
        buttonsClass: "danger",
        columns: [
            {
                field: "comment",
                title: "Comment",
                sortable: true
            },
            {

                formatter: utilities.editOpCommentFormatter,

            },
            {

                formatter: utilities.deleteOpCommentFormatter,

            },


        ],
    });



};


var utilities = {

    editOpCommentFormatter: function (val, row, index) {

        debugger

        return [
            "<button type='button' class='btn btn-warning btn-icon' title='Edit' ",
            "onclick='utilities.editOpComFrm(" + row.id + ")'>",
            "<i class='fas fa-edit'>",
            "</i></button>"
        ].join("");

    },

    deleteOpCommentFormatter: function (val, row, index) {
        return [
            "<button type='button' class='btn btn-danger btn-icon' title='Delete'",
            "onclick='utilities.deleteOpComFrm(" + row.id + ")'>",
            "<i class='fas fa-times'>",
            "</i></button>"
        ].join("");
    },
    editOpComFrm: function (self) {

        debugger

        var groupData = $("#data-table")
            .bootstrapTable('getRowByUniqueId', self);

        console.log(groupData);

        var groupId = groupData.id;

        $("#commentedit").val(groupData.comment);

        $("#callId").val(groupId);

        $("#editOpCom").modal("show");

    },
    deleteOpComFrm: function (self) {

        debugger

        var groupData = $("#data-table")
            .bootstrapTable('getRowByUniqueId', self);

        console.log(groupData);

        var groupId = groupData.id;

        $("#commentdelete").val(groupData.comment);

        $("#deleteid").val(groupId);

        $("#deleteOpCom").modal("show");
    },
}






$(document).ready(function () {
    $('#btnedit').click(function () {
        debugger
        var opec = {};
        opec.Comment = $("#commentedit").val();
        opec.Id = $("#callId").val();

        $.ajax({
            url: "/NarrationSetup/Edit",
            data: opec,
            type: "POST",
            dataType: "json",
            success: function (response) {
                $("#data-table")
                    .bootstrapTable("refresh", {
                        url: "/NarrationSetup/ListNarration"
                    });
                swal({ title: 'Narration Comment', text: 'Your Comment has been Edited Successfully!', type: 'success' });
            },
            error: function (err) {
                alert(err);
            }

        })

        initDataTable();
        $("#editOpCom").modal("hide");

    });
});




$(document).ready(function () {
    $('#btndelete').click(function () {
        debugger
        var opeco = {}
        opeco.Id = $("#deleteid").val();
        opeco.Comment = $("#commentdelete").val();

        $.ajax({
            url: "/NarrationSetup/Delete",
            data: opeco,
            type: "POST",
            dataType: "json",
            success: function (response) {
                $("#data-table")
                    .bootstrapTable("refresh", {
                        url: "/NarrationSetup/ListNarration"
                    });

                swal({ title: 'Narration Comment', text: 'Your Comment has been Deleted Successfully!', type: 'success' });

            },
            error: function (err) {
                alert(err);
            }

        })

        initDataTable();
        $("#deleteOpCom").modal("hide");
    });
});
