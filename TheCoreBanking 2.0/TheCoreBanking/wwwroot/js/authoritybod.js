﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}

function reloadpage() {
    location.reload();
}




$(document).ready(function ($) {


    $("#closeboard").on('click', function () {
        clearBoardContent();
    });

    $("#btnaddbod").on('click', function () {
        clearBoardContent();
    });

});








function clearBoardContent() {

    $("#bodOperation").val('').trigger("change");

    $("#boddepartment").val('').trigger("change");

    $("#bodauthority").val('').trigger("change");

    $("#bodculLimit").val('');

    $("#bodminamount").val('');

    $("#bodmaxamount").val('');

    $("#bodapplevel").val('');
}



$("#btnclearboard").on("click", function () {
    clearB();
});



function clearB() {

    $("#bodOperation").val('').trigger("change");

    $("#boddepartment").val('').trigger("change");

    $("#bodauthority").val('').trigger("change");

    $("#bodculLimit").val('');

    $("#bodminamount").val('');

    $("#bodmaxamount").val('');

    $("#bodapplevel").val('');
}





//BOARD COMMITTEE

$(document).ready(function () {
    debugger
    $("#bodOperation").select2({
        theme: "bootstrap4",
        placeholder: "Loading..."

    });
    $.ajax({
        url: "../BoardAuthority/GetOperation",
    }).then(function (response) {
        debugger
        console.log(response);
        $("#bodOperation").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation",
            width: '100%',
            data: response,
            dropdownParent: $("#btnCreateboard.modal"),
        });
    });
})



$("#bodOperation").on('select2:select', function (e) {
    debugger
    var OPId = e.params.data.id;
    $("#bodapplevel").html('<option></option>')

    $.ajax({
        url: "../BoardAuthority/AppLevel",
        data: { id: OPId },

    }).then(function (response) {
        debugger
        console.log(response);
        $("#bodapplevel").select2({
            theme: "bootstrap4",
            placeholder: "Select Approving level",
            width: '100%',
            data: response,
            dropdownParent: $("#btnCreateboard"),
        });
    });

    console.log(data);
});




$(document).ready(function () {
    debugger
    $("#boddepartment").select2({
        theme: "bootstrap4",
        placeholder: "Loading..."

    });
    $.ajax({
        url: "../BoardAuthority/GetDepartment",
    }).then(function (response) {
        debugger
        console.log(response);
        $("#boddepartment").select2({
            theme: "bootstrap4",
            placeholder: "Select Department",
            width: '100%',
            data: response,
            dropdownParent: $("#btnCreateboard.modal"),
        });
    });
})



$("#boddepartment").on("select2:select", function (e) {
    debugger
    var deptId = e.params.data.id;
    $('#bodauthority').html('<option></option>')

    $.ajax({
        url: "../BoardAuthority/LoadDeptStaff",
        data: { id: deptId },

    }).then(function (response) {
        debugger
        console.log(response);
        $("#bodauthority").select2({
            theme: "bootstrap4",
            placeholder: "Select Staff",
            width: '100%',
            data: response,
            dropdownParent: $("#btnCreateboard"),
        });
    });

});




$(document).ready(function () {
    $("#btnbrdapp").click(function (e) {
        debugger
        e.preventDefault();
        var brd = {};

        var data = $("#bodauthority").select2("data")

        brd.StaffName = data[0].text;
        brd.text = data[0].text;
        brd.id = data[0].id;


        brd.OperationId = $("#bodOperation").val();
        brd.MaxAmt = $("#bodmaxamount").val();
        brd.MinAmt = $("#bodminamount").val();
        brd.Department = $("#boddepartment").val();
        brd.ApprovingLevel = $("#bodapplevel").val();
        brd.CummulativeAmt = $("#bodculLimit").val();

        $.ajax({
            url: "../BoardAuthority/AddComBoard",
            method: "POST",

            dataType: "json",
            data: brd,
            success: function (response) {
                if (response == true) {
                    swal({ title: 'Board Officer', text: 'Your Board Officer has been Created Successfully!', type: 'success' }).then(function () { clear(); });

                    $("#data-table")
                        .bootstrapTable("refresh", {
                            url: "../BoardAuthority/listCommitBoard"
                        });
                    $("#btnbrdapp").removeAttr("disabled");
                }
                else if (response == false) {
                    swal({ title: 'Board Officer', text: 'Your Board Officer Has Already been profiled! or Board Committee does not exist!', type: 'success' }).then(function () { clear(); });
                }
                else {
                    swal({ title: 'Board Officer', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                    $("#btnbrdapp").removeAttr("disabled");
                }
                $("#btnCreateboard").modal("hide");
            },
            error: function (err) {
                alert(err);
            }
        })

        $("#btnCreateboard").modal("hide");

    });
});



$('#data-table').on("expand-row.bs.table", function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<form id="form">' +
        '<div class="card"><div class="card-header">' +
        '<div class="content"><div class="card">' +
        '<div class="row"><div class="col-md-6">' +

        '<strong style="color:#007E33"> Branch Code:</strong> &nbsp' + row.branchCode + '</div>' +
        '<strong style="color:#007E33"> Company Code:</strong> &nbsp' + row.companyCode + '</div></div>' +
        '<div class="row"><div class="col-md-6">' +
        '<strong style="color:#007E33"> Created By:</strong> &nbsp' + row.createdBy + '</div></div>' +
        '<strong style="color:#007E33"> Board No:</strong> &nbsp' + row.boardNo + '</div></div>' +
        '<br/>' + '</form>'


    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});


