﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}


$(document).ready(function ($) {

    $("#closecommittee").on('click', function () {
        clearModalContent();
    });

    $("#addclearcommittee").on('click', function () {
        clearModalContent();
    });

});


function reloadpage() {
    location.reload();
}


function clearModalContent() {

    $("#MgtNo").val('');

    $("#MgtNoApproval").val('');

    $("#CreditMinMgt").val('');

    $("#CreditMaxComteeAmt").val('');

    $("#OperationM").val('').trigger("change");


    $("#BoardNo").val('');

    $("#BoardNoApproval").val('');

    $("#CreditMinBoard").val('');

    $("#CreditMaxBoardAmt").val('');

}



$("#btnclear").on("click", function () {
    clear();
});





function clear() {
    $("#MgtNo").val('');

    $("#MgtNoApproval").val('');

    $("#CreditMinMgt").val('');

    $("#CreditMaxComteeAmt").val('');

    $("#OperationM").val('').trigger("change");


    $("#BoardNo").val('');

    $("#BoardNoApproval").val('');

    $("#CreditMinBoard").val('');

    $("#CreditMaxBoardAmt").val('');

}






$('#data-table').on("expand-row.bs.table", function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<form id="form">' +
        '<div class="card"><div class="card-header">' +
        '<div class="content"><div class="card">' +
        '<div class="row"><div class="col-md-6">' +

        '<strong style="color:#007E33"> Committee:</strong> &nbsp' + row.committee + '</div>' +
        '<strong style="color:#007E33"> Createdby:</strong> &nbsp' + row.createdby + '</div></div>' +
        '<div class="row"><div class="col-md-6">' +
        '<strong style="color:#007E33"> Credit Min.Management:</strong> &nbsp' + row.creditMinMgt + '</div>' +
        '<strong style="color:#007E33"> Credit Min.Board:</strong> &nbsp' + row.creditMinBoard + '</div></div>' +
        '<div class="row"><div class="col-md-6">' +
        '<strong style="color:#007E33"> Credit Max Management:</strong> &nbsp' + row.creditMaxComteeAmt + '</div></div>' +
        '<strong style="color:#007E33"> Credit Max Board:</strong> &nbsp' + row.creditMaxBoardAmt + '</div></div>' +
        '<br/>' + '</form>'


    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});







//ADD NEW COMMITTEE


//function showModal(self) {
//    debugger   
//    $("#openManModal()").modal('show');

//}


$(document).ready(function () {
    $("#clicked").click(function () {
        $('#bcc').show();
    });
});


$(document).ready(function () {
    debugger
    $("#OperationM").select2({
        theme: "bootstrap4",
        placeholder: "Loading..."

    });
    $.ajax({
        url: "../ApprovalCommittee/GetOperation",
    }).then(function (response) {
        debugger
        console.log(response);
        $("#OperationM").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation",
            width: '100%',
            data: response,
            dropdownParent: $("#AddNewCommittee.modal"),
        });
    });


})




//function reloadpage() {
//    location.reload();
//}



$(document).ready(function () {
    $('#btnCommit').click(function (e) {
        e.preventDefault();
        debugger
        var group = {};
        group.MgtNo = $('#MgtNo').val();
        group.MgtNoApproval = $('#MgtNoApproval').val();
        group.CreditMinMgt = $('#CreditMinMgt').val();
        group.CreditMaxComteeAmt = $('#CreditMaxComteeAmt').val();
        group.OperationId = $('#OperationM').val();

        group.BoardNo = $('#BoardNo').val();
        group.BoardNoApproval = $('#BoardNoApproval').val();
        group.CreditMinBoard = $('#CreditMinBoard').val();
        group.CreditMaxBoardAmt = $('#CreditMaxBoardAmt').val();


        $.ajax({
            url: '../ApprovalCommittee/PostCommittee',
            method: 'POST',
            //contentType: 'application/json;charset = utf-8',
            dataType: "json",
            data: group,
            success: function (response) {
                if (response.toString != '' && response != null) {
                    swal({ title: 'Add Credit Commitee', text: 'Credit Commitee added completed successfully!', type: 'success' }).then(function () { clear(); });

                    $('#data-table').
                        bootstrapTable(
                            'refresh', { url: '../ApprovalCommittee/listcommittee' });

                    $("#btnCommit").removeAttr("disabled");
                }
                else {
                    swal({ title: 'Add Credit Commitee', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                    $("#btnCommit").removeAttr("disabled");
                }
                $('#AddNewCommittee').modal('hide');

            },
            error: function (err) {
                alert(err);
            }
        })



    });
});





