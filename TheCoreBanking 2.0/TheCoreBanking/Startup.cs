﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TheCoreBanking.Data;
using TheCoreBanking.Models;
using TheCoreBanking.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;
using IdentityServer4;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Repository;
using TheCoreBanking.Data.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using System.Security.Claims;
using Newtonsoft.Json.Linq;
using Microsoft.IdentityModel.Tokens;
using IdentityModel;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace TheCoreBanking
{
    public class Startup
    {
        private IConfigurationRoot _configurationRoot;
        public Startup(IHostingEnvironment hostingEnvironment)
        {
            _configurationRoot = new ConfigurationBuilder().SetBasePath(hostingEnvironment.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .Build();
        }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
     
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = @"Server=DLM-FINTRAK\fintraksql;Database=TheCoreBanking;User Id=sa;Password=sqluser10$;";
            //var connectionString = Configuration.GetConnectionString("TheCoreBanking");
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            //services.AddDbContext<ApplicationDbContext>(builder =>
            //builder.UseSqlServer(connectionString, sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly)));

            services
             .AddIdentity<ApplicationUser, IdentityRole>()
             //.AddEntityFrameworkStores<ApplicationDbContext>()
             .AddDefaultTokenProviders();

            services.AddMvc(options => options.EnableEndpointRouting = false);
            services.AddAuthorization(options =>
            {
                // inline policies
                options.AddPolicy("UserRegistration", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    //policy.RequireClaim("subscriptionlevel", "PayingUser");
                    //policy.RequireClaim("country", "nl");
                    policy.RequireRole("User_Register");
                });
                options.AddPolicy("Staff", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireRole("Staff");
                });
            });
                JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Main.Cookies";
                options.DefaultChallengeScheme = "oidc";
                options.DefaultAuthenticateScheme = "oidc";
            })
               .AddCookie("Main.Cookies", options => { options.AccessDeniedPath = "/account/accessdenied"; })
               .AddOpenIdConnect("oidc", options =>
               {
                   options.Authority = "https://fintrakbankingmmbs-company.azurewebsites.net";
                   options.RequireHttpsMetadata = true;
                   options.ClientId = "TheCoreBanking.Main";
                   options.SignInScheme = "Main.Cookies";
                   options.ResponseType = "code id_token";
                   options.Scope.Clear();
                   options.Scope.Add("openid");
                   options.Scope.Add("profile");
                   options.Scope.Add("email");
                   options.Scope.Add("roles");
                   options.GetClaimsFromUserInfoEndpoint = true;
                   options.SaveTokens = true;
                   options.ClientSecret = "secret";
                   options.Events = new OpenIdConnectEvents()
                   {
                       OnTokenValidated = tokenValidatedContext =>
                       {
                           return Task.FromResult(0);
                       },
                       OnUserInformationReceived = (context) =>
                       {
                           ClaimsIdentity claimsId = context.Principal.Identity as ClaimsIdentity;
                           try
                           {

                               dynamic userClaim = JObject.Parse(context.User.ToString());
                               var roles = userClaim.role;
                               foreach (string role in roles)
                               {
                                   claimsId.AddClaim(new Claim("role", role));
                               }
                           }
                           catch (Exception)
                           {
                               //Users does not have roles
                           }
                           return Task.FromResult(0);
                       }

                   };

                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       NameClaimType = JwtClaimTypes.Name,
                       RoleClaimType = JwtClaimTypes.Role
                   };
               });

            //services.AddEntityFrameworkSqlServer().AddDbContext<TheCoreBankingContext>(options =>
            //  {
            //      options.UseSqlServer("Server=(local)\\fintrak;Database=TheCoreBanking;User Id=sa;password=sqluser10$;Trusted_Connection=True;MultipleActiveResultSets=true",
            //                           sqlOptions => sqlOptions.MigrationsAssembly("TheCoreBanking.Data"));
            //  });



            services.AddDbContext<TheCoreBankingContext>(options => options.UseSqlServer("Server=.\fintraksql;Database=TheCoreBanking;User Id=sa;Password=sqluser10$;"), ServiceLifetime.Scoped);
          
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddTransient<IUnitRepository, UnitRepository>();
            services.AddTransient<IBranchRepository, BranchRepository>();
            services.AddTransient<IMISRepository, MISRepository>();
            services.AddTransient<ICurrencyRepository, CurrencyRepository>();
            //services.AddTransient<ITitleRepository, TitleRepository>();
            //services.AddTransient<IServiceSetupRepository, ServiceSetupRepository>();
            //services.AddTransient<ISectorRepository, SectorRepository>();
            //services.AddTransient<IModelofIdRepository, ModelofIdRepository>();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<IDesignationRepository, DesignationRepository>();
            services.AddTransient<IRankRepository, RankRepository>();
            //services.AddTransient<IChequeConfirmationRepository, ChequeConfirmationRepository>();
            services.AddScoped<ISetupUnitOfWork, SetupUnitOfWork>();
            services.AddScoped<IRepositoryProvider, RepositoryProvider>();
            services.AddSingleton<RepositoryFactories>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Index");
            }

            //app.UseAuthentication();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=CompanySetup}/{action=Index}/{id?}");
            });
        }
    }
}
