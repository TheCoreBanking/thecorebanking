﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface ICompanyRepository : IRepository<TblCompanyInformation>
    {
        //IEnumerable<TblCompanyInformation> Company { get;}
        //TblCompanyInformation GetTblCompany(int id);
        IQueryable<TblCompanyInformation> ValidateCompany(string companyId);

    }
}
