﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    public interface IOperationApprovalRepository : IRepository<TblOperationApproval>
    {
        IQueryable<TblOperationApproval> GetByOperationid(int operateid);

        //IQueryable<TblOperationApproval> GetById(int[] Ref);
    }
}
