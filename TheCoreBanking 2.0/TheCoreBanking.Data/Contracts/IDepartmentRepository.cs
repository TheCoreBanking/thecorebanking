﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface IDepartmentRepository : IRepository<TblDepartment>
    {
        IQueryable<TblDepartment> ValidateDepartment(int departmentId, string companyId);

    }
}
