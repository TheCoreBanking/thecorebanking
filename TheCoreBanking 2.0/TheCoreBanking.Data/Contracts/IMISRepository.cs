﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface IMISRepository : IRepository<TblMisinformation>
    {
        IQueryable<TblMisinformation> ValidateMIS(int MISId);

    }
}
