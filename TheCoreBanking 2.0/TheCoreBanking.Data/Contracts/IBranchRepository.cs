﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface IBranchRepository : IRepository<TblBranchInformation>
    {
        IQueryable<TblBranchInformation> ValidateBranch(string companyId, int BranchID);

    }
}
