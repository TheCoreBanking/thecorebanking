


using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    /// <summary>
    /// Interface for the "Unit of Work"
    /// </summary>
    public interface ISetupUnitOfWork
    {
        // Save pending changes to the data store.
        void Commit();
        IDirectorRepository Director { get; }
        ICompanyRepository Company { get; }
        IBranchRepository Branch { get; }
        IMISRepository MIS { get; }
        //ITitleRepository Title { get; }
        IStaffRepository Staff { get; }
        ICountryRepository Country { get; }
        IStateRepository State { get; }
        ISecurityRepository Security { get; }
        ICurrencyRepository Currency { get; }
        IDepartmentRepository Department { get; }
        IDesignationRepository Designation { get; }
        IRankRepository Rank { get; }
        IUnitRepository Unit { get; }
        ICurrencyHistRepository currencyHist { get; }
        IOperationCommentRepository NarrateComment { get; }

        IOperationsRepository OperationLevel { get; }

        ICommitteeRepository Committee { get; }

        IOperationApprovalRepository OpeApprove { get; }

        IApprovalCommitteeRepository ApprovalCommittee { get; }
        // Repositories
        //IPersonsRepository Persons { get; }
        //IRepository<Room> Rooms { get; }
        //ISessionsRepository Sessions { get; }
        //IRepository<TimeSlot> TimeSlots { get; }
        //IRepository<Track> Tracks { get; }
        //IAttendanceRepository Attendance { get; }
    }
}