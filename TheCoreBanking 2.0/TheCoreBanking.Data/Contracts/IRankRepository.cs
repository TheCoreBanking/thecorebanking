﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface IRankRepository : IRepository<TblRank>
    {
        IQueryable<TblRank> ValidateRank(int ID);

    }
}
