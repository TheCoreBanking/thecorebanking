﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface IOperationsRepository : IRepository<TblOperationsLevel>
    {
        IQueryable<TblOperationsLevel> ValidateOperation(int OperationID);

    }
}
