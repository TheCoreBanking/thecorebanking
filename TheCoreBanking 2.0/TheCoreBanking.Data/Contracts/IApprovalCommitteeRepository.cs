﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface IApprovalCommitteeRepository : IRepository<TblOperationApprovalCommittee>
    {
   
        IQueryable<TblOperationApprovalCommittee> ValidateApproval(int ID);

    }
}
