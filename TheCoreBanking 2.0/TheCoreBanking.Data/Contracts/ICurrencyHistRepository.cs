﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    public interface ICurrencyHistRepository : IRepository<TblCurrencyHistory>
    {
        //IQueryable<TblCurrencyHistory> ArchiveCurrency(TblCurrencyHistory currency);
    }
}
