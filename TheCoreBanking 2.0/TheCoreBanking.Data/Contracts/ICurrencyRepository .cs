﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface ICurrencyRepository : IRepository<TblCurrency>
    {
        IQueryable<TblCurrency> ValidateCurrency(int CurrencyID);
        IQueryable<TblCurrency> GetByIdLong(long id);
    }
}
