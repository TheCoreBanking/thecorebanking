﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    
   public interface IStateRepository : IRepository<TblState>
    {
   
        IQueryable<TblState> ValidateState(int stateId);

    }
}
