﻿using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Contracts
{
    public interface IOperationCommentRepository : IRepository<GeneralSetupTblOperationComment>
    {
    }
}
