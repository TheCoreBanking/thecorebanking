using Microsoft.EntityFrameworkCore;
using System;
using TheCoreBanking.Data;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data
{
    /// <summary>
    /// The Code Camper "Unit of Work"
    ///     1) decouples the repos from the controllers
    ///     2) decouples the DbContext and EF from the controllers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="Person"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext in Code Camper).
    /// </remarks>
    public class SetupUnitOfWork : ISetupUnitOfWork, IDisposable
    {
        private TheCoreBankingContext DbContext = new TheCoreBankingContext();
        public SetupUnitOfWork(IRepositoryProvider repositoryProvider)
        {
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }
        protected IRepositoryProvider RepositoryProvider { get; set; }

        // Define repositories
        public IDirectorRepository Director { get { return GetEntityRepository<IDirectorRepository>(); } }
        public IUnitRepository Unit { get { return GetEntityRepository<IUnitRepository>(); } }
        public ICompanyRepository Company { get { return GetEntityRepository<ICompanyRepository>(); } }
        public IBranchRepository Branch { get { return GetEntityRepository<IBranchRepository>(); } }
        public IMISRepository MIS { get { return GetEntityRepository<IMISRepository>(); } }
        public ICurrencyRepository Currency { get { return GetEntityRepository<ICurrencyRepository>(); } }
        //public IChequeConfirmationRepository Cheque { get { return GetEntityRepository<IChequeConfirmationRepository>(); } }
        public IStaffRepository Staff { get { return GetEntityRepository<IStaffRepository>(); } }
        public ISecurityRepository Security { get { return GetEntityRepository<ISecurityRepository>(); } }
        public ICountryRepository Country { get { return GetEntityRepository<ICountryRepository>(); } }
        public IStateRepository State { get { return GetEntityRepository<IStateRepository>(); } }
        public IDepartmentRepository Department { get { return GetEntityRepository<IDepartmentRepository>(); } }
        public IDesignationRepository Designation { get { return GetEntityRepository<IDesignationRepository>(); } }
        public IRankRepository Rank { get { return GetEntityRepository<IRankRepository>(); } }
        public ICurrencyHistRepository currencyHist { get { return GetEntityRepository<ICurrencyHistRepository>(); } }
        public IOperationCommentRepository NarrateComment => GetEntityRepository<IOperationCommentRepository>();

        public IOperationsRepository OperationLevel => GetEntityRepository<IOperationsRepository>();

        public ICommitteeRepository Committee => GetEntityRepository<ICommitteeRepository>();

        public IOperationApprovalRepository OpeApprove => GetEntityRepository<IOperationApprovalRepository>();

        public IApprovalCommitteeRepository ApprovalCommittee => GetEntityRepository<IApprovalCommitteeRepository>();
        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        public void Commit()
        {
            //System.Diagnostics.Debug.WriteLine("Committed");
            DbContext.SaveChanges();
        }

     

      
        private IRepository<T> GetStandardRepository<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }
        private T GetEntityRepository<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

    
        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}