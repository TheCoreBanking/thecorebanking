﻿using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Repository
{
    public class OperationCommentRepository : EFRepository<GeneralSetupTblOperationComment>, IOperationCommentRepository
    {
        public OperationCommentRepository(TheCoreBankingContext context) : base(context) { }

    }
}
