﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class CommitteeRepository : EFRepository<TblBankingCommittee>, ICommitteeRepository
    {
        public CommitteeRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblBankingCommittee> ValidateCommittee(int CommitteeID)
        {
            return dbSet.Where(ps => ps.Id == CommitteeID);
        }

    }
}
