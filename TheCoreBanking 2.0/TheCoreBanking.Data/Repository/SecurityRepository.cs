﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Repository
{
      public class SecurityRepository : EFRepository<TblSecurityUsers>,ISecurityRepository
    {
        public SecurityRepository(TheCoreBankingContext context) : base(context) { }

            public IQueryable<TblSecurityUsers> ValidateSecurity(string StaffId)
        {
            return dbSet.Where(ps => ps.StaffNo == StaffId);
        }

    }
}
