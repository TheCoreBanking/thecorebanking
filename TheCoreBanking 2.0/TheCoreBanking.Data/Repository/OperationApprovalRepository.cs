﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;

namespace TheCoreBanking.Data.Repository
{
    public class OperationApprovalRepository : EFRepository<TblOperationApproval>, IOperationApprovalRepository
    {
        public OperationApprovalRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblOperationApproval> GetByOperationid(int operateid)
              => dbSet.Where(s => s.OperationId == operateid);

        //public IQueryable<TblOperationApproval> GetByid(int[] Ref)
        //    return dbSet.Where(i => i.Id == Ref);
    }
}
