﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class DirectorRepository : EFRepository<TblDirectorInformation>, IDirectorRepository
    {
        public DirectorRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblDirectorInformation> ValidateDirector(string id )
        {
            return dbSet.Where(ps => ps.CompanyId == id);
        }

    }
}
