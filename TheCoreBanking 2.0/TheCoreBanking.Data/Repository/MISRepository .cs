﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class MISRepository : EFRepository<TblMisinformation>, IMISRepository
    {
        public MISRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblMisinformation> ValidateMIS( int MISId )
        {
            return dbSet.Where(ps => ps.Id == MISId);
        }

     
    }
}
