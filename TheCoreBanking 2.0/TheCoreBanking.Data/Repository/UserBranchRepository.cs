﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class UserBranchRepository : EFRepository<UsertoBranch>, IUserBranchRepository
    {
        public UserBranchRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<UsertoBranch> ValidateUserBranch(int ID)
        {
            return dbSet.Where(ps => ps.Id == ID);
        }

 

    }
}
