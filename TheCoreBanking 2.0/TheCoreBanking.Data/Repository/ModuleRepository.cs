﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class ModuleRepository : EFRepository<TblOperationModule>, IModuleRepository
    {
        public ModuleRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblOperationModule> ValidateModule(int ModuleID)
        {
            return dbSet.Where(ps => ps.ModuleId == ModuleID);
        }

    }
}
