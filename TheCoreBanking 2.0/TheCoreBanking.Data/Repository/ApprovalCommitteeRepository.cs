﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class ApprovalCommitteeRepository : EFRepository<TblOperationApprovalCommittee>, IApprovalCommitteeRepository
    {
        public ApprovalCommitteeRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblOperationApprovalCommittee> ValidateApproval(int ID)
        {
            return dbSet.Where(ps => ps.Id == ID);
        }

 

    }
}
