﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class CurrencyRepository : EFRepository<TblCurrency>, ICurrencyRepository
    {
        public CurrencyRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblCurrency> ValidateCurrency(int CurrencyId )
        {
            return dbSet.Where(ps => ps.Id == CurrencyId);
        }
        public IQueryable<TblCurrency> GetByIdLong(long id)
        {
            return dbSet.Where(ps => ps.Id == id);
        }

    }
}
