﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class BranchDepartmentRepository : EFRepository<TblBranchDepartmentUnit>, IBranchDepartmentRepository
    {
        public BranchDepartmentRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblBranchDepartmentUnit> ValidateDepartBranch(int ID )
        {
            return dbSet.Where(ps => ps.Id == ID);
        }

    }
}
