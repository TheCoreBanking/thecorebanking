﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class OperationsRepository : EFRepository<TblOperationsLevel>, IOperationsRepository
    {
        public OperationsRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblOperationsLevel> ValidateOperation(int OperationID)
        {
            return dbSet.Where(ps => ps.OperationId == OperationID);
        }

    }
}
