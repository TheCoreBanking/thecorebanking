﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    
    public class CompanyRepository : EFRepository<TblCompanyInformation>, ICompanyRepository
    {
        //private readonly TheCoreBankingContext _theCoreBankingContext;
        public CompanyRepository(TheCoreBankingContext context) : base(context) { } 

        public IQueryable<TblCompanyInformation> ValidateCompany(string companyId)
        {
            return dbSet.Where(ps => ps.CoyId == companyId);
        }
    }
}
