﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class UnitRepository : EFRepository<TblUnit>, IUnitRepository
    {
        public UnitRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblUnit> ValidateUnit(string unitId)
        {
            return dbSet.Where(ps => ps.UnitCode == unitId);
        }

 

    }
}
