﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class CountryRepository : EFRepository<TblCountry>, ICountryRepository
    {
        public CountryRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblCountry> ValidateCountry(int countryId)
        {
            return dbSet.Where(ps => ps.Countryid == countryId);
        }

 

    }
}
