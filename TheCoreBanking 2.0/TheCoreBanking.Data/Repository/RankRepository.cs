﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    
    public class RankRepository : EFRepository<TblRank>, IRankRepository
    {
        //private readonly TheCoreBankingContext _theCoreBankingContext;
        public RankRepository(TheCoreBankingContext context) : base(context) { } 

        public IQueryable<TblRank> ValidateRank(int ID)
        {
            return dbSet.Where(ps => ps.Id == ID);
        }
    }
}
