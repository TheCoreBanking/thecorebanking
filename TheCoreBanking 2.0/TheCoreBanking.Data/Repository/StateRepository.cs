﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    public class StateRepository : EFRepository<TblState>, IStateRepository
    {
        public StateRepository(TheCoreBankingContext context) : base(context) { }

        public IQueryable<TblState> ValidateState(int stateId)
        {
            return dbSet.Where(ps => ps.Stateid == stateId);
        }

 

    }
}
