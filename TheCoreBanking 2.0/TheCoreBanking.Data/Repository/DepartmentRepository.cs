﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    
    public class DepartmentRepository : EFRepository<TblDepartment>, IDepartmentRepository
    {
        //private readonly TheCoreBankingContext _theCoreBankingContext;
        public DepartmentRepository(TheCoreBankingContext context) : base(context) { }   

        public IQueryable<TblDepartment> ValidateDepartment(int departmentId, string companyId)
        {
            return dbSet.Where(ps => ps.CoyId == companyId && ps.Id == departmentId);
        }
    }
}
