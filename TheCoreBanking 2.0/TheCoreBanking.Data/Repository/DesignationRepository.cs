﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBanking.Data.Contracts;
using TheCoreBanking.Data.Models;
using System.Linq;

namespace TheCoreBanking.Data.Repository
{
    
    public class DesignationRepository : EFRepository<TblDesignation>, IDesignationRepository
    {
        //private readonly TheCoreBankingContext _theCoreBankingContext;
        public DesignationRepository(TheCoreBankingContext context) : base(context) { } 

        public IQueryable<TblDesignation> ValidateDesignation(int DesignationId)
        {
            return dbSet.Where(ps => ps.Id == DesignationId);
        }
    }
}
