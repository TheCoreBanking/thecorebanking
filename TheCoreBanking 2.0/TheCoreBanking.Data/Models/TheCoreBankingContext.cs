﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TheCoreBanking.Data.Models
{
    public partial class TheCoreBankingContext : DbContext
    {

        public TheCoreBankingContext()
        {
        }

        public TheCoreBankingContext(DbContextOptions<TheCoreBankingContext> options)
               : base(options)
        {
        }
        public virtual DbSet<TblCountry> TblCountry { get; set; }
        public virtual DbSet<TblCurrencyHistory> TblCurrencyHistory { get; set; }
        public virtual DbSet<TblBranchDepartmentUnit> TblBranchDepartmentUnit { get; set; }
        public virtual DbSet<TblBranchInformation> TblBranchInformation { get; set; }
        public virtual DbSet<TblCompanyInformation> TblCompanyInformation { get; set; }
        public virtual DbSet<TblCurrency> TblCurrency { get; set; }
        public virtual DbSet<TblDepartment> TblDepartment { get; set; }
        public virtual DbSet<TblDesignation> TblDesignation { get; set; }
        public virtual DbSet<TblStaffInformation> TblStaffInformation { get; set; }
        public virtual DbSet<TblState> TblState { get; set; }
        public virtual DbSet<TblMisinformation> TblMisinformation { get; set; }
   
        public virtual DbSet<TblUnit> TblUnit { get; set; }

        public virtual DbSet<TblBankingCommittee> TblBankingCommittee { get; set; }

        public virtual DbSet<TblOperationApprovalCommittee> TblOperationApprovalCommittee { get; set; }
        public virtual DbSet<GeneralSetupTblOperationComment> GeneralSetupTblOperationComment { get; set; }

        public virtual DbSet<TblOperationsLevel> TblOperationsLevel { get; set; }

        public virtual DbSet<TblOperationApproval> TblOperationApproval { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                //optionsBuilder.UseSqlServer(@"Server=DLM-FINTRAK\fintraksql;Database=TheCoreBanking;User Id=sa;Password=sqluser10$;");
                optionsBuilder.UseSqlServer(@"Server=fintraksqlmmbs.database.windows.net;Database=TheCoreBankingAzure;user id=fintrak;password=Password20$");
               

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<GeneralSetupTblOperationComment>(entity =>
            {
                entity.ToTable("GeneralSetup.tbl_OperationComment");

                entity.Property(e => e.Id)
                    .HasColumnName("ID");


                entity.Property(e => e.BrCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.Property(e => e.CoyCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });



            modelBuilder.Entity<TblCurrencyHistory>(entity =>
            {
                entity.ToTable("tbl_CurrencyHistory");

                entity.Property(e => e.AverageRate).HasColumnType("decimal(10, 4)");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CurrName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CurrSymbol)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(10, 4)");

               // entity.Property(e => e.Code)
                 // .HasColumnName("CurrCode");

              //  entity.Property(e => e.DateUpdated)
                //   .HasColumnName("UpdatedDate");
            });


            modelBuilder.Entity<TblDirectorInformation>(entity =>
            {
                entity.ToTable("tbl_DirectorInformation", "GeneralSetup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Bvn)
                    .HasColumnName("BVN")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId)
                    .HasColumnName("CompanyID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PercentageShare)
                    .HasColumnType("decimal(18, 0)")
                    .HasDefaultValueSql("((0.0))");

                entity.Property(e => e.FullName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.HasSequence("GenerateInterestTransID");

            modelBuilder.HasSequence("seqGetNextBatchRef")
                .StartsAt(25000)
                .HasMin(25000);

            modelBuilder.HasSequence("TransactionSequence").HasMin(0);
            modelBuilder.Entity<TblState>(entity =>
            {
                entity.HasKey(e => e.Stateid);

                entity.ToTable("TBL_STATE", "Customer");

                entity.HasIndex(e => e.Statename)
                    .HasName("UQ__tbl_Stat__737584F619751F56")
                    .IsUnique();

                entity.Property(e => e.Stateid).HasColumnName("STATEID");

                entity.Property(e => e.Countryid).HasColumnName("COUNTRYID");

                entity.Property(e => e.Createdby)
                    .HasColumnName("CREATEDBY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Datetimecreated)
                    .HasColumnName("DATETIMECREATED")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lgaid).HasColumnName("LGAID");

                entity.Property(e => e.Regionid).HasColumnName("REGIONID");

                entity.Property(e => e.Statename)
                    .IsRequired()
                    .HasColumnName("STATENAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.HasSequence("seqGetNextBatchRef")
                .StartsAt(25000)
                .HasMin(25000);

            modelBuilder.HasSequence("TransactionSequence").HasMin(0);
            //modelBuilder.Entity<TblBankingChequeConfirmation>(entity =>
            //{
            //    entity.ToTable("tbl_BankingChequeConfirmation", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AccountNo)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.BeneficiaryName)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.BrCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.ChequeNo)
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.ConfirmedBy)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.ConfirmedFrom)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CoyCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.DateConfirmed).HasColumnType("datetime");

            //    entity.Property(e => e.IsApproved).HasColumnName("Is_Approved");

            //    entity.Property(e => e.PnoneNumber)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblBankingChequeLocationSetup>(entity =>
            //{
            //    entity.ToTable("tbl_BankingChequeLocationSetup", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AccountName)
            //        .HasMaxLength(500)
            //        .IsUnicode(false);

            //    entity.Property(e => e.AccountNo)
            //        .HasMaxLength(500)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Location)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblBankingCustomerPrivilege>(entity =>
            //{
            //    entity.ToTable("tbl_BankingCustomerPrivilege", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.CoyCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CreatedBy)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.DateCreated).HasColumnType("smalldatetime");

            //    entity.Property(e => e.DateLastUpdated).HasColumnType("smalldatetime");

            //    entity.Property(e => e.GetUser)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblBankingModeofId>(entity =>
            //{
            //    entity.ToTable("tbl_BankingModeofID", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Coy)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CreateDate).HasColumnType("datetime");

            //    entity.Property(e => e.CreatedBy)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Idmode)
            //        .HasColumnName("IDMode")
            //        .HasMaxLength(200)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblBankingProductChargesSetup>(entity =>
            //{
            //    entity.ToTable("tbl_BankingProductChargesSetup", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.BranchCode)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CoyCode)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Op)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Operation)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.PdTypeId).HasColumnName("PdTypeID");

            //    entity.Property(e => e.ProdType)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Product)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.Targets)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblBankingSector>(entity =>
            //{
            //    entity.ToTable("tbl_BankingSector", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Sector)
            //        .HasMaxLength(500)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblBankingSensitiveCustomers>(entity =>
            //{
            //    entity.ToTable("tbl_BankingSensitiveCustomers", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Approvedby)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.BrCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CoyCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CreatedBy)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CustCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CustomerName)
            //        .HasMaxLength(500)
            //        .IsUnicode(false);

            //    entity.Property(e => e.DateApproved).HasColumnType("smalldatetime");

            //    entity.Property(e => e.Dateccreated).HasColumnType("smalldatetime");

            //    entity.Property(e => e.OperationId).HasColumnName("OperationID");
            //});

            //modelBuilder.Entity<TblBankingSensitiveUsers>(entity =>
            //{
            //    entity.ToTable("tbl_BankingSensitiveUsers", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Approvedby)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.BrCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CoyCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CreatedBy)
            //        .HasMaxLength(100)
            //        .IsUnicode(false);

            //    entity.Property(e => e.DateApproved).HasColumnType("smalldatetime");

            //    entity.Property(e => e.Dateccreated).HasColumnType("smalldatetime");

            //    entity.Property(e => e.OperationId).HasColumnName("OperationID");

            //    entity.Property(e => e.StaffNo)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.UserName)
            //        .HasMaxLength(500)
            //        .IsUnicode(false);
            //});

            modelBuilder.Entity<TblBranchDepartmentUnit>(entity =>
            {
                entity.ToTable("tbl_BranchDepartmentUnit", "GeneralSetup");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CoyId)
                    .HasColumnName("CoyID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Department)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeptCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnitName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblBranchInformation>(entity =>
            {
                entity.ToTable("tbl_BranchInformation", "GeneralSetup");

                entity.Property(e => e.BrAddress)
                    .HasColumnName("brAddress")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BrId)
                    .HasColumnName("brID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BrLocation)
                    .HasColumnName("brLocation")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.BrManager)
                    .HasColumnName("brManager")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.BrName)
                    .HasColumnName("brName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BrState)
                    .HasColumnName("brState")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CoyId)
                    .HasColumnName("coyID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCompanyInformation>(entity =>
            {
                entity.ToTable("tbl_CompanyInformation", "GeneralSetup");

                entity.Property(e => e.AccountStand)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.AuthorisedShareCapital).HasColumnType("money");

                entity.Property(e => e.CompanyClass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CoyClass)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CoyId)
                    .HasColumnName("coyID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CoyName)
                    .HasColumnName("coyName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CoyRegisteredBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfCommencement).HasColumnType("datetime");

                entity.Property(e => e.DateOfIncorporation).HasColumnType("datetime");

                entity.Property(e => e.DateOfRenewalOfRegistration).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EoyprofitAndLossGl)
                    .HasColumnName("EOYProfitAndLossGL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FormerManagersTrustees)
                    .HasColumnName("FormerManagers_Trustees")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FunctionsRegistered)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvestmentObjective).IsUnicode(false);

                entity.Property(e => e.ManagementType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Manager)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameOfRegistrar)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NameOfScheme).IsUnicode(false);

                entity.Property(e => e.NameOfTrustees)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NatureOfBusiness)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TrusteesAddress).IsUnicode(false);

                entity.Property(e => e.Webbsite)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CoyCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Deleted).HasColumnName("DELETED");
            });

            modelBuilder.Entity<TblCurrency>(entity =>
            {
                entity.ToTable("tbl_Currency", "GeneralSetup");

                entity.Property(e => e.AverageRate).HasColumnType("decimal(10, 4)");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CurrName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CurrSymbol)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ExchangeRate).HasColumnType("decimal(10, 4)");
            });

            modelBuilder.Entity<TblDepartment>(entity =>
            {
                entity.ToTable("tbl_Department", "GeneralSetup");


                entity.Property(e => e.CoyId)
                    .HasColumnName("CoyID")
                    .HasMaxLength(50)
                    
                    .IsUnicode(false);


                entity.Property(e => e.Department)
                    .HasColumnName("Department")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DeptCode)
                    .HasColumnName("DeptCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Remark)
                 .HasColumnName("Remark")
                    .HasMaxLength(500)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<TblDesignation>(entity =>
            {
                entity.ToTable("tbl_Designation", "GeneralSetup");

                entity.Property(e => e.Designation)
                .HasColumnName("Designation")
                    .HasMaxLength(100)
                    .IsUnicode(false);
                entity.Property(e => e.DesignationCode)
              .HasColumnName("DesignationCode")
                  .HasMaxLength(100)
                  .IsUnicode(false);
            });
            modelBuilder.Entity<TblCountry>(entity =>
            {
                entity.HasKey(e => e.Countryid);

                entity.ToTable("TBL_COUNTRY", "Customer");

                entity.HasIndex(e => e.Name)
                    .HasName("UQ__tbl_Coun__737584F6A66ACCD7")
                    .IsUnique();

                entity.Property(e => e.Countryid).HasColumnName("COUNTRYID");

                entity.Property(e => e.Countrycode)
                    .HasColumnName("COUNTRYCODE")
                    .HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(100);
            });

            modelBuilder.HasSequence("seqGetNextBatchRef")
                .StartsAt(25000)
                .HasMin(25000);

            modelBuilder.HasSequence("TransactionSequence").HasMin(0);
            //modelBuilder.Entity<TblFinanceAccountCategory>(entity =>
            //{
            //    entity.ToTable("tbl_FinanceAccountCategory", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AccountGroupId).HasColumnName("AccountGroupID");

            //    entity.Property(e => e.Description)
            //        .IsRequired()
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblFinanceAccountGroup>(entity =>
            //{
            //    entity.ToTable("tbl_FinanceAccountGroup", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Description)
            //        .IsRequired()
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblFinanceAccountSub>(entity =>
            //{
            //    entity.ToTable("tbl_FinanceAccountSub", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AccountId)
            //        .IsRequired()
            //        .HasColumnName("AccountID")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.AccountSubId)
            //        .IsRequired()
            //        .HasColumnName("AccountSubID")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.AccountSubName)
            //        .IsRequired()
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.MsreplTranVersion).HasColumnName("msrepl_tran_version");

            //    entity.Property(e => e.UserName)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblFinanceChartOfAccount>(entity =>
            //{
            //    entity.ToTable("tbl_FinanceChartOfAccount", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.AccountCategoryId).HasColumnName("AccountCategoryID");

            //    entity.Property(e => e.AccountGroupId).HasColumnName("AccountGroupID");

            //    entity.Property(e => e.AccountId)
            //        .IsRequired()
            //        .HasColumnName("AccountID")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.AccountName)
            //        .IsRequired()
            //        .HasMaxLength(500)
            //        .IsUnicode(false);

            //    entity.Property(e => e.AccountTypeId).HasColumnName("AccountTypeID");

            //    entity.Property(e => e.BrId)
            //        .HasColumnName("brID")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CoyId)
            //        .HasColumnName("coyID")
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.DateCreated).HasColumnType("smalldatetime");

            //    entity.Property(e => e.OldAccountId)
            //        .HasColumnName("OldAccountID")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.OldAccountId1)
            //        .HasColumnName("OldAccountID1")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.OldAccountId2)
            //        .HasColumnName("OldAccountID2")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.StCode).HasColumnName("stCode");

            //    entity.Property(e => e.UserName)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblFinanceCostCenter>(entity =>
            //{
            //    entity.ToTable("tbl_FinanceCostCenter", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Description)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            //modelBuilder.Entity<TblFinanceDefaultAccounts>(entity =>
            //{
            //    entity.HasKey(e => e.DfId);

            //    entity.ToTable("tbl_FinanceDefaultAccounts", "GeneralSetup");

            //    entity.Property(e => e.DfId).HasColumnName("dfID");

            //    entity.Property(e => e.AccountId)
            //        .HasColumnName("AccountID")
            //        .HasMaxLength(20)
            //        .IsUnicode(false);

            //    entity.Property(e => e.AccountName)
            //        .HasMaxLength(500)
            //        .IsUnicode(false);

            //    entity.Property(e => e.ApprovedBy)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CoyCode)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.CreatedBy)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.DateApproved).HasColumnType("smalldatetime");

            //    entity.Property(e => e.DateCreated).HasColumnType("smalldatetime");

            //    entity.Property(e => e.DfDescription)
            //        .HasColumnName("dfDescription")
            //        .HasMaxLength(500)
            //        .IsUnicode(false);

            //    entity.Property(e => e.FinancePnc).HasColumnName("FinancePNC");

            //    entity.Property(e => e.FinancePnd).HasColumnName("FinancePND");

            //    entity.Property(e => e.LastUpdate).HasColumnType("smalldatetime");

            //    entity.Property(e => e.TellerPnc).HasColumnName("TellerPNC");

            //    entity.Property(e => e.TellerPnd).HasColumnName("TellerPND");

            //    entity.Property(e => e.UpdatedBy)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);
            //});

            modelBuilder.Entity<TblMisinformation>(entity =>
            {
                entity.ToTable("tbl_MISInformation", "GeneralSetup");

                entity.Property(e => e.CompanyCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MisCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
                
                 entity.Property(e => e.MisTypeId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
                entity.Property(e => e.MisName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ParentMisCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblOperationApproval>(entity =>
            {
                entity.ToTable("tbl_OperationApproval", "GeneralSetup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Approvedby)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovingAuthority).IsUnicode(false);

                entity.Property(e => e.BranchCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.Property(e => e.CommitteeType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CummulativeAmt).HasColumnType("money");

                entity.Property(e => e.DateApproved).HasColumnType("datetime");

                entity.Property(e => e.Designation)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaxAmt).HasColumnType("money");

                entity.Property(e => e.MinAmt).HasColumnType("money");

                entity.Property(e => e.Miscode)
                    .HasColumnName("MISCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OpId).HasColumnName("OpID");

                entity.Property(e => e.OperationId).HasColumnName("OperationID");

                entity.Property(e => e.Ref)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StaffId)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<TblOperationApprovalCommittee>(entity =>
            {
                entity.ToTable("tbl_OperationApprovalCommittee", "GeneralSetup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MaxAmt).HasColumnName("MaxAmt");

                entity.Property(e => e.MinAmt).HasColumnName("MinAmt");

                entity.Property(e => e.ApprovingAuthority).IsUnicode(false);

                entity.Property(e => e.BranchCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CommitteeType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CummulativeAmt).HasColumnType("money");

                entity.Property(e => e.Department).HasColumnName("Department");


                entity.Property(e => e.Miscode)
                    .HasColumnName("MISCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OperationId).HasColumnName("OperationID");

                entity.Property(e => e.StaffId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
               .HasColumnName("COMMENT")
               .HasMaxLength(500)
                .IsUnicode(false);

                entity.Property(e => e.ApprovedBy)
                  .HasColumnName("APPROVEDBY")
                  .HasMaxLength(50)
                  .IsUnicode(false);

                entity.Property(e => e.Approved).HasColumnName("Approved");

                entity.Property(e => e.Disapproved).HasColumnName("Disapproved");


                entity.Property(e => e.BoardNo).HasColumnName("BoardNo");

                entity.Property(e => e.BoardNoApproval).HasColumnName("BoardNoApproval");

                entity.Property(e => e.CreditMaxBoardAmt).HasColumnName("CreditMaxBoardAmt");

                entity.Property(e => e.CreditMaxComteeAmt).HasColumnName("CreditMaxComteeAmt");

                entity.Property(e => e.CreditMinBoard).HasColumnName("CreditMinBoard");

                entity.Property(e => e.CreditMinMgt).HasColumnName("CreditMinMgt");

                entity.Property(e => e.MgtNo).HasColumnName("MgtNo");

                entity.Property(e => e.MgtNoApproval).HasColumnName("MgtNoApproval");
            });
            modelBuilder.Entity<TblRank>(entity =>
            {
                entity.ToTable("tbl_Rank", "GeneralSetup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Rank)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<TblOperationsLevel>(entity =>
            {
                entity.HasKey(e => e.OperationId);

                entity.ToTable("tbl_OperationsLevel", "GeneralSetup");

                entity.Property(e => e.OperationId).HasColumnName("OperationID");

                entity.Property(e => e.ApprovalLevels).HasColumnName("ApprovalLevels");


                entity.Property(e => e.SetupStatus)
                   .HasColumnName("SetupStatus")
                   .HasMaxLength(50)
                   .IsUnicode(false);

                entity.Property(e => e.Operations).IsUnicode(false);
            });


            //modelBuilder.Entity<TblServiceSetup>(entity =>
            //{
            //    entity.ToTable("tbl_ServiceSetup", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Date).HasColumnType("datetime");
            //});

            //modelBuilder.Entity<TblTitle>(entity =>
            //{
            //    entity.ToTable("tbl_Title", "GeneralSetup");

            //    entity.Property(e => e.Id).HasColumnName("ID");

            //    entity.Property(e => e.Title)
            //        .IsRequired()
            //        .HasMaxLength(250)
            //        .IsUnicode(false);
            //});
            modelBuilder.Entity<TblStaffInformation>(entity =>
            {
                entity.ToTable("tbl_StaffInformation", "GeneralSetup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Age).HasColumnType("datetime");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BranchID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId)
                    .HasColumnName("CompanyID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Department)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DeptCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Miscode)
                    .HasColumnName("MISCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Nationality)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NextOfKinAddress)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.NextOfKinEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NextOfKinGender)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NextOfKinName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NextOfKinPhone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PcCode)
                    .HasColumnName("pcCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Rank)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RelationShip)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StaffName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StaffNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Staffsignature).HasColumnType("image");

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Unit)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<TblBankingCommittee>(entity =>
            {
                entity.ToTable("tbl_BankingCommittee", "GeneralSetup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BrCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CoyCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Createdby)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                .HasColumnName("COMMENT")
                .HasMaxLength(500)
                 .IsUnicode(false);

                entity.Property(e => e.CommitteeType)
                   .HasMaxLength(50)
                   .IsUnicode(false);

                entity.Property(e => e.ApprovedBy)
                  .HasColumnName("APPROVEDBY")
                  .HasMaxLength(50)
                  .IsUnicode(false);

                entity.Property(e => e.Approved).HasColumnName("Approved");

                entity.Property(e => e.Disapproved).HasColumnName("Disapproved");


                entity.Property(e => e.OperationId).HasColumnName("OperationID");

                entity.Property(e => e.BoardNo).HasColumnName("BoardNo");

                entity.Property(e => e.BoardNoApproval).HasColumnName("BoardNoApproval");

                entity.Property(e => e.CreditMaxBoardAmt).HasColumnName("CreditMaxBoardAmt");

                entity.Property(e => e.CreditMaxComteeAmt).HasColumnName("CreditMaxComteeAmt");

                entity.Property(e => e.CreditMinBoard).HasColumnName("CreditMinBoard");

                entity.Property(e => e.CreditMinMgt).HasColumnName("CreditMinMgt");

                entity.Property(e => e.MgtNo).HasColumnName("MgtNo");

                entity.Property(e => e.MgtNoApproval).HasColumnName("MgtNoApproval");
            });


            modelBuilder.Entity<TblUnit>(entity =>
            {
                entity.ToTable("tbl_Unit", "GeneralSetup");

                //entity.Property(e => e.Id).HasColumnName("ID");

                //entity.Property(e => e.BrCode)
                //    .HasMaxLength(50)
                //    .IsUnicode(false);

                //entity.Property(e => e.CoyCode)
                //    .HasMaxLength(50)
                //    .IsUnicode(false);

                //entity.Property(e => e.CreatedBy)
                //    .HasMaxLength(50)
                //    .IsUnicode(false);

                //entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Remark)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnitName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
