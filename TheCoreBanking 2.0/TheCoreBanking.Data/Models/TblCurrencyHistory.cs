﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBanking.Data.Models
{
    public partial class TblCurrencyHistory
    {
        public long Id { get; set; }
        public int? CurrCode { get; set; }
        public string CurrName { get; set; }
        public string CurrSymbol { get; set; }
        public decimal? ExchangeRate { get; set; }
        public string CountryCode { get; set; }
        public decimal? AverageRate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
