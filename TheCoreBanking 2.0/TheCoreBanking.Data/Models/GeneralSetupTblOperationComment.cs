﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCoreBanking.Data.Models
{
    public partial class GeneralSetupTblOperationComment
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public string BrCode { get; set; }
        public string CoyCode { get; set; }
    }
}
