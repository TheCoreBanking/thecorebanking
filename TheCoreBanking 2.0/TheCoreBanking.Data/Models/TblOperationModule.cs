﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCoreBanking.Data.Models
{
    public class TblOperationModule
    {
    
        public int ModuleId { get; set; }
        public string ModuleOperation { get; set; }
        public int OperationId { get; set; }
        public TblOperationsLevel TblOperationsLevel { get; set; }
       
    }
}
