﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheCoreBanking.Data.Models
{
    public partial class TblDepartment
    {

        public long Id { get; set; }
       
        public string CoyId { get; set; }
        public string Department { get; set; }
        public string Remark { get; set; }
        public string DeptCode { get; set; }
        
    

    }
}
