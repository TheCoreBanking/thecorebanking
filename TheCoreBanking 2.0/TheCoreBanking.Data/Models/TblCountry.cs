﻿using System;
using System.Collections.Generic;

namespace TheCoreBanking.Data.Models
{
    public partial class TblCountry
    {
        public int Countryid { get; set; }
        public string Name { get; set; }
        public string Countrycode { get; set; }
    }
}
