﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheCoreBanking.Data.Models
{
    public partial class TblDesignation
    {

        public long Id { get; set; }
        public string Designation { get; set; }
        public string DesignationCode { get; set; }
    }
}
