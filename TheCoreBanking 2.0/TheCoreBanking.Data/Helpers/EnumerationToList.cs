﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCoreBanking.Data.Helpers
{
    public class EnumerationToList : Enumeration
    {
        public static EnumerationToList Current = new EnumerationToList(1, "Current/Saving Receiving");
        public static EnumerationToList Non = new EnumerationToList(2, "Non Current/Saving Receiving");
      

        protected EnumerationToList() { }

        public EnumerationToList(int id, string name)
            : base(id, name)
        {
        }

        public static IEnumerable<EnumerationToList> List()
        {
            return new[] { Current, Non };
        }
        // Other util methods
    }
}
