﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}

function branchFormatter(value, row, index) {
    return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edit btn btn-sm btn-info"  title="Edit Branch">'
        + '<i class="fas fa-edit"></i>' +
        '<a style="color:white" title="Remove branch" class="remove btn btn-sm btn-danger">'
        + '<i class="fas fa-trash"></i></a>' +
        '</a> '
    ].join('');
}

window.branchEvents = {
    'click .edit': function (e, value, row, index) {

        if (row.state = true) {

            var data = JSON.stringify(row);

            $('#Id').val(row.id);
            $('#BrId').val(row.brId);           
            $('#ddlCompany').val(row.coyID).trigger('change');
            $('#BrAddress').val(row.brAddress);
            $('#BrLocation').val(row.brLocation);
            $('#ddlStates').val(row.brState).trigger('change');
            $('#BrManager').val(row.brManager);
            $('#BrName').val(row.brName);          
            $('#AddNewBranch').modal('show');
            $('#btnBranchUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            $('#btnBranchUpdate').show();
            $('#btnBranch').hide();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);
      
        debugger

        swal({
            title: "Are you sure?",
            text: "Branch will be deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ff9800",
            confirmButtonText: "Yes, continue",
            cancelButtonText: "No, stop!",
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        resolve();
                    }, 4000);
                });
            }
        }).then(function (isConfirm) {
            if (isConfirm) {


                debugger
               

                $.ajax({
                    url: url_path + '/Companysetup/RemoveBranch',
                    type: 'POST',
                    data: {
                        Id: row.id,
                        BrId: row.brId,
                        CoyId: row.coyId,
                        BrAddress: row.brAddress,
                        BrLocation: row.brLocation,
                        BrState: row.brState,
                        BrManager: row.brManager,
                        BrName: row.brName
                    },
                    //dataType: "json",
                   
                    success: function (result) {

                        if (result.toString != '' && result != null) {
                            swal({ title: 'Delete branch', text: 'Branch deleted successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                            // $('#AddNewCompany').modal('hide')
                            $('#branchTable').
                                bootstrapTable(
                                    'refresh', { url: url_path + '/Companysetup/listbranch' });

                           

                        }
                        else {
                            swal({ title: 'Delete branch', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                           
                        }
                    },
                    error: function (e) {
                        swal({ title: 'Delete branch', text: 'deleting branch encountered an error', type: 'error' }).then(function () { clear(); });
                     
                    }
                });
            }
        });

    
    }

};

function updateBranch() {
    debugger

    var json_data = {};

    json_data.Id = $('#Id').val();
    json_data.BrId= $('#BrId').val();
    json_data.CoyId = $('#ddlCompany').val();
    json_data.BrAddress= $('#BrAddress').val();
    json_data.BrLocation= $('#BrLocation').val();
    json_data.BrState = $('#ddlStates').val();
    json_data.BrManager= $('#BrManager').val();
    json_data.BrName= $('#BrName').val();   

    $("input[type=submit]").attr("disabled", "disabled");

    $('#frmbranch').validate({

        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Branch will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnBranchUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: url_path +'/Companysetup/UpdateBranch',
                        type: 'POST',
                        data: json_data,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Branch', text: 'Branch updated successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewBranch').modal('hide')
                                $('#branchTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path +'/Companysetup/listbranch' });

                                $("#btnBranchUpdate").removeAttr("disabled");

                            }
                            else {
                                swal({ title: 'Update Branch', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnBranchUpdate").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Branch', text: 'Branch update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnBranchUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Branch', 'You cancelled branch update.', 'error');
            $("#btnBranchUpdate").removeAttr("disabled");
        });

}



$(document).ready(function ($) {

    $('#btnBranch').on('click', function () {
        debugger

        addBranch();

    });

});
function addBranch() {
    debugger
    $('#btnBranchUpdate').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmbranch').validate({
        messages: {
          
            BrAddress: { required: "Branch Address is required" },
            BrId: { required: "Branch Code is required" },
            BrLocation: { required: "Branch Location is required" },
            BrState: { required: "Branch State is required" },
            BrManager: { required: "Branch Manager is required" },
            BrName: { required: "Branch Name is required" }
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Branch will be Added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnBranch").attr("disabled", "disabled");

                    debugger
                    var json_data = {};

                    json_data.Id = $('#Id').val();
                    json_data.BrId= $('#BrId').val();
                    json_data.CoyId = $('#ddlCompany').val();
                    json_data.BrAddress = $('#BrAddress').val();
                    json_data.BrLocation = $('#BrLocation').val();
                    json_data.BrState = $('#ddlStates').val();
                    json_data.BrManager = $('#BrManager').val();
                    json_data.BrName = $('#BrName').val();   
                    $.ajax({
                        url: url_path +'/Companysetup/AddBranch',
                        type: 'POST',
                        data: json_data,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                            
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Add Branch', text: 'Branch added successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewBranch').modal('hide');
                                $('#branchTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path +'/Companysetup/listbranch' });

                                $("#btnBranch").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Branch', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnBranch").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Branch', text: 'Adding branch encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnBranch").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Branch', 'You cancelled add branch.', 'error');
            $("#btnBranch").removeAttr("disabled");
        });

}
$(document).ready(function ($) {
    debugger
    //for dropdown list control for Comapny (id=ddlCompany)starts here

    $("#ddlCompany").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",

    });

    $.ajax({
        url: url_path +'/Companysetup/loadCompany',
    }).then(function (response) {
        debugger
        $("#ddlCompany").select2({
            theme: "bootstrap4",
            // placeholder: "Select Company...",  
            width: '100%',
            data: response.results
        });
    });
});


$(document).ready(function ($) {
    $('#btnBranchUpdate').hide();
    $('#btnBranch').show();
    $('#btnBranchUpdate').on('click', function () {
        debugger
        updateBranch();
    });

});
function reloadpage() {
    location.reload();
}

function clear() {
    $('#Id').val('');
    $('#BrId').val('');
   
    $('#CoyId').val('');
    $('#BrAddress').val('');
    $('#BrLocation').val('');
    $('#BrState').val('');
    $('#BrManager').val('');
    $('#BrName').val('');  
}

$('#branchTable').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<h8>' +
        '<div class="content"><div class="card"><div class="card-body">' +
        '<div class="row"><p>' +
        '<div class="col-md-6 "><strong style="color:red"> Branch Code:</strong> &nbsp' + row.brId + '' + '</div>'+
       
        ' <div class="col-md-6 "><strong style="color:red"> Branch Location: </strong>&nbsp' + row.brLocation + '' + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Branch State: </strong>&nbsp' + row.brState + '' + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Branch Manager: </strong>&nbsp' + row.brManager + '' + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Branch Name: </strong>&nbsp' + row.brName + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Branch Address: </strong> &nbsp' + row.brAddress + '' + '</div>' +'';


    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});


