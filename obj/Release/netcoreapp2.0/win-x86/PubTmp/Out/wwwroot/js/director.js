﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}

function DirectorFormatter(value, row, index) {
    return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edit btn btn-sm btn-info"  title="Edit Director">'
        + '<i class="fas fa-edit"></i>' +
        '<a style="color:white" title="Remove Director" class="remove btn btn-sm btn-danger">'
        + '<i class="fas fa-trash"></i></a>' +
        '</a> '
    ].join('');
}

window.DirectorEvents = {
    'click .edit': function (e, value, row, index) {

        if (row.state = true) {

            var data = JSON.stringify(row);

            $('#Id').val(row.id);
            $('#Bvn').val(row.bvn);           
            $('#ddlDirCompany').val(row.companyId).trigger('change');
            $('#PercentageShare').val(row.percentageShare);
            $('#Position').val(row.position);
            $('#FullName').val(row.fullName);                    
            $('#AddNewDirector').modal('show');
            $('#btnDirectorUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            $('#btnDirectorUpdate').show();
            $('#btnDirector').hide();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);
      
        debugger
        $('#ID').val(row.id);
        $.ajax({
            url: url_path +'/Companysetup/RemoveDirector',
            type: 'POST',
            data: { ID: row.id },
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {



                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#DirectorTable').
                            bootstrapTable(
                                'refresh', { url: url_path +'/Companysetup/listDirector' });

                        //return false;
                    }
                    else {
                        swal("Director", "You cancelled add Director.", "error");
                    }
                    $('#DirectorTable').
                        bootstrapTable(
                            'refresh', { url: url_path +'/Companysetup/listDirector' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};

function updateDirector() {
    debugger
   
    var json_data = {};
    json_data.Id = $('#Id').val();
    json_data.Bvn = $('#Bvn').val();
    json_data.CompanyId = $('#ddlDirCompany').val();
    json_data.PercentageShare = $('#PercentageShare').val();
    json_data.Position = $('#Position').val();
    json_data.FullName = $('#FullName').val();
      

    $("input[type=submit]").attr("disabled", "disabled");

    $('#frmDirector').validate({

        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Director will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnDirectorUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: url_path +'/Companysetup/UpdateDirector',
                        type: 'POST',
                        data: json_data,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Director', text: 'Director updated successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewDirector').modal('hide')
                                $('#DirectorTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path +'/Companysetup/listDirector' });

                                $("#btnDirectorUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Director', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnDirectorUpdate").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Director', text: 'Director update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnDirectorUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Director', 'You cancelled Director update.', 'error');
            $("#btnDirectorUpdate").removeAttr("disabled");
        });

}



$(document).ready(function ($) {

    $('#btnDirector').on('click', function () {
        debugger

        addDirector();

    });

});
function addDirector() {
    debugger
    $('#btnDirectorUpdate').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmDirector').validate({
        rules: {
            Bvn: {
                digits: true,
                maxlength: 10
            }
        },


        messages: {
            Bvn: {
                required: "BVN number is required",
                digits: "BVN can only contain digits",
                maxlength: jQuery.validator.format("BVN number cannot exceed {0} characters")
            },

          
            ignore: false,
        },
        messages: {
      
            Bvn: { required: "Director BVN is required" },
            PercentageShare: { required: "Director Percentage Share is required" },
            Position: { required: "Director Position is required" },
            FullName: { required: "Director FullName is required" },
           
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Director will be Added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnDirector").attr("disabled", "disabled");

                    debugger
                    
                    var json_data = {};
                    json_data.Id = $('#Id').val();
                    json_data.Bvn = $('#Bvn').val();
                    json_data.CompanyId = $('#ddlDirCompany').val();
                    json_data.PercentageShare = $('#PercentageShare').val();
                    json_data.Position = $('#Position').val();
                    json_data.FullName = $('#FullName').val();  
                    $.ajax({
                        url: url_path +'/Companysetup/AddDirector',
                        type: 'POST',
                        data: json_data,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                            
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Add Director', text: 'Director added successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewDirector').modal('hide');
                                $('#DirectorTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path +'/Companysetup/listDirector' });

                                $("#btnDirector").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Director', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnDirector").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Director', text: 'Adding Director encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnDirector").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Director', 'You cancelled add Director.', 'error');
            $("#btnDirector").removeAttr("disabled");
        });

}
$(document).ready(function ($) {
    debugger
    //for dropdown list control for Comapny (id=ddlCompany)starts here

    $("#ddlDirCompany").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",

    });

    $.ajax({
        url: url_path +'/Companysetup/loadCompany',
    }).then(function (response) {
        debugger
        $("#ddlDirCompany").select2({
            theme: "bootstrap4",
            // placeholder: "Select Company...",  
            width: '100%',
            data: response.results
        });
    });
});


$(document).ready(function ($) {
    $('#btnDirectorUpdate').hide();
    $('#btnDirector').show();
    $('#btnDirectorUpdate').on('click', function () {
        debugger
        updateDirector();
    });

});
function reloadpage() {
    location.reload();
}

function clear() {
    $('#Id').val('');
    $('#Bvn').val('');
   
    $('#FullName').val('');
    $('#Position').val('');
    $('#PercentageShare').val('');
    $('#ddlDirCompany').val('').trigger('change');
 
}

$('#DirectorTable').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<h8>' +
        '<div class="content"><div class="card"><div class="card-body">' +
        '<div class="row"><p>' +
        '<div class="col-md-6 "><strong style="color:red"> Director Code:</strong> &nbsp' + row.brId + '' + '</div>'+
       
        ' <div class="col-md-6 "><strong style="color:red"> Director Location: </strong>&nbsp' + row.brLocation + '' + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Director State: </strong>&nbsp' + row.brState + '' + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Director Manager: </strong>&nbsp' + row.brManager + '' + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Director Name: </strong>&nbsp' + row.brName + '</div>' +
        ' <div class="col-md-6 "><strong style="color:red"> Director Address: </strong> &nbsp' + row.brAddress + '' + '</div>' +'';


    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});


