﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}
function designationFormatter(value, row, index) {
    return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edit btn btn-sm btn-info "  title="Edit Designation">'
        + '<i class="fas fa-edit"></i>' +
        '<a style="color:white"  title="Remove designation" class="remove btn btn-sm btn-danger">'
        + '<i class="fas fa-trash"></i></a>' +
        '</a> '
    ].join('');
}

window.designationEvents = {
    'click .edit': function (e, value, row, index) {
      
        if (row.state = true) {

            var data = JSON.stringify(row);
            $('#Id').val(row.id);
            $('#Designation').val(row.designation); 
            $('#DesignationCode').val(row.designationCode).attr('disabled','disabled');
            $('#AddNewDesignation').modal('show');            
            $('#btnDesignationUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            $('#btnDesignationUpdate').show();
            $('#btnDesignation').hide();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);  
        debugger
        $('#ID').val(row.Id);
        $.ajax({
            url: url_path + '/RemoveDesignation',
            type: 'POST',
            data: { ID: row.id },
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {



                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#designationTable').
                            bootstrapTable(
                                'refresh', { url: url_path + '/listdesignation' });

                        //return false;
                    }
                    else {
                        swal("Designation", "You cancelled delete designation.", "error");
                    }
                    $('#designationTable').
                        bootstrapTable(
                            'refresh', { url: url_path + '/listdesignation' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }
};


$(document).ready(function ($) {
    $('#btnDesignationUpdate').hide();
    $('#btnDesignation').show();
    $('#btnDesignationUpdate').on('click', function () {
        updateDesignation();
      
    });

});
$(document).ready(function ($) {
   
    $('#btnDesignation').on('click', function () {
        debugger
        addDesignation();     
    });
});
function addDesignation() {
    debugger
    $('#btnDesignationUpdate').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmdesignation').validate({
        messages: {
            Designation: { required: "Designation is required" }
         },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Desigantion will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnDesignation").attr("disabled", "disabled");

                    debugger
                    var json_data = {};
                    json_data.Id = $('#Id').val();
                    json_data.Designation = $('#Designation').val();
                    json_data.DesignationCode = $('#DesignationCode').val();
                    //var Id = $('#Id').val();
                    //var Designation = $('#Designation').val();

                    $.ajax({
                        url: url_path + '/AddDesignation',
                        type: 'POST',
                        data: json_data,
                        dataType: "json",                     
                        success: function (result) {
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Add Designation', text: 'Designation added successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewDesignation').modal('show');     
                                $('#designationTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path + '/listdesignation' });

                                $("#btnDesignation").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Designation', text: 'Something went wrong: </br>' + "The Designation Code already exist", type: 'error' }).then(function () { clear(); });
                                $("#btnDesignation").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Designation', text: 'Adding designation encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnDesignation").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Designation', 'You cancelled add designation.', 'error');
            $("#btnDesignation").removeAttr("disabled");
        });

}
function reloadpage() {
    location.reload();
}

function clear() {
    $('#Id').val('');
    $('#Designation').val('');
}

function updateDesignation() {
    debugger
   
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmdesignation').validate({
        messages: {
            Designation: { required: "Designation is required" },

        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Designation will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnDesignationUpdate").attr("disabled", "disabled");
                    debugger
                   var id = $('#Id').val();
                    var Designation = $('#Designation').val();
                    $.ajax({
                        url: url_path + '/UpdateDesignation',
                        type: 'POST',
                        data: { id, Designation},
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                           
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Designation', text: 'Designation updated successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#AddNewDesignation').modal('show');     
                                $('#designationTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path + '/listdesignation' });

                                $("#btnDesignationUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Designation', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnDesignationUpdate").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Designation', text: 'Update designation encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnDesignationUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Designation', 'You cancelled designation update.', 'error');
            $("#btnDesignationUpdate").removeAttr("disabled");
        });

}
