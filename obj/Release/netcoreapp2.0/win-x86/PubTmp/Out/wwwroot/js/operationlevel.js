﻿
function operationFormatter(value, row, index) {
    return [
        "<div class='dropright'>",
        "<button class='btn btn-sm btn-warning' ",
        "data-toggle='dropdown' type='button'>",
        "<i class='now-ui-icons ui-2_settings-90'></i>",
        "</button>",
        "<div class='dropdown-menu dropdown-menu-right'>",
        "<h6 class='dropdown-header'>Update</h6>",
        "<a class='edit dropdown-item'>",
        "Operation Level",
        "</a>",
        "<a class='edit2 dropdown-item' id='" + row.customerid + "' ",
        " onclick='handleAddressUpdate(this)'>",
        "Module Operation",
        "</a>", 
        "</div>",
        "</div>"
    ].join("");
    //return [
    //    '<div class="btn-group">' +
    //    '<a style="color:white"  class="edit btn btn-sm btn-warning"  title="Edit Operation">'
    //    + '<i class="now-ui-icons ui-2_settings-90"></i>' +
    //    '<a style="color:white"  title="Remove Operation" class="remove btn btn-sm btn-danger">'
    //    + '<i class="now-ui-icons ui-1_simple-remove"></i></a>' +
    //    '</a> '
    //].join('');
}
function moduleFormatter(value, row, index) {
return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edit2 btn btn-sm btn-warning"  title="Edit Module">'
        + '<i class="now-ui-icons ui-2_settings-90"></i>' +
        '<a style="color:white"  title="Remove Operation" class="remove btn btn-sm btn-danger">'
        + '<i class="now-ui-icons ui-1_simple-remove"></i></a>' +
        '</a> '
    ].join('');
}
window.moduleEvents = {
    
    'click .edit2': function (e, value, row, index) {
        debugger
        if (row.state = true) {
            var form = $("#frmmodule");
            var data = JSON.stringify(row);
            $('#ModuleIds').val(row.moduleId);
            $('#ModuleOperations').val(row.moduleOperation);
            $('#OperationIds').val(row.operationId);              
            $("#ddlOperation").val(row.operations).trigger('change');
            $('#UpdateModuleLevel').modal('show');
            $('#btnModuleUpdate').show();

        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);

        debugger
        $('#ID').val(row.moduleId);
        $.ajax({
            url: 'ApprovalSetup/RemoveOperationLevel',
            type: 'POST',
            data: { ID: row.moduleId },
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {



                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#currencyTable').
                            bootstrapTable(
                                'refresh', { url: 'ApprovalSetup/listOperationLevel' });

                        //return false;
                    }
                    else {
                        swal("Operation Level", "You cancelled add operation level.", "error");
                    }
                    $('#operationTable').
                        bootstrapTable(
                            'refresh', { url: 'ApprovalSetup/listOperationLevel' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};
$(document).ready(function ($) {
    $('#btnModuleUpdate').on('click', function () {
        updateModule();
    });
});


window.operationEvents = {
    'click .edit': function (e, value, row, index) {
        debugger
        if (row.state = true) {
            var form = $("#frmoperation");           
            var data = JSON.stringify(row);
            $('#OperationId').val(row.operationId);
            $("#ddlOperation").val(row.operations).trigger('change');           
            $('#ApprovalLevels').val(row.approvalLevels);
            $('#SetupStatus').val(row.setupStatus);
            $('#AddNewOperation').modal('show');           
            $('#btnOperationUpdate').show();
            $('#btnOperation').hide();

        }
    },
    'click .edit2': function (e, value, row, index) {
        debugger
        if (row.state = true) {
            var form = $("#frmmodule");
            var data = JSON.stringify(row);
            var TblOperationsLevel = {
                TblModule: {
                    ModuleId: $('#ModuleIds').val(row.moduleId),
                    ModuleOperation: $('#ModuleOperations').val(row.moduleOperation),
                    OperationsId: $('#OperationIds').val(row.operationId)
                },
            
                Operations: $("#ddlOperation").val(row.operations).trigger('change'),
               
            }
            $('#UpdateModuleLevel').modal('show');
            $('#btnModuleUpdate').show();     

        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);
       
        debugger
        $('#ID').val(row.moduleId);
        $.ajax({
            url: 'ApprovalSetup/RemoveOperationLevel',
            type: 'POST',
            data: { ID: row.moduleId },
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {



                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#currencyTable').
                            bootstrapTable(
                            'refresh', { url: 'ApprovalSetup/listOperationLevel' });

                        //return false;
                    }
                    else {
                        swal("Operation Level", "You cancelled add operation level.", "error");
                    }
                    $('#operationTable').
                        bootstrapTable(
                        'refresh', { url: 'ApprovalSetup/listOperationLevel' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};


$(document).ready(function ($) {
    debugger
    //for dropdown list control for Comapny (id=ddlCompany) starts here
    $("#Operations").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "/company/ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#Operations").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",  
            width: '100%',
            data: response.results
        });
    });
});
$(document).ready(function ($) {
    debugger
    //for dropdown list control for Comapny (id=ddlCompany) starts here
    $("#Operation").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "/company/ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#Operation").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",
            width: '100%',
            data: response.results
        });
    });
});
function updateModule() {
    debugger
    var TblOperationsLevel = {        
        ModuleId: $('#ModuleIds').val(),
            ModuleOperation: $('#ModuleOperations').val(),
        OperationId: $('#OperationIds').val()   
    }
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmmodule').validate({
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Module operation will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnModuleUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: 'ApprovalSetup/UpdateModule',
                        type: 'POST',
                        data: TblOperationsLevel,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Module Operation', text: 'Module operation updated successfully!', type: 'success' }).then(function () { clear(); });

                                $('#moduleTable').
                                    bootstrapTable(
                                        'refresh', { url: 'ApprovalSetup/listModule' });
                                //$('#UpdateModuleLevel').modal('hide');
                                $("#btnModuleUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Module Operation', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnModuleUpdate").removeAttr("disabled");
                                $('#UpdateModuleLevel').modal('hide');
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Module Operation', text: 'Module operation update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnModuleUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Module Operation ', 'You cancelled module operation update.', 'error');
            $("#btnModuleUpdate").removeAttr("disabled");
        });

}
function updateOperation() {
    debugger
    $('#AddNewOperation').trigger('reset');
    
    var TblOperationsLevel = {
        TblModule: {
            ModuleId: $('#ModuleId').val(),
            ModuleOperation: $('#ModuleOperation').val()
        },
        OperationId: $('#OperationId').val(),
        Operations: $('#ddlOperation').val(),
        SetupStatus: $('#SetupStatus').val(),
        ApprovalLevels: $('#ApprovalLevels').val()
    }
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmoperation').validate({       
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Operation will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnOperationUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: 'ApprovalSetup/UpdateOperationLevel',
                        type: 'POST',
                        data: TblOperationsLevel,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Operation Level', text: 'Operation level updated successfully!', type: 'success' }).then(function () { clear(); });

                                $('#operationTable').
                                    bootstrapTable(
                                    'refresh', { url: 'ApprovalSetup/listOperationLevel' });
                                $('#AddNewOperation').modal('hide');
                                $("#btnOperationUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Operation', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnOperationUpdate").removeAttr("disabled");
                                $('#AddNewOperation').modal('hide');
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Operation', text: 'Operation level update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnOperationUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Operation Level', 'You cancelled operation level update.', 'error');
            $("#btnOperationUpdate").removeAttr("disabled");
        });

}
function clear() {
    $("#AddNewOperation form select").val(null)
        .trigger("select2:unselect")
        .trigger("change");
    $("#AddNewOperation form").trigger("reset");
}
$(document).ready(function ($) {
    $('#btnOperationUpdate').hide();
    $('#btnOperation').show();
    $('#btnOperationUpdate').on('click', function () {
        debugger
        updateOperation();
    })
    
});
$(document).ready(function ($) {
   
    $('#btnOperations').on('click', function () {
        debugger
        addOperation();
    })

});
function addOperation() {
    debugger
    $('#btnOperations').show();
    //$('#AddNewOperation').trigger('reset');     
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmoperations').validate({       
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Operation level will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnOperations").attr("disabled", "disabled");

                    debugger
                    //var json_data = {};
                    //json_data.OperationId = $('#OperationId').val();
                    //json_data.Operations = $('#Operations').val();
                    //json_data.ApprovalLevels = $('#ApprovalLevels').val(); 
                    
                    var TblOperationsLevels = {
                        TblModule: {
                            ModuleId: $('#ModulIds').val(),
                            ModuleOperation: $('#ModulOperations').val(),
                            OperationId: $('#OperatioIds').val()
                        },
                        Operations: $('#OperationIds').val(),
                        Operations: $('#Operation').val(),
                        SetupStatus: $('#SetupStatus').val(),
                        ApprovalLevels: $('#ApprovalLevel').val()
                    }
                    $.ajax({
                        url: 'ApprovalSetup/AddOperationLevel',
                        type: 'POST',
                        data: TblOperationsLevels,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Add Operation Level', text: 'Operation level added successfully!', type: 'success' }).then(function () { clear(); });

                                $('#operationTable').bootstrapTable('refresh', { url: 'ApprovalSetup/listoperationlevel' });
                                $('#moduleTable').bootstrapTable('refresh', { url: 'ApprovalSetup/listModule' });
                                $("#AddNewOperations").modal("hide");
                                $("#btnOperations").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Operation Level', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnOperations").removeAttr("disabled");
                                $('#moduleTable').bootstrapTable('refresh', { url: 'ApprovalSetup/listModule' });
                                $("#AddNewOperations").modal("hide");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Operation Level', text: 'Adding Operation level encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnOperations").removeAttr("disabled");
                            $('#moduleTable').bootstrapTable('refresh', { url: 'ApprovalSetup/listModule' });
                            $("#AddNewOperations").modal("hide");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Operation Level', 'You cancelled add operation level.', 'error');
            $("#btnOperations").removeAttr("disabled");
        });

}