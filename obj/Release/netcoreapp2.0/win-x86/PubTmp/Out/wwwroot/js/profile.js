﻿$(document).ready(function ($) {
    debugger
    //for dropdown list control for Comapny (id=ddlCompany)starts here

    $("#ddlName").select2({
        theme: "bootstrap4",
        placeholder: "Click here to see your profile...",

    });
    $.ajax({
        url: "../CompanySetup/loadProfile",
    }).then(function (response) {
        debugger
        $("#ddlName").select2({
            theme: "bootstrap4",
            // placeholder: "Select Company...",  
            width: '100%',
            data: response.results
        });
    });
    $("#ddlName").on("select2:select", function (e) {
        debugger
        $("#ddlName").val(e.params.data.brId);
        $("#companyId").val(e.params.data.id);
        $("#Username").val(e.params.data.text);        
        $("#dept").val(e.params.data.deptId);
        $("#email").val(e.params.data.email);
        $("#Phone").val(e.params.data.phone);
        $("#Fullname").val(e.params.data.name);
        $("#Address").val(e.params.data.add);
        $("#Country").val(e.params.data.country);
        $("#State").val(e.params.data.state);
       
    });
});

$(document).ready(function ($) {   
    $('#btnProfile').on('click', function () {
        debugger
        addUpload();
    });
});
function addUploads() {
    debugger
    var json_data = {};
    json_data.Id = $('#Id').val();
    json_data.CompanyId = $('#CompanyId').val();
    json_data.CoyName = $('#CoyName').val();
    json_data.Address = $('#Address').val();
    json_data.Phone = $('#Phone').val();
    json_data.Email = $('#email').val();
    json_data.Fullname = $('#Fullname').val();
    json_data.State = $('#State').val();
    json_data.Country = $('#Country').val();
    json_data.Staffpicture = $('#Staffpicture').val();  
    $("input[type=submit]").attr("disabled", "disabled");

    $('#frmprofile').validate({

        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Profile will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnProfile").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: 'CompanySetup/UploadProfile',
                        type: 'POST',
                        data: json_data,
                        contentType: false,
                        processData: false,                       
                        success: function (result) {

                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Profile', text: 'Your profile updated successfully!', type: 'success' }).then(function () { clear(); });
                              
                            }
                            else {
                                swal({ title: 'Update Profile', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnProfile").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Profile', text: 'Your profile encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnProfile").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Profile', 'You cancelled profile update.', 'error');
            $("#btnProfile").removeAttr("disabled");
        });

}


function addUpload() {
    var profileFile = $("#picture").get(0).files;
    debugger;
    if (!profileFile) {
        return;
    }
   
    var Id = $('#Id').val();
    var CompanyId = $('#CompanyId').val();
    var CoyName = $('#CoyName').val();
    var Address = $('#Address').val();
    var Phone = $('#Phone').val();
    var Email = $('#email').val();
    var Fullname = $('#Fullname').val();
    var State = $('#State').val();
    var Country = $('#Country').val();
    var Staffpicture = $('#picture').val();  
    var ProfileImgData = new FormData();
    ProfileImgData.append("CompanyId", $('#CompanyId').val());
    ProfileImgData.append("CoyName", $('#CoyName').val());  
    ProfileImgData.append("Address", $('#Address').val());
    ProfileImgData.append("Phone", $('#Phone').val());  
    ProfileImgData.append("email", $('#email').val());
    ProfileImgData.append("Fullname", $('#Fullname').val());
    ProfileImgData.append("State", $('#State').val());  
    ProfileImgData.append("Country", $('#Country').val());   
    
    for (var ImageCounter = 0; ImageCounter < profileFile.length; ImageCounter++) {
        ProfileImgData.append("profileFile", profileFile[ImageCounter]);
    }

    debugger;
    $.ajax(
        {
            method: "POST",
            url: "../CompanySetup/AddImage",
        contentType: false,
        processData: false,
        data: ProfileImgData,
        success: function (data) {
            swal({ title: 'Profile Update', text: 'Your profile updated successfully!', type: 'success' }).then(function () { clear(); });

        },
        error: function (e) {
            swal({ title: 'Profile Update', text: 'Your profile encountered an error', type: 'error' }).then(function () { clear(); });

        }
    });
}