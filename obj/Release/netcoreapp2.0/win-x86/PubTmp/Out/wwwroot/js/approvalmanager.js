﻿$("#MinAmt").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#MinAmt").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#MinAmt").val(formatnumber(realAmount))
});
$("#MaxAmt").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#MaxAmt").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#MaxAmt").val(formatnumber(realAmount))
});
$("#CummulativeAmt").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#CummulativeAmt").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#CummulativeAmt").val(formatnumber(realAmount))
});
//function clear() {
//    $("#assign form select").val(null)
//        .trigger("select2:unselect")
//        .trigger("change");
//    $("#assign form").trigger("reset");
//}
//Function called 

function formatnumber(value) {
    return parseFloat(value.toString().replace(/,/g, "")).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var $usertable = $('#userAssignTable'), $Branchtable = $('#BranchAssignTable'), $button = $('#btnMapBranch'), $approvaltable = $('#officerTable'), $approvaltablemgt = $('#officerTablemgt'), $approvaltableBoard = $('#officerTableBoard');
function branchFormatter(value, row, index) {
    return [
        '<a style="color:white" title="Remove Branchname" class="remove btn btn-sm btn-danger">'
        + '<i class="fas fa-trash"></i></a>'
    ].join('');
}
function approvedFormatter(value, row, index) {
    return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edits btn btn-sm btn-info"  title="Edit">'
        + '<i class="fas fa-edit"></i>' +
        '<a style="color:white"  title="Remove" class="removes btn btn-sm btn-danger">'
        + '<i class="fas fa-trash></i></a>' +
        '</a> '
    ].join('');
}

$(document).ready(function ($) {
    $("#MaxAmt").mouseout(function (event) {
        debugger
        var minAmt = $('#MinAmt').val();
        var maxAmt = $('#MaxAmt').val();
        if (maxAmt <= minAmt) {
            $.notify({ icon: "add_alert", message: 'Minimum amount can not be greater or equal to maximum amount!' }, { type: 'danger', timer: 1000 });
            $(this).css('border-color', 'red');
            $('#maxAmt').focus();
            return false;
        } else {
            $(this).css('border-color', 'green');
            $('#maxAmt').focus();
        }

    });
});
$(document).ready(function ($) {
    $("#CummulativeAmt").mouseout(function (event) {
        debugger
        var minAmt = $('#MinAmt').val();
        var maxAmt = $('#MaxAmt').val();
        var cummulativeAmt = $('#CummulativeAmt').val();
        if (cummulativeAmt < minAmt || CummulativeAmts < maxAmt) {
            $.notify({ icon: "add_alert", message: 'You have to check cummulative limit against minimum and maximum amount!' }, { type: 'danger', timer: 1000 });
            $(this).css('border-color', 'red');
            $('#CummulativeAmt').focus();
            return false;
        } else {
            $(this).css('border-color', 'green');
            $('#CummulativeAmt').focus();
        }

    });
});
window.BranchEvents = {

    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);
        debugger
        $('#ID').val(row.id);
        $.ajax({
            url: '@Url.Action("RemoveBranch")',
            type: 'POST',
            data: { ID: row.id },
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        swal("Deleted succesfully");
                        $('#BranchAssignTable').
                            bootstrapTable(
                                'refresh', { url: 'Administration/ListBranch' });
                        $('#userAssignTable').bootstrapTable(
                            'refresh', { url: 'Administration/ListUser' });
                        $('#BranchTable').bootstrapTable(
                            'refresh', { url: 'Administration/ListUsersBranchs' });

                    }
                    else {
                        swal("Branchname", "You cancelled add Branchname.", "error");
                    }
                    $('#BranchAssignTable').
                        bootstrapTable(
                            'refresh', { url: 'Administration/ListBranch' });
                    $('#userAssignTable').bootstrapTable(
                        'refresh', { url: 'Administration/ListUser' });
                    $('#BranchTable').bootstrapTable(
                        'refresh', { url: 'Administration/ListUsersBranchs' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};
window.approvedEvents = {
    'click .edits': function (e, value, row, index) {
        if (row.state = true) {
            var data = JSON.stringify(row);
            $('#Id').val(row.id);
            $('#ddlOperations').val(row.operationID).trigger('change');
            $('#ddldesignation').val(row.designation).trigger('change');
            $('#MinAmts').val(row.minAmt);
            $('#MaxAmts').val(row.maxAmt);
            $('#CummulativeAmts').val(row.cummulativeAmt);
            $('#ApprovingLevels').val(row.approvingLevel);
            $('#Designations').modal('show');
            $('#btnApprovalCommitteeUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            $('#btnApprovalCommitteeUpdate').show();
            //$('#btnCurrency').hide();
        }
    },
    'click .removes': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);
        debugger
        $('#ID').val(row.id);
        $.ajax({
            url: 'ApprovalSetup/RemoveApprovedStaff',
            type: 'POST',
            data: { ID: row.id },
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#approvedStaffTable').
                            bootstrapTable(
                                'refresh', { url: 'ApprovalSetup/listapprovedstaff' });
                        //return false;
                    }
                    else {
                        swal("Approved Staff", "You cancelled add bank.", "error");
                    }
                    $('#approvedStaffTable').
                        bootstrapTable(
                            'refresh', { url: 'ApprovalSetup/listapprovedstaff' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};
$(document).ready(function ($) {
    $('#btnApprovalCommittee').on('click', function () {
        addApproval();
      
    });

});
$(document).ready(function ($) {    
    $('#btnApprovalCommitteeUpdate').on('click', function () {
        debugger
        updateApproval();
    });

});
function addApproval() {
    debugger
    //$('#btnApprovalCommittee').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmApproval').validate({
        messages: {
            minAmt: { required: "Minimum Amount is required" },
            maxAmt: { required: "Maximum Amount is required" },
            CummulativeAmt: { required: "Cummulative limit is required" },
            ApprovingLevel: { required: "Approving level is required" }
           
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Approving officer will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnApprovalCommittee").attr("disabled", "disabled");
                    debugger
                    var staffData = $approvaltable.bootstrapTable('getAllSelections');
                    var reg_datas = {
                        OperationID: $('#ddlOperationss').val(),
                        Designation: $('#ddlDesignations').val(),
                        MinAmt: $('#MinAmt').val(),
                        MaxAmt: $('#MaxAmt').val(),
                        CummulativeAmt: $('#CummulativeAmt').val(),
                        ApprovingLevel: $('#ApprovingLevel').val()
                    }
                    $.each(staffData, function (index, staffItemData) {
                        $.ajax({
                            url: 'ApprovalSetup/AddApprovalCommittee',
                            type: 'POST',
                            data: {
                                StaffId: staffItemData.staffName, CompanyCode: staffItemData.CompanyId, BranchCode: staffItemData.branchId,
                                OperationID: $('#ddlOperationss').val(), Designation: $('#ddlDesignations').val(), MinAmt: $('#MinAmt').val(),
                                MaxAmt: $('#MaxAmt').val(), CummulativeAmt: $('#CummulativeAmt').val(), ApprovingLevel: $('#ApprovingLevel').val()
                            },
                            dataType: "json",
                            success: function (result) {
                                debugger
                                if (result.toString != '' && result != null && result.message =='' ) {
                                    swal({ title: 'Add Approving Officer', text: 'Approving officer added successfully!', type: 'success' }).then(function () { clear(); });

                                    $('#approvedStaffTable').
                                        bootstrapTable(
                                        'refresh', { url: 'ApprovalSetup/listapprovedstaff' });
                                    $('#Designation').modal('hide').data('bs.modal', null);
                                    $("#btnApprovalCommittee").removeAttr("disabled");
                                }
                                else {
                                    //alert(result.message);
                                    swal({ title: 'Add Approving Officer', text: 'Something went wrong: ' + result.message, type: 'error' }).then(function () { clear(); });
                                    $("#btnApprovalCommittee").removeAttr("disabled");
                                }
                            },
                            error: function (e) {
                                swal({ title: 'Add Approving Officer', text: 'Adding Approving officer encountered an error', type: 'error' }).then(function () { clear(); });
                                $("#btnApprovalCommittee").removeAttr("disabled");
                            }
                        });
                    })
                }
            
             });
        }

    },
        function (dismiss) {
            swal('Add Approving Officer', 'You cancelled add approving officer.', 'error');
            $("#btnApprovalCommittee").removeAttr("disabled");
        });

}
function updateApproval() {
    debugger
    //$('#btnApprovalCommittee').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmApprovals').validate({       
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Approving officer will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnApprovalCommitteeUpdate").attr("disabled", "disabled");
                    debugger
                    //var staffData = $approvaltable.bootstrapTable('getAllSelections');
               
                    
                        $.ajax({
                            url: 'ApprovalSetup/UpdateApprovedStaff',
                            type: 'POST',
                            data: {
                                Id: $('#Id').val(),MinAmt: $('#MinAmts').val(), 
                                MaxAmt: $('#MaxAmts').val(), CummulativeAmt: $('#CummulativeAmts').val(), ApprovingLevel: $('#ApprovingLevels').val()
                            },
                            dataType: "json",
                            success: function (result) {

                                if (result.toString != '' && result != null) {
                                    swal({ title: 'Update Approving Officer', text: 'Approving officer updated successfully!', type: 'success' }).then(function () { clear(); });

                                    $('#approvedStaffTable').
                                        bootstrapTable(
                                            'refresh', { url: 'ApprovalSetup/listapprovedstaff' });
                                    $('#Designations').modal('hide').data('bs.modal', null);
                                    $("#btnApprovalCommitteeUpdate").removeAttr("disabled");
                                }
                                else {
                                    swal({ title: 'Update Approving Officer', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                    $("#btnApprovalCommitteeUpdate").removeAttr("disabled");
                                }
                            },
                            error: function (e) {
                                swal({ title: 'Update Approving Officer', text: 'Update Approving officer encountered an error', type: 'error' }).then(function () { clear(); });
                                $("#btnApprovalCommitteeUpdate").removeAttr("disabled");
                            }
                        });
                  
                }

            });
        }

    },
        function (dismiss) {
            swal('Add Approving Officer', 'You cancelled add approving officer.', 'error');
            $("#btnApprovalCommittee").removeAttr("disabled");
        });

}

function addBranch() {

    $("input[type=submit]").attr("disabled", "disabled");

    $('#frmBranch').validate({

        messages: {
            BranchName: { required: "BranchName is required" }
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Branch will be created!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnAddBranch").attr("disabled", "disabled");
                    debugger
                    var json_data = {};
                    json_data.BranchName = $('#BranchName').val();
                    $.ajax({
                        url: 'Administration/ManageBranch',
                        type: 'POST',
                        data: json_data,
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Create  Branch', text: ' Branch added successfully!', type: 'success' }).then(function () { clear(); });

                                $('#BranchTable').
                                    bootstrapTable(
                                        'refresh', { url: '../Administration/ListBranch' });

                                $("#btnAddBranch").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Create  Branch', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnAddBranch").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Create  Branch', text: 'Create  Branch encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnAddBranch").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Create  Branch', 'You cancelled  Branch creation.', 'error');
            $("#btnAddBranch").removeAttr("disabled");
        });

}

$(document).ready(function ($) {
    debugger
    $("#ddlOperationss").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#ddlOperationss").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",  
            width: '100%',
            data: response.results
        });
        $("#ddlOperationss").on("select2:select", function (e) {
            debugger
            var datas = e.params.data;           
            $("#ddllevels").val(datas.brId);
         
        });
    });
});
$(document).ready(function ($) {
    debugger
    $("#ddlDesignations").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadDesignation",
    }).then(function (response) {
        debugger
        $("#ddlDesignations").select2({
            theme: "bootstrap4",
            placeholder: "Select Designation...",
            width: '100%',
            data: response.results
        });
        });

    $('#ddlDesignations').on('change', function () {
        debugger
       
        $.ajax({
            url: "ApprovalSetup/ListOfficer",
            data: { Designation: $('#ddlDesignations').val() },
            type: "POST",
            datatype: "application/json",
            success: function (response) {

                console.log(response);
                debugger;

                var $table = $('#officerTable');
                $table.bootstrapTable("destroy");

                $table.bootstrapTable({
                    data: response,

                    columns: [
                        {
                            field: 'state',
                            checkbox: true

                        }, {
                            field: 'staffNo',
                            title: 'Staff No'
                        }
                        , {
                            field: 'staffName',
                            title: 'Staff Name'
                        }
                        //, {
                        //    field: 'designationCode',
                        //    title: 'Designation '
                        //}
                    ]
                });

            },
        })
           
    });

});

function approvaltable() {
    $.ajax({
        url: 'ApprovalSetup/listapprovedstaff',
        type: 'POST',
        success: function (response) {
            var $table = $('#approvedStaffTable');
            $table.bootstrapTable("destroy");
            $table.bootstrapTable({
                url: 'ApprovalSetup/listapprovedstaff',
                data: response,

                columns: [
                    {
                        field: 'state',
                        checkbox: true

                    }, {
                        field: 'approvingAuthority',
                        title: 'Authority'
                    }
                    , {
                        field: 'staffId',
                        title: 'Staff Name'
                    }
                    , {
                        field: 'approvingLevel',
                        title: 'Level '
                    }
                    
                    , {
                        field: 'minAmt',
                        title: 'Min.Amt'
                    }
                    , {
                        field: 'maxAmt',
                        title: 'Max.Amt'
                    },
                    //} ,{
                    //    field: 'branchCode',
                    //    title: 'Branch'
                    //},
                    {
                        field: '',
                        title: '',
                        events: 'approvedEvents',
                        formatter: 'approvedFormatter'
                    }

                ]
            });
        }
    });

    
    
   
}


$(document).ready(function ($) {
    debugger
    approvaltable();
});

$('#btnOthers, #btnmanagement, #btnboard').on('click',function () {
    if (this.id == 'btnOthers') {
        $('#collapseOther').collapse('show');
        $('#collapseManagement').collapse('hide');
        $('#collapseBoard').collapse('hide');
    }
    else if (this.id == 'btnmanagement') {
        $('#collapseOther').collapse('hide');
        $('#collapseManagement').collapse('show');
        $('#collapseBoard').collapse('hide');
    }
    else {
        $('#collapseOther').collapse('hide');
        $('#collapseManagement').collapse('hide');
        $('#collapseBoard').collapse('show');
    }
});

function initFormValidations() {
    // defaults
    jQuery.validator.setDefaults({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        normalizer: function (value) {
            // Trim the value of every element
            // before validation
            return $.trim(value);
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text()
            }, {
                    type: "danger",
                    placement: {
                        from: "top",
                        align: "right"
                    }
                });
        }
    });

    $("#frmAddressUpdate").validate({
        rules: {
            address: {
                maxlength: 200
            },
            city: {
                maxlength: 50
            }
        },
        messages: {
            address: {
                required: "Address is required",
                maxlength: jQuery.validator.format("Address cannot exceed {0} characters")
            },
            addresstypeid: {
                required: "Address type is required"
            },
            city: {
                required: "City is required",
                maxlength: jQuery.validator.format("City cannot exceed {0} characters")
            },
            stateid: { required: "State is required" },
            countryid: { required: "Country is required" }
        }
    });
    $("#frmPhoneUpdate").validate({
        rules: {
            phone: {
                maxlength: 50,
                digits: true
            }
        },
        messages: {
            phone: {
                required: "Phone number is required",
                maxlength: jQuery.validator.format("Phone number cannot exceed {0} characters"),
                digits: "Phone number can only contain digits",
            }
        }
    });
    $("#frmEmailUpdate").validate({
        rules: {
            email: {
                maxlength: 200,
                email: true
            }
        },
        messages: {
            required: "E-mail is required",
            email: "E-mail format is invalid",
            maxlength: jQuery.validator.format("E-mail cannot exceed {0} characters")
        }
    });
}
//Management
$(document).ready(function ($) {
    debugger
    $("#ddlOperationmgt").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#ddlOperationmgt").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",
            width: '100%',
            data: response.results
        });
        $("#ddlOperationmgt").on("select2:select", function (e) {
            debugger
            var datas = e.params.data;
            $("#ddllevelsmgt").val(datas.brId);

        });
    });
});
$(document).ready(function ($) {
    debugger
    $("#ddlDesignationmgt").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadDesignation",
    }).then(function (response) {
        debugger
        $("#ddlDesignationmgt").select2({
            theme: "bootstrap4",
            placeholder: "Select Designation...",
            width: '100%',
            data: response.results
        });
    });

    $('#ddlDesignationmgt').on('change', function () {
        debugger

        $.ajax({
            url: "ApprovalSetup/ListOfficer",
            data: { Designation: $('#ddlDesignationmgt').val() },
            type: "POST",
            datatype: "application/json",
            success: function (response) {

                //console.log(response);
                debugger;

                var $table = $('#officerTablemgt');
                $table.bootstrapTable("destroy");

                $table.bootstrapTable({
                    data: response,

                    columns: [
                        {
                            field: 'state',
                            checkbox: true

                        }, {
                            field: 'staffNo',
                            title: 'Staff No'
                        }
                        , {
                            field: 'staffName',
                            title: 'Staff Name'
                        }
                        //, {
                        //    field: 'designationCode',
                        //    title: 'Designation '
                        //}
                    ]
                });

            },
        })

    });

});
$("#MinAmtmgt").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#MinAmtmgt").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#MinAmtmgt").val(formatnumber(realAmount))
});
$("#MaxAmtmgt").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#MaxAmtmgt").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#MaxAmtmgt").val(formatnumber(realAmount))
});
$("#CummulativeAmtmgt").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#CummulativeAmtmgt").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#CummulativeAmtmgt").val(formatnumber(realAmount))
});
$(document).ready(function ($) {
    $('#btnApprovalCommitteemgt').on('click', function () {
        debugger
        addApprovalmgt();
    });
});
function addApprovalmgt() {
    debugger
    //$('#btnApprovalCommittee').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmApprovalMgt').validate({
        messages: {
            minAmt: { required: "Minimum Amount is required" },
            maxAmt: { required: "Maximum Amount is required" },
            CummulativeAmt: { required: "Cummulative limit is required" },
            ApprovingLevel: { required: "Approving level is required" }

        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Approving officer will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnApprovalCommitteemgt").attr("disabled", "disabled");
                    debugger
                    var staffDatas = $approvaltablemgt.bootstrapTable('getAllSelections');
                    var reg_datas = {
                        OperationID: $('#ddlOperationmgt').val(),
                        Designation: $('#ddlDesignationmgt').val(),
                        MinAmt: $('#MinAmtmgt').val(),
                        MaxAmt: $('#MaxAmtmgt').val(),
                        CummulativeAmt: $('#CummulativeAmtmgt').val(),
                        ApprovingLevel: $('#ApprovingLevelmgt').val()
                    }
                    $.each(staffDatas, function (index, staffItemDatas) {
                        $.ajax({
                            url: 'ApprovalSetup/AddApprovalCommitteeMgt',
                            type: 'POST',
                            data: {
                                StaffId: staffItemDatas.staffName, CompanyCode: staffItemDatas.CompanyId, BranchCode: staffItemDatas.branchId,
                                OperationID: $('#ddlOperationmgt').val(), Designation: $('#ddlDesignationmgt').val(), MinAmt: $('#MinAmtmgt').val(),
                                MaxAmt: $('#MaxAmtmgt').val(), CummulativeAmt: $('#CummulativeAmtmgt').val(), ApprovingLevel: $('#ApprovingLevelmgt').val()
                            },
                            dataType: "json",
                            success: function (result) {
                                debugger
                                if (result.toString != '' && result != null && result.message == '') {
                                    swal({ title: 'Add Approving Officer', text: 'Approving officer added successfully!', type: 'success' }).then(function () { clear(); });

                                    $('#approvedStaffTable').
                                        bootstrapTable(
                                            'refresh', { url: 'ApprovalSetup/listapprovedstaff' });
                                    $('#Designation').modal('hide').data('bs.modal', null);
                                    $("#btnApprovalCommitteemgt").removeAttr("disabled");
                                }
                                else {
                                    //alert(result.message);
                                    swal({ title: 'Add Approving Officer', text: 'Something went wrong: ' + result.message, type: 'error' }).then(function () { clear(); });
                                    $("#btnApprovalCommitteemgt").removeAttr("disabled");
                                }
                            },
                            error: function (e) {
                                swal({ title: 'Add Approving Officer', text: 'Adding Approving officer encountered an error', type: 'error' }).then(function () { clear(); });
                                $("#btnApprovalCommitteemgt").removeAttr("disabled");
                            }
                        });
                    })
                }

            });
        }

    },
        function (dismiss) {
            swal('Add Approving Officer', 'You cancelled add approving officer.', 'error');
            $("#btnApprovalCommittee").removeAttr("disabled");
        });

}
//Board
$(document).ready(function ($) {
    debugger
    $("#ddlOperationBoard").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#ddlOperationBoard").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",
            width: '100%',
            data: response.results
        });
        $("#ddlOperationBoard").on("select2:select", function (e) {
            debugger
            var datas = e.params.data;
            $("#ddllevelsBoard").val(datas.brId);

        });
    });
});
$(document).ready(function ($) {
    debugger
    $("#ddlDesignationBoard").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadDesignation",
    }).then(function (response) {
        debugger
        $("#ddlDesignationBoard").select2({
            theme: "bootstrap4",
            placeholder: "Select Designation...",
            width: '100%',
            data: response.results
        });
    });

    $('#ddlDesignationBoard').on('change', function () {
        debugger

        $.ajax({
            url: "ApprovalSetup/ListOfficer",
            data: { Designation: $('#ddlDesignationBoard').val() },
            type: "POST",
            datatype: "application/json",
            success: function (response) {

                //console.log(response);
                debugger;

                var $table = $('#officerTableBoard');
                $table.bootstrapTable("destroy");

                $table.bootstrapTable({
                    data: response,

                    columns: [
                        {
                            field: 'state',
                            checkbox: true

                        }, {
                            field: 'staffNo',
                            title: 'Staff No'
                        }
                        , {
                            field: 'staffName',
                            title: 'Staff Name'
                        }
                        //, {
                        //    field: 'designationCode',
                        //    title: 'Designation '
                        //}
                    ]
                });

            },
        })

    });

});
$("#MinAmtBoard").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#MinAmtBoard").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#MinAmtBoard").val(formatnumber(realAmount))
});
$("#MaxAmtBoard").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#MaxAmtBoard").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#MaxAmtBoard").val(formatnumber(realAmount))
});
$("#CummulativeAmtBoard").blur(function () {
    //var amtValue = $.trim(("#Amount").val);
    var amtValue = $.trim($("#CummulativeAmtBoard").val());
    var lastvalueofAmtString = amtValue.substr(amtValue.length - 1);
    var realAmount;

    switch (lastvalueofAmtString.toLowerCase()) {
        case 't':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000;
            break;
        case 'm':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000;
            break;
        case 'b':
            realAmount = $.trim(amtValue.replace(lastvalueofAmtString, '')) * 1000000000;
            break;
        default:
            0
    }


    // alert(formatnumber(realAmount));
    $("#CummulativeAmtBoard").val(formatnumber(realAmount))
});
$(document).ready(function ($) {
    $('#btnApprovalCommitteeBoard').on('click', function () {
        debugger
        addApprovalBoard();
    });
});
function addApprovalBoard() {
    debugger
    //$('#btnApprovalCommittee').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmApprovalBoard').validate({
        messages: {
            minAmt: { required: "Minimum Amount is required" },
            maxAmt: { required: "Maximum Amount is required" },
            CummulativeAmt: { required: "Cummulative limit is required" },
            ApprovingLevel: { required: "Approving level is required" }

        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Approving officer will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnApprovalCommitteeBoard").attr("disabled", "disabled");
                    debugger
                    var staffDatass = $approvaltableBoard.bootstrapTable('getAllSelections');
                   
                    $.each(staffDatass, function (index, staffItemDatass) {
                        $.ajax({
                            url: 'ApprovalSetup/AddApprovalCommitteeBoard',
                            type: 'POST',
                            data: {
                                StaffId: staffItemDatass.staffName, CompanyCode: staffItemDatass.CompanyId, BranchCode: staffItemDatass.branchId,
                                OperationID: $('#ddlOperationBoard').val(), Designation: $('#ddlDesignationBoard').val(), MinAmt: $('#MinAmtBoard').val(),
                                MaxAmt: $('#MaxAmtBoard').val(), CummulativeAmt: $('#CummulativeAmtBoard').val(), ApprovingLevel: $('#ApprovingLevelBoard').val()
                            },
                            dataType: "json",
                            success: function (result) {
                                debugger
                                if (result.toString != '' && result != null && result.message == '') {
                                    swal({ title: 'Add Approving Officer', text: 'Approving officer added successfully!', type: 'success' }).then(function () { clear(); });

                                    $('#approvedStaffTable').
                                        bootstrapTable(
                                            'refresh', { url: 'ApprovalSetup/listapprovedstaff' });
                                    $('#Designation').modal('hide').data('bs.modal', null);
                                    $("#btnApprovalCommitteeBoard").removeAttr("disabled");
                                }
                                else {
                                    //alert(result.message);
                                    swal({ title: 'Add Approving Officer', text: 'Something went wrong: ' + result.message, type: 'error' }).then(function () { clear(); });
                                    $("#btnApprovalCommitteeBoard").removeAttr("disabled");
                                }
                            },
                            error: function (e) {
                                swal({ title: 'Add Approving Officer', text: 'Adding Approving officer encountered an error', type: 'error' }).then(function () { clear(); });
                                $("#btnApprovalCommitteeBoard").removeAttr("disabled");
                            }
                        });
                    })
                }

            });
        }

    },
        function (dismiss) {
            swal('Add Approving Officer', 'You cancelled add approving officer.', 'error');
            $("#btnApprovalCommitteeBoard").removeAttr("disabled");
        });

}
//Approval Mapping
$(function () {
    $button.click(function () {
        debugger
        var userData = $usertable.bootstrapTable('getAllSelections');
        var branchData = $Branchtable.bootstrapTable('getAllSelections');
        $.each(userData, function (index, userItemData) {
            debugger
            $.each(branchData, function (index, branchItemData) {
                debugger
                $.ajax({
                    url: 'ApprovalSetup/AddUserBranch',
                    type: 'POST',
                    data: { BrName: branchItemData.brName, Username: userItemData.userName },
                    success: function (data) {
                        swal({
                            title: "Are you sure?",
                            text: "You are about to map the user to branch!",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#ff9800",
                            confirmButtonText: "Yes, proceed",
                            cancelButtonText: "No, cancel!",
                            showLoaderOnConfirm: true,
                            preConfirm: function () {
                                return new Promise(function (resolve) {
                                    setTimeout(function () {
                                        resolve();
                                    }, 4000);
                                });
                            }
                        }).then(function (isConfirm) {
                            if (isConfirm) {



                                swal("Mapped succesfully");
                                //alert('Deleted succesfully');
                                $('#userAssignTable').
                                    bootstrapTable(
                                    'refresh', { url: 'CompanySetup/listProfile' });
                                $('#BranchAssignTable').
                                    bootstrapTable(
                                    'refresh', { url: 'CompanySetup/listbranche' });

                                //return false;
                            }
                            else {
                                swal("Assign Branch", "You cancelled assign branch.", "error");
                            }
                            $('#userAssignTable').
                                bootstrapTable(
                                'refresh', { url: 'CompanySetup/listProfile' });
                        });
                        return

                    },

                    error: function (e) {
                        //alert("An exception occured!");
                        swal("An exception occured!");
                    }
                });

            });

        });


    });
});