﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) === '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}




var groupId;


function currencyFormatter(value, row, index) {
    return [
        '<div class="btn-group">'+
        '<a style="color:white"  class="edit btn btn-sm btn-info"  title="Edit Currency">'
        + '<i class="fas fa-edit"></i>' +
        '<a style="color:white"  title="Remove currency" class="remove btn btn-sm btn-danger">'
        +'<i class="fas fa-trash"></i></a>'+
        '</a> '
    ].join('');
}

window.currencyEvents = {
    'click .edit': function (e, value, row, index) {
       
        if (row.state = true) {
          
            var data = JSON.stringify(row);
            $('#Id1').val(row.id);
            $('#CurrCode1').val(row.currCode);
            $('#CurrName1').val(row.currName);
            $('#CurrSymbol1').val(row.currSymbol);
            $('#ExchangeRate1').val(row.exchangeRate);
            $('#CountryCode1').val(row.countryCode);
            $('#AverageRate1').val(row.averageRate);           
            $('#UpdateNewCurrency').modal('show');
            $('#btnCurrencyUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            //$('#btnCurrencyUpdate').show();
            //$('#btnCurrency').hide();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);
        var reg = {
           Id: $('#id').val(),
            Code:$('#CurrCode').val(),
            Name:$('#CurrName').val(),
            Symbol:$('#CurrSymbol').val(),
            Rate:$('#ExchangeRate').val(),
            Country: $('#CountryCode').val(),
           AverageRate: $('#AverageRate').val()
        }
        debugger
        $('#ID').val(row.id);
        $.ajax({
            url: '../CompanySetup/RemoveCurrency',
            type: 'POST',
            data: { ID: row.id},
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {                    
                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#currencyTable').
                            bootstrapTable(
                                'refresh', { url: '../CompanySetup/listcurrency' });

                        //return false;
                    }
                    else {
                        swal("Currency", "You cancelled add bank.", "error");
                    }
                     $('#currencyTable').
                            bootstrapTable(
                                'refresh', { url: '../CompanySetup/listcurrency' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};

function updateCurrency() {
    debugger
   
    var CurrencyViewModel = {};
    CurrencyViewModel.Id = $('#Id1').val();
    CurrencyViewModel.CurrCode = $('#CurrCode1').val();
    CurrencyViewModel.CurrName = $('#CurrName1').val();
    CurrencyViewModel.CurrSymbol = $('#CurrSymbol1').val();
    CurrencyViewModel.ExchangeRate = $('#ExchangeRate1').val();
    CurrencyViewModel.CountryCode = $('#CountryCode1').val();
    CurrencyViewModel.AverageRate = $('#AverageRate1').val();
    //CurrencyViewModel.Id = $('#Id').val();
    //CurrencyViewModel.CurrCode = $('#CurrCode').val();
    //CurrencyViewModel.CurrName = $('#CurrName').val();
    //CurrencyViewModel.CurrSymbol = $('#CurrSymbol').val();
    //CurrencyViewModel.ExchangeRate = $('#ExchangeRate').val();
    //CurrencyViewModel.CountryCode = $('#CountryCode').val();
    //CurrencyViewModel.AverageRate = $('#AverageRate').val();



    $("input[type=submit]").attr("disabled", "disabled");  

    $('#frmCurrencyUpdate').validate({

        messages: {
            CurrCode1: { required: "Currency Code is required" },
            CurrName1: { required: "Currency Name is required" },
            CurrSymbol1: { required: "Currency Symbol is required" },
            ExchangeRate1: { required: "Exchange Rate is required" },
            CountryCode1: { required: "Country Code is required" },
            AverageRate1: { required: "Average Rate is required" }
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Currency will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnCurrencyUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: '../CompanySetup/EditCurrency',
                        type: 'POST',
                        data: CurrencyViewModel,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                            
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Currency', text: 'Currency updated successfully!', type: 'success' }).then(function () { window.location.reload(true); });
					$('#AddNewCurrency').modal('hide');

                                $('#currencyTable').
                                    bootstrapTable(
                                        'refresh', { url: '../CompanySetup/listcurrency' });

                                $("#btnCurrencyUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Currency', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnCurrencyUpdate").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Currency', text: 'Currency update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnCurrencyUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Currency', 'You cancelled currency update.', 'error');
            $("#btnCurrencyUpdate").removeAttr("disabled");
        });

}



$(document).ready(function ($) {
    //$('#btnCurrencyUpdate').hide();
    //$('#btnCurrency').show();
    $('#btnCurrency').on('click', function () {
        debugger      
       
            addCurrency();   
 
    });

});
function addCurrency() {
    debugger
    $('#btnCurrencyUpdate').hide();
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmCurrency').validate({
        messages: {
            CurrCode: { required: "Currency Code is required" },
            CurrName: { required: "Currency Name is required" },
            CurrSymbol: { required: "Currency Symbol is required" },
            ExchangeRate: { required: "Exchange Rate is required" },
            CountryCode: { required: "Country Code is required" },
            AverageRate: { required: "Average Rate is required" }
        },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Currency will be Added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnCurrency").attr("disabled", "disabled");

                    debugger
                    var json_data = {};
                    json_data.Id = $('#Id').val();
                    json_data.CurrCode = $('#CurrCode').val();
                    json_data.CurrName = $('#CurrName').val();
                    json_data.CurrSymbol = $('#CurrSymbol').val();
                    json_data.ExchangeRate = $('#ExchangeRate').val();
                    json_data.CountryCode = $('#CountryCode').val();
                    json_data.AverageRate = $('#AverageRate').val();
                    $.ajax({
                        url: '../CompanySetup/AddCurrency',
                        type: 'POST',
                        data: json_data,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {
                            
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Add Currency', text: 'Currency added successfully!', type: 'success' }).then(function () { window.location.reload(true); });				

                                $('#currencyTable').
                                    bootstrapTable(
                                        'refresh', { url: '../CompanySetup/listcurrency' });

                                $("#btnCurrency").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Currency', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnCurrency").removeAttr("disabled");
                            }
				$('#AddNewCurrency').modal('hide');
                        },
                        error: function (e) {
                            swal({ title: 'Add Currency', text: 'Adding currency encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnCurrency").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Currency', 'You cancelled add currency.', 'error');
            $("#btnCurrency").removeAttr("disabled");
        });

}

$(document).ready(function ($) {
    //$('#btnCurrencyUpdate').hide();
    //$('#btnCurrency').show();
    $('#btnCurrencysUpdate').on('click', function () {
        debugger
        updateCurrency();
    });
});

function reloadpage() {
    location.reload();
}

function clear() {
    $('#CurrCode').val('');
    $('#CurrName').val('');
    $('#CurrSymbol').val('');
    $('#ExchangeRate').val('');
    $('#CountryCode').val('');
    $('#AverageRate').val('');
}

$('#currencyTable').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<h8>' +
        '<p style="text-align:left">' +
   
        '<strong> Currency Code:</strong> &nbsp' + row.currCode + '' + '<p>' +
        '<strong> Currency Name:</strong> &nbsp' + row.currName + '' + '<p>' +
        '<strong> Currency Symbol:</strong> &nbsp' + row.currSymbol + '' + '<p>' +
        '<strong> Exchange Rate:</strong> &nbsp' + row.exchangeRate + '' + '<p>' +
        '<strong> Exchange Rate:</strong> &nbsp' + row.countryCode + '' + '<p>' +
        '<strong> Country Code:</strong> &nbsp' + row.averageRate + '';
      
    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});
