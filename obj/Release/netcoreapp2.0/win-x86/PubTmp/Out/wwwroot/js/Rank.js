﻿var url_path = window.location.pathname;
if (url_path.charAt(url_path.length - 1) == '/') {
    url_path = url_path.slice(0, url_path.length - 1);
}

function rankFormatter(value, row, index) {
    return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edit btn btn-sm btn-info "  title="Edit Rank">'
        + '<i class="fas fa-edit"></i>'+'</a>' +
        '<a  style="color:white" title="Remove Rank" class="remove btn btn-sm btn-danger">'
        +'<i class="fas fa-trash"></i></a>'+
        '</a> '
    ].join('');
}

window.rankEvents = {
    'click .view': function (e, value, row, index) {
        info = JSON.stringify(row);

        swal('You click view icon', info);
       console.log(info);
    },
    'click .edit': function (e, value, row, index) {
       
        if (row.state = true) {          
            var data = JSON.stringify(row);        
            $('#Id').val(row.id);
            $('#Ranks').val(row.rank);   
            $('#AddNewRank').modal('show');
            $('#btnRankUpdate').html('  <i class="now-ui-icons ui-1_check"></i> Update Record');
            $('#btnRankUpdate').show();
            $('#btnRank').hide();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);

        debugger
        $('#ID').val(row.Id);
        $.ajax({
            url: url_path + '/RemoveRank',
            type: 'POST',
            data: { ID: row.id},
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {


                      
                        $.notify("Deleted Successfully", {
                            animate: {
                                enter: 'animated flipInY',
                                exit: 'animated flipOutX'
                            }
                        });
                        $('#rankTable').
                            bootstrapTable(
                                'refresh', { url: url_path + '/listrank' });
                        $('#AddNewRank').modal('hide');
                        //return false;
                    }
                    else {
                        swal("Rank", "You cancelled add rank.", "error");
                    }
                    $('#rankTable').
                            bootstrapTable(
                                'refresh', { url: url_path + '/listrank' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};

$(document).ready(function ($) {
    $('#btnRankUpdate').hide();
    $('#btnRank').show();
    $('#btnRankUpdate').on('click', function () {
        debugger
        updateRank();
    });

});
function updateRank() {
    debugger
   
    var json_data = {};
    json_data.Id = $('#Id').val();
    json_data.Rank= $('#Ranks').val();

    $("input[type=submit]").attr("disabled", "disabled");  

    $('#frmRank').validate({

        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Rank will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnRankUpdate").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: url_path + '/UpdateRank',
                        type: 'POST',
                        data: {
                            Id: $('#Id').val(),
                            Rank: $('#Ranks').val()
                        },
                        dataType: "json",
                     
                        success: function (result) {
                            
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Update Rank', text: 'Rank updated successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                       
                                $('#rankTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path + '/listrank' });
                                $('#AddNewRank').modal('hide');
                                $("#btnRankUpdate").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Rank', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnRankUpdate").removeAttr("disabled");

                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Rank', text: 'Rank update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnRankUpdate").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Rank', 'You cancelled rank update.', 'error');
            $("#btnRankUpdate").removeAttr("disabled");
        });

}



$(document).ready(function ($) {

    $('#btnRank').on('click', function () {
        debugger      
       
            addRank();   
 
    });

});
function addRank() {
    debugger
    $('#btnRankUpdate').hide();


    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmRank').validate({
        messages: {
            Rank: { required: "Rank is required" }
                 },
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Rank will be Added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnRank").attr("disabled", "disabled");

                    debugger

                    var json_data = {};
                    json_data.Id = $('#Id').val();
                    json_data.Rank = $('#Ranks').val();                

                    $.ajax({
                        url: url_path + '/AddRank',
                        type: 'POST',
                        data: { Rank : $('#Ranks').val()},
                        dataType: "json",
                  
                        success: function (result) {
                           
                            if (result.toString != '' && result != null) {
                                swal({ title: 'Add Rank', text: 'Rank added successfully!', type: 'success' }).then(function () { window.location.reload(true); });

                                $('#rankTable').
                                    bootstrapTable(
                                        'refresh', { url: url_path + '/listrank' });
                                $('#AddNewRank').modal('hide');
                                $("#btnRank").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Rank', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnRank").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Rank', text: 'Adding rank encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnRank").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Rank', 'You cancelled add rank.', 'error');
            $("#btnRank").removeAttr("disabled");
        });

}

$(document).ready(function ($) {
    debugger
    //for dropdown list control for Comapny (id=ddlCompany)starts here

    $("#ddlCompany").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",

    });

    $.ajax({
        url: url_path + '/loadCompany',
    }).then(function (response) {
        debugger
        $("#ddlCompany").select2({
            theme: "bootstrap4",
            // placeholder: "Select Company...",  
            width: '100%',
            data: response.results
        });
    });
});

function reloadpage() {
    location.reload();
}

function clear() {
    $('#Id').val('');
    $('#MisCode').val('');
    $('#MisName').val('');
    $('#MisTypeId').val('');
    $('#ParentMisCode').val('');
    $('#CompanyCode').val('');
    $('#Deleted').val('');
    $('#DateCreated').val('');
}

$('#misTable').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<h8>' +
        '<p style="text-align:left">' +
        '<strong>MisCode:</strong>&nbsp' + row.misCode + '' + '<p>' +
        ' <strong>MisName: </strong>&nbsp' + row.misName + '' + '<p>' +
        '<strong> Company Code:</strong>&nbsp' + row.companyCode + ' </div>';

    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});



