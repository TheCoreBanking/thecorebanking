﻿
function operationFormatter(value, row, index) {
    return [
        "<div class='dropright'>",
        "<button class='btn btn-sm btn-warning' ",
        "data-toggle='dropdown' type='button'>",
        "<i class='now-ui-icons ui-2_settings-90'></i>",
        "</button>",
        "<div class='dropdown-menu dropdown-menu-right'>",
        "<h6 class='dropdown-header'>Update</h6>",
        "<a class='edit2 dropdown-item'>",
        "Operation Level",
        "</a>",
        "<a class='edit2 dropdown-item' id='" + row.ID + "' ",
        " onclick='handleAddressUpdate(this)'>",
        "Module Operation",
        "</a>",
        "</div>",
        "</div>"
    ].join("");
}
function committeeFormatter(value, row, index) {
return [
        '<div class="btn-group">' +
        '<a style="color:white"  class="edit2 btn btn-sm btn-warning"  title="Edit Committee">'
        + '<i class="now-ui-icons ui-2_settings-90"></i>' +
        '<a style="color:white"  title="Remove Committee" class="remove btn btn-sm btn-danger">'
        + '<i class="now-ui-icons ui-1_simple-remove"></i></a>' +
        '</a> '
    ].join('');
}
window.committeeEvents = {
    
    'click .edit2': function (e, value, row, index) {
        debugger
        if (row.state === true) {
            var data = JSON.stringify(row);
            $('#id').val(row.id);
            $('#Committee').val(row.committee);
            $('#mgtNo').val(row.mgtNo);  
            $('#boardNo').val(row.boardNo);           
            $('#coyCode').val(row.coyCode);
            $('#brCode').val(row.brCode);
            $('#createdby').val(row.createdby);
            $('#createDate').val(row.createDate);
            $('#creditMinMgt').val(row.creditMinMgt);
            $('#creditMinBoard').val(row.creditMinBoard);
            $('#mgtNoApproval').val(row.mgtNoApproval);          
            $('#boardNoApproval').val(row.boardNoApproval); 
            $('#operationId').val(row.operationId); 

            $('#creditMaximumMgt').val(row.creditMaxComteeAmt);
            $('#creditMaximumMgtBcc').val(row.creditMaxBoardAmt);

            $('#UpdateNewCommittee').modal('show');
           //$('#btnCommitteeUpdate').show();
            $('#btnCommittee').show();
        }
    },
    'click .remove': function (e, value, row, index) {
        info = JSON.stringify(row);
        console.log(info);

        debugger
        $('#ID').val(row.moduleId);
        $.ajax({
            url: 'ApprovalSetup/RemoveCommittee',
            type: 'POST',
            data: { ID: row.id },
            success: function (data) {
                swal({
                    title: "Are you sure?",
                    text: "You are about to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#ff9800",
                    confirmButtonText: "Yes, proceed",
                    cancelButtonText: "No, cancel!",
                    showLoaderOnConfirm: true,
                    preConfirm: function () {
                        return new Promise(function (resolve) {
                            setTimeout(function () {
                                resolve();
                            }, 4000);
                        });
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {



                        swal("Deleted succesfully");
                        //alert('Deleted succesfully');
                        $('#committeeTable').
                            bootstrapTable(
                            'refresh', { url: 'ApprovalSetup/listcommittee' });

                        //return false;
                    }
                    else {
                        swal("Operation Level", "You cancelled add operation level.", "error");
                    }
                    $('#committeeTable').
                        bootstrapTable(
                        'refresh', { url: 'ApprovalSetup/listcommittee' });
                });
                return

            },

            error: function (e) {
                //alert("An exception occured!");
                swal("An exception occured!");
            }
        });
    }

};






$(document).ready(function ($) {
    debugger
    //for dropdown list control for Comapny (id=ddlCompany) starts here
    $("#Operations").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "../ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#Operations").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",  
            width: '100%',
            data: response.results
        });
    });
});

function updateCommittee() {
    debugger
 
    var Json_Data = {        
        Id:$('#id').val(),
        Committee:$('#committee').val(),
        BoardNo: $('#boardNo').val(),
        MgtNo: $('#mgtNo').val(),
        CreditMinMgt:$('#creditMinMgt').val(),
        CoyCode:$('#coyCode').val(),
        BrCode: $('#brCode').val(),
        Createdby:$('#createdby').val(),
        CreateDate:$('#createDate').val(),
        CreditMinBoard:$('#creditMinBoard').val(),
        //CreditMinMgt:$('#creditMinMgt').val(),
        MgtNoApproval:$('#mgtNoApproval').val(),
        BoardNoApproval:$('#boardNoApproval').val(),
        OperationId: $('#ddlOperationM').val(),

         CreditMaxComteeAmt: $('#creditMaximumMgt').val(),
         CreditMaxBoardAmt: $('#creditMaximumMgtBcc').val()

    }
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmcommitteeupdate').validate({
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Credit Committee will be updated!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnCommitteeUpdates").attr("disabled", "disabled");
                    debugger
                    $.ajax({
                        url: 'ApprovalSetup/UpdateCommittee',
                        type: 'POST',
                        data: Json_Data,
                        dataType: "json",
                        //headers: {
                        //    'VerificationToken': forgeryId
                        //},
                        success: function (result) {

                            if (result.toString !== '' && result !== null) {
                                swal({ title: 'Update Credit Committee', text: 'Credit committee updated successfully!', type: 'success' }).then(function () { window.location.reload(true); };

                                $('#committeeTable').
                                    bootstrapTable(
                                    'refresh', { url: 'ApprovalSetup/listcommittee' });
                                $('#UpdateNewCommittee').modal('hide');
                                $("#btnCommitteeUpdates").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Update Credit Committee', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $("#btnCommitteeUpdates").removeAttr("disabled");
                                $('#UpdateNewCommittee').modal('hide');
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Update Credit Committee', text: 'Credit committee update encountered an error', type: 'error' }).then(function () { clear(); });
                            $("#btnCommitteeUpdates").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Update Credit Committee ', 'You cancelled Credit committee update.', 'error');
            $("#btnModuleUpdate").removeAttr("disabled");
        });

}

function clear() {
    $("#AddNewCommittee form select").val(null)
        .trigger("select2:unselect")
        .trigger("change");
    $("#AddNewCommittee form").trigger("reset");
}
$(document).ready(function ($) {
    //$('#btnCommitteeUpdate').hide();
    //$('#btnCommittee').show();
    $('#btnCommitteeUpdates').on('click', function () {
        debugger
        updateCommittee();
    })
    
});
//Add 
$(document).ready(function ($) {
    $(".modal").perfectScrollbar();     

    $('#btnCommittee').on('click', function () {
        debugger
        addCommittee();
    })

      


});
function addCommittee() {
    debugger
    $('#btnCommittee').show();
    //$('#AddNewOperation').trigger('reset');     
    $("input[type=submit]").attr("disabled", "disabled");
    $('#frmcommittee').validate({       
        errorPlacement: function (error, element) {
            $.notify({
                icon: "now-ui-icons travel_info",
                message: error.text(),
            }, {
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
        },
        submitHandler: function (form) {
            swal({
                title: "Are you sure?",
                text: "Credit committee will be added!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff9800",
                confirmButtonText: "Yes, continue",
                cancelButtonText: "No, stop!",
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve();
                        }, 4000);
                    });
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    $("#btnCommittee").attr("disabled", "disabled");

                    debugger

                    var Json_Data = {
                        //Id: $('#Id').val(),
                        Committee: $('#Committee').val(),
                        MgtNo: $('#MgtNo').val(),
                        BoardNo: $('#BoardNo').val(),
                        OperationId: $('#ddlOperationM').val(),
                        CreditMinMgt: $('#CreditMinMgt').val(),
                        CoyCode: $('#CoyCode').val(),
                        BrCode: $('#BrCode').val(),
                        Createdby: $('#Createdby').val(),
                        CreateDate: $('#CreateDate').val(),
                        CreditMinBoard: $('#CreditMinBoard').val(),
                        //CreditMinMgt: $('#CreditMinMgt').val(),
                        MgtNoApproval: $('#MgtNoApproval').val(),
                        BoardNoApproval: $('#BoardNoApproval').val(),
                        //OperationId: $('#OperationId').val()

                        CreditMaxComteeAmt: $('#CreditMaximumMgt').val(),
                        CreditMaxBoardAmt: $('#CreditMaximumMgtBcc').val()
                    }            
             
                    $.ajax({
                        url: 'ApprovalSetup/AddCommittee',
                        type: 'POST',
                        data: Json_Data,
                        dataType: "json",                     
                        success: function (result) {

                            if (result.toString !== '' && result !== null) {
                                swal({ title: 'Add Credit Committee', text: 'Credit committee added successfully!', type: 'success' }).then(function () { window.location.reload(true); });
                                $('#committeeTable').bootstrapTable('refresh', { url: 'ApprovalSetup/listcommittee' });                               
                                $("#AddNewCommittee").modal("hide");
                                $("#btnCommittee").removeAttr("disabled");
                            }
                            else {
                                swal({ title: 'Add Credit Committee', text: 'Something went wrong: </br>' + result.toString(), type: 'error' }).then(function () { clear(); });
                                $('#committeeTable').bootstrapTable('refresh', { url: 'ApprovalSetup/listcommittee' });
                                $("#AddNewCommittee").modal("hide");
                                $("#btnCommittee").removeAttr("disabled");
                            }
                        },
                        error: function (e) {
                            swal({ title: 'Add Operation Level', text: 'Adding credit committee level encountered an error', type: 'error' }).then(function () { clear(); });
                            $('#committeeTable').bootstrapTable('refresh', { url: 'ApprovalSetup/listcommittee' });
                            $("#AddNewCommittee").modal("hide");
                            $("#btnCommittee").removeAttr("disabled");
                        }
                    });
                }
            });
        }

    },
        function (dismiss) {
            swal('Add Credit Committee', 'You cancelled add committee level.', 'error');
            $("#btnCommittee").removeAttr("disabled");
        });

}

$(document).ready(function ($) {
    debugger
    $("#ddlOperationM").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#ddlOperationM").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",
            width: '100%',
            data: response.results
        });

    });
});
$(document).ready(function ($) {
    debugger
    $("#ddlOperationB").select2({
        theme: "bootstrap4",
        placeholder: "Loading...",
    });
    $.ajax({
        url: "ApprovalSetup/loadOperations",
    }).then(function (response) {
        debugger
        $("#ddlOperationB").select2({
            theme: "bootstrap4",
            placeholder: "Select Operation...",
            width: '100%',
            data: response.results
        });

    });
});

//detail view
$('#committeeTable').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading request...');

    var htmlData = '';
    var header = '<div>';
    var footer = '</div>';
    htmlData = htmlData + header;

    debugger

    var html =
        '<h8>' +
        '<div class="content"><div class="card"><div class="card-body">' +
        '<div class="row"><p style="color:red">' +
        '<div class="col-sm-6 "><strong style="color:red"> Management No.Of Approval:</strong> &nbsp' + row.mgtNoApproval + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Management No.:</strong>  &nbsp' + row.mgtNo + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Credit Min.Management:</strong>  &nbsp' + row.creditMinMgt + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Committee:</strong> ' + row.committee + '</div>' +
        '<div class="col-sm-6"><strong style="color:red">Created by:</strong>  &nbsp' + row.createdby + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Board No:</strong>  &nbsp' + row.boardNo + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Board No.Approval:</strong>  &nbsp' + row.boardNoApproval + '</div>' +
        '<div class="col-sm-6"><strong style="color:red"> Credit Min.Board:</strong>  &nbsp' + row.creditMinBoard + '</div>' +'';

    htmlData = htmlData + html;
    htmlData = htmlData + footer;
    $detail.html(htmlData);
});